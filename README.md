Everyware Onboarding
==================

## Development setup
The development setup is written for Mac users.

### Requirement

#### Bitbucket
You need to have [Bitbucket account](https://bitbucket.org)

#### Docker
[Installation instructions](https://docs.docker.com/v17.12/docker-for-mac/install/)

#### NPM
[Installation instructions](https://blog.teamtreehouse.com/install-node-js-npm-mac)

#### Gulp
```bash
npm install --global gulp-cli
```

#### Composer 
[Installation instructions](https://getcomposer.org/doc/00-intro.md)

#### Terminal client

### Download project
Open your favorite terminal and go the directory you want to download the project

```bash
# SSH
git clone git@bitbucket.org:infomaker/everyware-onboarding.git .

# HTTPS
git clone https://bitbucket.org/infomaker/everyware-onboarding.git .
```

### Add development domain to hosts

Open your favorite terminal and type:
```bash
sudo nano /etc/hosts
```
And add
```bash
127.0.0.1 ew.onboarding.localhost site.ew.onboarding.localhost
```

### Start project for the first time
```bash
# Install dev dependencies
composer install && npm install && gulp

# Install and start Docker
make luarocks && make start
```		

#### Helpful commands
```bash
# Docker
make start      # Starts projects containers
make stop       # Stops all projects containers
make restart    # Same as as make start && make stop combined

# Gulp
gulp                    # Build css & js resources for projects
gulp watch              # Build and watch css & js resources for project
gulp watch:admin        # Build and watch css & js resources for admin presentation for project

gulp stage              # Build css & js resources for stage environment
gulp prod               # Build css & js resources for production environment
```