## -*- makefile -*-

## kim@infomaker.se, Kim Brandt

## GNU Makefile for docker-compose run and other commands
##
## REFs:
##   https://docs.docker.com/compose/reference/
##   https://github.com/docker/compose/releases
##
##   NOTE: You must use tabs, not spaces, in a Makefile!

# The special rule .PHONY tells Make which targets are not files.
# This avoids conflict with files of the same name, and improves performance
.PHONY: start stop reload restart restart-full pull nginx-restart real-clean clean docker-compose-install docker-machine-install

.ONESHELL:

# Basic docker-compose commands
DOCKER_COMPOSE = docker-compose

# Define container-names
OPENRESTY = openresty
PHP = php-fpm
REDIS = redis
MYSQL = mysql
MY_IP = `ifconfig en0 inet | grep 'inet' | awk '{ print $$2 }'`

# Basic stop/start functions
#start: luarocks fix-extra-host-var-pre real-start fix-extra-host-var-post
start: fix-extra-host-var-pre real-start fix-extra-host-var-post

real-start:
	@${DOCKER_COMPOSE} up -d

stop:
	@${DOCKER_COMPOSE} stop

pull:
	@${DOCKER_COMPOSE} pull

restart: stop start

restart-full: stop remove pull start

# Requires either .rockspec- or .luarock-files in the folder ./rockspec.
# Installed libraries will be placed in ./lua/includes.
luarocks:
	@docker run --rm -v $$(pwd)/rockspec:/install -v $$(pwd)/lua/includes:/rocks registry.infomaker.io/luarocks:latest

# Removes all containers except MySQL
remove: stop
	@$(DOCKER_COMPOSE) rm -f ${OPENRESTY} ${PHP} ${REDIS}

# Removes the MySQL container.
# You will lose all MySQL data.
remove-mysql: stop
	@$(DOCKER_COMPOSE) rm -f ${MYSQL}

# Functions inside containers
nginx-restart:
	@${DOCKER_COMPOSE} exec ${OPENRESTY} nginx -s reload

mysql-dump:
	@${DOCKER_COMPOSE} exec ${MYSQL} mysqldump everyware > `pwd`/mysql/everyware-dump-`date +"%Y%m%d"`.sql

redis-flush:
	@${DOCKER_COMPOSE} exec ${REDIS} redis-cli flushall

# Restore a given MySQL dump file
mysql-restore: mysql-copy-files remove-mysql start
mysql-copy-files:
	@set -e ;\
	echo "Enter the path to dump and press [ENTER] to restore!" ;\
	echo "----------------------------------------------------" ;\
	ls -A1 `pwd`/mysql/*.sql ;\
	echo "----------------------------------------------------" ;\
	read MYSQLDUMPFILE ;\
	mv `pwd`/mysql/startup.sql `pwd`/mysql/startup.old.sql && cp $$MYSQLDUMPFILE `pwd`/mysql/startup.sql

composer:
	docker run --rm -v `pwd`:/app composer composer install

fix-extra-host-var-pre:
	@sed -i '' -e "s|WILL_BE_REPLACED_WITH_YOUR_HOST_IP|${MY_IP}|g" `pwd`/docker-compose.yml

fix-extra-host-var-post:
	@sed -i '' -e "s|${MY_IP}|WILL_BE_REPLACED_WITH_YOUR_HOST_IP|g" `pwd`/docker-compose.yml

# Bad stuff will happen if you don't know what you're doin'
real-clean:
	@echo "Exited containers: $$(docker ps -aq -f status=exited)"
	@docker rm -v $$(docker ps -aq -f status=exited)

## Install docker-compose on Linux and MacOS
## https://docs.docker.com/compose/install/
docker-compose-install:
	@mkdir -p /usr/local/bin
	@curl -L https://github.com/docker/compose/releases/download/1.8.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
	@chmod +x /usr/local/bin/docker-compose

## Install docker-machine on Linux and MacOS
## https://docs.docker.com/machine/install-machine/
docker-machine-install:
	@mkdir -p /usr/local/bin
	@curl -L https://github.com/docker/machine/releases/download/v0.7.0/docker-machine-`uname -s`-`uname -m` > /usr/local/bin/docker-machine
	@chmod +x /usr/local/bin/docker-machine
