/**
 * inView checks if the element is currently in view and triggers events on "view enter" and "view exit".
 *
 * Triggers:
 * 'inView.viewEnter'
 * 'inView.viewExit'
 */
;(function( $, window, undefined ) {
    "use strict";
    var eventNames = {
        move: 'everyware.moveViewFrame',
        resize: 'everyware.resizeViewFrame'
    };

    // InView Plugin
    // ======================================================
    var pluginInfo = {
        name : 'inview',
        version : '0.1',
        defaults : {
            threshold: 20,
            initialThreshold: window.innerHeight,
            onViewEnter: function (data) {},
            onViewExit: function (data) {}
        }
    };

    var $w = $(window),
        eventListener = $({}),
        viewFrame,
        inviewFrame = {
            frameHeight: 0,
            viewFrameTop: 0,
            viewFrameBottom: 0
        };


    // InViewFrame constructor
    function InViewFrame() {
        this.timeoutReset = null;

        this.init();
        return this;
    }

    // Avoid InViewFrame.prototype conflicts
    $.extend( InViewFrame.prototype, {
        init: function () {

            this.positionViewFrame($w.scrollTop(), $w.height())
                .addEvents();

            return this;
        },

        /**
         * Add Events
         * @returns {InViewFrame}
         */
        addEvents: function () {
            var self = this;

            // Do initial image check and setup events on window scroll and resize..
            $w.scroll( self.onScroll.bind(self) );
            $w.resize( self.onResize.bind(self) );

            return this;
        },

        /**
         * Function triggered on window resize
         */
        onResize: function () {
            var self = this,
                wHeight = $w.height();

            clearTimeout(this.timeoutReset);

            this.timeoutReset = setTimeout(function () {
                // Only trigger on vertical resize
                if (inviewFrame.frameHeight !== wHeight) {
                    self.positionViewFrame($w.scrollTop(), wHeight);
                    eventListener.trigger( eventNames.resize, inviewFrame);
                }
            }, 10);
        },

        /**
         * Function triggered on window scroll
         */
        onScroll: function () {
            var self = this;
            clearTimeout(this.timeoutReset);

            this.timeoutReset = setTimeout(function () {
                self.moveViewFrame($w.scrollTop());
                eventListener.trigger( eventNames.move, inviewFrame);
            }, 10);
        },

        /**
         * Function to move the viewFrame vertically on scroll
         * @returns {InViewFrame}
         */
        moveViewFrame: function (topPos) {

            // Only if position has changed
            if (topPos !== inviewFrame.viewFrameTop) {
                inviewFrame.viewFrameTop = topPos;
                inviewFrame.viewFrameBottom = topPos + inviewFrame.frameHeight;
            }
            return this;
        },

        /**
         * Function to set the view frame borders of which the element should trigger
         * @returns {InViewFrame}
         */
        positionViewFrame: function (topPos, wHeight) {

            wHeight = wHeight || $w.height();

            // If height has changed
            if (inviewFrame.frameHeight !== wHeight) {

                inviewFrame.frameHeight = wHeight;
                inviewFrame.viewFrameTop = topPos;
                inviewFrame.viewFrameBottom = topPos + inviewFrame.frameHeight;
            }

            return this;
        }
    });

    // The actual plugin constructor
    function Plugin ( element, options ) {

        this.el = element;
        this.$el = $(element);
        this._name = pluginInfo.name;
        this.inView = false;
        this.bottomPos = 0;
        this.topPos = 0;
        this.frameHeight = 0;
        this.viewFrameTop = 0;
        this.viewFrameBottom = 0;
        this.timeoutReset = null;
        this.settings = $.extend( {}, pluginInfo.defaults, options );

        // Change threshold on initialize to get a larger spec at first
        var threshold = this.settings.threshold;
        this.settings.threshold = this.settings.initialThreshold;
        this.init();
        this.settings.threshold = threshold;

        return this;
    }

    // Avoid Plugin.prototype conflicts
    $.extend( Plugin.prototype, {

        init: function () {
            this.$el.addClass('inview-el');

            this.windowWidth = window.innerWidth;

            // Use the frame values from the inviewFrame object above
            this.setFrameHeight(inviewFrame)
                .setViewFrame(inviewFrame)
                .calculateElPos()
                .checkImageInView()
                .addEvents();

            return this;
        },

        /**
         * Add Events
         * @returns {Plugin}
         */
        addEvents: function () {
            var self = this;

            $w.resize( self.onResize.bind(self) );

            eventListener.on( eventNames.resize, function (ev, inviewFrame) {
                ev.stopPropagation();

                self.setFrameHeight(inviewFrame)
                    .setViewFrame(inviewFrame)
                    .calculateElPos()
                    .checkImageInView();
            });

            eventListener.on( eventNames.move, function (ev, inviewFrame) {
                ev.stopPropagation();

                self.setViewFrame(inviewFrame)
                    .calculateElPos()
                    .checkImageInView();
            });

            return this;
        },

        /**
         * Function to calculate the position of the inview element
         */
        calculateElPos: function () {
            this.topPos = this.$el.offset().top;
            this.bottomPos = this.topPos + this.$el.height();

            return this;
        },

        /**
         * Checks if the images are in view or not, triggers load on images that are in view.
         */
        checkImageInView: function () {

            // If the element is within the window view or within the threshold set in options
            if ( this.bottomPos >= this.viewFrameTop && this.topPos <= this.viewFrameBottom ) {

                if (! this.inView) {
                    this.inView = true;
                    this.$el.addClass('inview-in').removeClass('inview-out');
                    this.triggerinViewCallback(this.inView);
                }

            } else if ( this.inView ) {
                this.inView = false;
                this.$el.addClass('inview-out').removeClass('inview-in');
                this.triggerinViewCallback(this.inView);
            }

            return this;
        },

        /**
         * Function triggered on window horizontal resize
         */
        onResize: function () {
            var self = this,
                wWidth = window.innerWidth;

            clearTimeout(this.timeoutReset);

            this.timeoutReset = setTimeout(function () {

                // Only trigger on horizontal resize
                if (wWidth !== self.windowWidth) {
                    self.calculateElPos()
                        .checkImageInView();
                }
                self.windowWidth = wWidth;

            }, 10);
        },

        /**
         * Function to calculate the height of the view frame
         * @returns {Plugin}
         */
        setFrameHeight: function (inviewFrame) {

            // Calculate the window height and add the threshold for top and bottom
            this.frameHeight = inviewFrame.frameHeight + (this.settings.threshold * 2);

            return this;
        },

        /**
         * Function to set the view frame borders of which the element should trigger
         * @returns {Plugin}
         */
        setViewFrame: function (inviewFrame) {

            // Remove the threshold from scrollTop to push the view frame above the window view.
            this.viewFrameTop = inviewFrame.viewFrameTop;
            this.viewFrameBottom = inviewFrame.viewFrameTop + inviewFrame.frameHeight;

            // Include threshold to frame offsets
            this.viewFrameTop = this.viewFrameTop - this.settings.threshold;
            this.viewFrameBottom = this.viewFrameBottom + this.settings.threshold;

            return this;
        },

        /**
         * Function to triggering callbacks depending on whether the element is in view.
         * @param isInView
         */
        triggerinViewCallback: function (isInView) {
            var data = { target: this.$el, inView: isInView };

            (isInView) ? this.settings.onViewEnter(data) : this.settings.onViewExit(data);

            return this;
        },

        /**
         * Function used to update the plugin
         * @param options
         */
        update: function (options) {
            this.settings = $.extend( {}, this.settings, options );
            this.checkImageInView();

            return this;
        }

    });

    /**
     * A really lightweight plugin wrapper around the constructor,
     * preventing against multiple instantiations
     */
    $.fn.inView = function( options ) {
        options = options || {};

        this.each(function() {
            if ( !$.data( this, "plugin_" + pluginInfo.name ) ) {
                $.data( this, "plugin_" + pluginInfo.name, new Plugin( this, options ) );
            } else {
                $.data( this, "plugin_" + pluginInfo.name).update(options);
            }
        });

        // chain jQuery functions
        return this;
    };

    $(function () {
        viewFrame = new InViewFrame();
    });

})( window.jQuery, window );
