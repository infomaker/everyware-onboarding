/**
 * This a test script that will trigger events on MediaQuery breakpoints.
 * This is not being used at the moment.
 * The point is that we will use this script to determine what "device" accross the Gota Javascript
 * to reduce all resize-listeners and window width-checks.
 */

;(function( $, window, document, undefined ) {
    'use strict';
    var eventNames = {
        mediaChange: 'everyware.mediaChange',
        orientationChange: 'everyware.orientationChange',
        heightChange: 'everyware.heightChange'
    };


    var $w = $(window),
        eventListener = $({}),
        mediaMatcher = {
            name : 'mediaMatcher',
            version : '0.1',
            defaults : {
                xs: {
                    min: 0,
                    max: 719
                },
                sm: {
                    min: 720,
                    max: 1079
                },
                md: {
                    min: 1080,
                    max: 1319
                },
                lg: {
                    min: 1320,
                    max: 2499
                },
                xl: {
                    min: 2500,
                    max: 3500
                }
            },
            mediaData: {
                size: '',
                previousSize: '',
                device: '',
                previousDevice: '',
                orientation: null,
                screenWidth: '',
                screenHeight: ''
            }
        };

    /**
     * The MediaMatcher Constructor
     * @returns {MediaMatcher}
     * @constructor
     */
    function MediaMatcher () {
        this.settings = $.extend( {}, mediaMatcher.defaults, {} );
        return this;
    }

    // Avoid MediaMatcher.prototype conflicts
    $.extend( MediaMatcher.prototype, {

        init: function () {
            this.windowWidth = window.innerWidth;
            this.windowHeight = window.innerHeight;

            this.addEvents()
                .setOrientation()
                .matchWidth(this.windowWidth);

            return this;
        },

        addEvents: function () {
            var self = this;

            $w.resize(self.onResize.bind(self));

            return this;
        },

        /**
         * Maths the saved orientation against the current to se if a change has occurred
         * @returns {boolean}
         */
        orientationChanged: function () {
            return this.getOrientation() !== mediaMatcher.mediaData.orientation;
        },

        /**
         * Function to match the window width
         * @param windowWidth
         *
         * @returns {boolean}
         */
        matchWidth: function (windowWidth) {
            var newSize = {};

            if (windowWidth >= this.settings.xl.min) {
                newSize = { size: 'xl', device: 'LargeDesktop' };

            } else if (windowWidth >= this.settings.lg.min ) {
                newSize = { size: 'lg', device: 'desktop' };

            } else if (windowWidth >= this.settings.md.min ) {
                newSize = { size: 'md', device: 'smallDesktop' };

            } else if (windowWidth >= this.settings.sm.min) {
                newSize = { size: 'sm', device: 'tablet' };

            } else {
                newSize = { size: 'xs', device: 'mobile' };
            }

            return this.setNewSize(newSize, windowWidth);
        },

        /**
         * Function to set the new data if size has changed
         * @param windowWidth
         * @param options
         * @returns {boolean}
         */
        setNewSize: function (options, windowWidth) {

            if (mediaMatcher.mediaData.size !== options.size) {

                mediaMatcher.mediaData.screenWidth = windowWidth;

                mediaMatcher.mediaData.previousSize = mediaMatcher.mediaData.size;
                mediaMatcher.mediaData.previousDevice = mediaMatcher.mediaData.device;

                mediaMatcher.mediaData.size = options.size;
                mediaMatcher.mediaData.device = options.device;

                return true;
            }

            return false;
        },

        /**
         * Function to determine the orientation of the window
         * @returns {string}
         */
        getOrientation: function () {
            return (this.windowHeight > this.windowWidth) ? 'portrait': 'landscape';
        },

        /**
         * Function to determine the orientation of the window
         * @returns {MediaMatcher}
         */
        setOrientation: function () {
            mediaMatcher.mediaData.orientation = this.getOrientation();
            return this;
        },

        /**
         * Function triggered on window resize
         */
        onResize: function() {
            var self = this;
            clearTimeout(this.timeoutReset);

            this.timeoutReset = setTimeout(function () {

                // Only trigger on horizontal change
                if (self.windowWidth !== window.innerWidth) {

                    // Save Current window width for further matching
                    self.windowWidth = window.innerWidth;

                    // if matcher indicate that a change in media has occurred
                    if (self.matchWidth(self.windowWidth)) {
                        eventListener.trigger( eventNames.mediaChange, mediaMatcher.mediaData );
                    }
                }

                // Only trigger on vertical change
                if (self.windowHeight !== window.innerHeight) {

                    // Save Current window height for further matching
                    self.windowHeight = window.innerHeight;

                    // If matcher indicate that a change in height has occurred
                    mediaMatcher.mediaData.screenHeight = self.windowHeight;
                    eventListener.trigger(eventNames.heightChange, mediaMatcher.mediaData);
                }

                // if orientation has changed
                if (self.orientationChanged()) {
                    self.setOrientation();
                    eventListener.trigger( eventNames.orientationChange, mediaMatcher.mediaData );
                }

            }, 10);
        }
    });

    // Extend jQuery to enable some information to get fetched from MediaMatcher
    // ex. $.mediaMatcher('getMedia', {callback});
    $.extend({
        mediaMatcher: function(funcName, callback) {

            this.getMedia = function (callback) {
                callback(mediaMatcher.mediaData);
                return this;
            };

            this.mediaChange = function (callback) {
                eventListener.on( eventNames.mediaChange, function (ev, data) {
                    ev.stopPropagation();
                    ev.preventDefault();
                    callback(data);
                });
                return this;
            };

            this.orientationChange = function (callback) {
                eventListener.on( eventNames.orientationChange, function (ev, data) {
                    ev.stopPropagation();
                    ev.preventDefault();
                    callback(data);
                });
                return this;
            };

            this.heightChange = function (callback) {
                eventListener.on( eventNames.heightChange, function (ev, data) {
                    ev.stopPropagation();
                    ev.preventDefault();
                    callback(data);
                });
                return this;
            };

            if (typeof this[funcName] === 'function') {
                this[funcName](callback);
            }

            return this;
        }
    });

    $(document).ready(function () {

        // Creating MediaMatcher
        var mediaMatcher = new MediaMatcher();

        // Initiating mediaMatcher
        mediaMatcher.init();
    });

})( window.jQuery, window, document );