var SocialShare = (function ($, undefined) {
	var shareDefaults = {
		shareImage: false,
		shareText: '',
		shareUrl: false
	};
	var twitterAccount = infomaker.settings.twitter.account;
	var facebookId = infomaker.settings.facebook.id;
	var facebookSDKSetting = {
		appId: facebookId,
		status: true,
		xfbml: false,
		version: 'v2.11'
	};
	
	/**
	 * Create a popup from a url and dimensions
	 * @param url
	 * @param w
	 * @param h
	 * @returns {Window}
	 */
	var buildPopup = function (url, w, h) {
		var dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : screen.left;
		var dualScreenTop = window.screenTop !== undefined ? window.screenTop : screen.top;
		
		var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
		var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
		
		var left = ((width / 2) - (w / 2)) + dualScreenLeft;
		var top = ((height / 2) - (h / 2)) + dualScreenTop;
		
		return window.open(url, '_blank', 'width=' + w + ', height=' + h + ', top=' + top + ' left=' + left);
	};
	
	var getShareData = function (btn) {
		var $btn = $(btn);
		return $.extend(shareDefaults, $btn.closest('.social-share-container').data() || {}, $btn.data());
	};
	
	var initializeFacebook = function () {
		if (facebookId !== undefined) {
			
			$.getScript('//connect.facebook.net/sv_SE/sdk.js', function () {
				FB.init(facebookSDKSetting);
				
				$(document).on('click', '.fb-share-button', function (e) {
					var data = getShareData(e.target);
					
					if (data.shareUrl) {
						var fb_data = {
							method: 'share',
							href: data.shareUrl
						};
						
						if (data.shareImage) {
							fb_data.picture = data.shareImage;
						}
						
						FB.ui(fb_data, function(response){
							// Debug response (optional)
							console.log(response);
						});
					}
					e.preventDefault();
				}.bind(this));
			}.bind(this));
		}
	};
	
	var initializeMail = function () {
		$(document).on('click', '.mail-share-button', function (e) {
			var data = getShareData(e.target);
			
			if (data.shareUrl) {
				window.location.href = serializedUrl('mailto:', {
					'subject': data.shareText,
					'body': data.shareUrl
				});
			}
			e.preventDefault();
		}.bind(this));
	};
	
	var initializeTwitter = function () {
		if (twitterAccount !== undefined) {
			$(document).on('click', '.twitter-share-button', function (e) {
				var data = getShareData(e.target);
				
				if (data.shareUrl) {
					buildPopup(serializedUrl('https://twitter.com/intent/tweet', {
						'text': data.shareText,
						'url': data.shareUrl,
						'via': twitterAccount
					}), 580, 380);
				}
				e.preventDefault();
			}.bind(this));
		}
	};
	
	/**
	 * Combine a url with an object of properties to create a valid url with query parameters
	 * @param url
	 * @param params
	 * @returns {string}
	 */
	var serializedUrl = function (url, params) {
		var str = [];
		for (var p in params)
			if (params.hasOwnProperty(p)) {
				str.push(encodeURIComponent(p) + "=" + encodeURIComponent(params[p]));
			}
		return url + '?' + str.join("&");
	};
	
	return {
		inited: false,
		init: function (options) {
			
			if (!this.inited) {
				var initOptions = $.extend({
					facebook: false,
					twitter: false,
					mail: false
				}, options);
				
				if ( initOptions.facebook ) {
					initializeFacebook();
				}
				
				if ( initOptions.twitter ) {
					initializeTwitter();
				}
				
				if ( initOptions.twitter ) {
					initializeMail();
				}
				
				this.inited = true;
				
				return true;
			}
			
			return false;
		}
	};
})(jQuery);

module.exports = SocialShare;