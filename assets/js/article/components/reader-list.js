var ReaderList = module.exports = function() {
    // DOM elements
    this.readerArticleListMenu = $('.readerlist__article-menu');
    this.readerArticleList = $('.readerlist__article-list');
    this.readerProgressBar = $('.readerlist__progress .progress__bar');
    this.readerShareListMenu = $('.readerlist__article-share');
    this.shareInfo = $('.share-info', '.readerlist__article-share-info');
    this.articelDisplayInfo = $('.readerlist__article-display');
    this.overlay = $('.readerlist-overlay');
    this.articleListArticleLink = $('.readerlist-anchor');

    // Current article showing
    this.activeArticle = null;

    // Booleans for menus
    this.shareInfoOpen = false;
    this.articleListOpen = false;

    this.scrollInfo = {
        lastY: 0,
        currentY: 0,
        minY: 0,
        maxY : 0
    };

    setupEvent.apply(this);
};

ReaderList.prototype.update = function( activeArticle ) {
    this.activeArticle = activeArticle;
    updateProgress.apply(this, [ activeArticle.uuid ] );
    // updateShareInfo.apply(this);
    updateArticleDisplayInfo.apply(this);

    this.scrollInfo.counter = Math.max(0, (this.scrollInfo.maxY - this.scrollInfo.currentY));
};

var setupEvent = function() {
    $(window).scroll( $.proxy(updateCurrentPosition, this) );

    var toggleOverlay = function(toggle) {
        $(this.overlay).toggle(toggle);
    }.bind(this);

    var toggleShareMenu = function(toggle) {
        this.shareInfoOpen = toggle;
        $(this.readerShareListMenu).find('input').prop('checked', toggle);
    }.bind(this);

    var toggleArticleList = function(toggle) {
        this.articleListOpen = toggle;
        $(this.readerArticleListMenu).find('input').prop('checked', toggle);
    }.bind(this);

    // Overlay
    $(this.overlay).click(function() {
        toggleShareMenu(false);
        toggleArticleList(false);
        toggleOverlay(false);
    }.bind(this));

    // Article list menu button
    $('label', this.readerArticleListMenu).click( function() {

        if( this.shareInfoOpen ) {
            toggleShareMenu(false);
        } else {
            toggleOverlay(!this.articleListOpen);
        }

        this.articleListOpen = !this.articleListOpen;
    }.bind(this));

    // Share info button
    // $('label', this.readerShareListMenu).click(function() {
    //     if( this.articleListOpen ) {
    //         toggleArticleList(false);
    //     } else {
    //         toggleOverlay(!this.shareInfoOpen);
    //     }
    //
    //     this.shareInfoOpen = !this.shareInfoOpen;
    // }.bind(this));

    // Goto article link
    $(this.articleListArticleLink).click(function(e) {
        e.preventDefault();

        var uuid = $(e.currentTarget).data('uuid');

        // Send event that we have clicked article in list
        window.dispatchEvent(new CustomEvent('readerListArticleClicked', { 'detail' : {'uuid': uuid }} ));

        toggleArticleList(false);
        toggleOverlay(false);
    }.bind(this));
};

var updateCurrentPosition = function() {
    this.scrollInfo.lastY = this.scrollInfo.currentY;
    this.scrollInfo.currentY = $(window).scrollTop();

    this.scrollInfo.minY = $(this.activeArticle.ele).offset().top;
    this.scrollInfo.maxY = this.scrollInfo.minY + $(this.activeArticle.ele).height() - $('.site-nav-wrapper').height();

    var progress = (this.scrollInfo.currentY - this.scrollInfo.minY) / (this.scrollInfo.maxY - this.scrollInfo.minY) * 100;

    $(this.readerProgressBar).css( {
        'width' : Math.min(100, Math.max(0, progress)) + '%'
    });
};

var updateProgress = function( activeUuid ) {
    $(this.readerArticleList).find('ul a').each(function(idx, ele) {
        var uuid = $(ele).data('uuid');

        if( activeUuid === uuid ) {
            $(ele).parent().addClass('current');
        } else {
            $(ele).parent().removeClass('current');
        }
    }.bind(this));
};

// var updateShareInfo = function() {
//
//     var found = false;
//     $(this.shareInfo).find('.article-share-image img').each(function(idx, ele) {
//         if( $(ele).data('article-uuid') !== this.activeArticle.uuid ) {
//             $(ele).css('display', 'none');
//         } else {
//             $(ele).css('display', 'block');
//             found = true;
//         }
//     }.bind(this));
//
//     if( !found ) {
//         var img = $('<img>');
//         $(img).data('article-uuid', this.activeArticle.uuid)
//             .attr('src', this.activeArticle.shareImage);
//
//         $(this.shareInfo).find('.article-share-image').append(img);
//     }
//
//     $(this.shareInfo).find('span:first-of-type').text(this.activeArticle.headline);
//     $(this.shareInfo).find('span:last-of-type').text(this.activeArticle.section + ', ' + this.activeArticle.author);
// };

var updateArticleDisplayInfo = function() {
    infomaker.article = infomaker.article || {};
    infomaker.article.related = infomaker.article.related || [];

    var position = infomaker.article.related.map(function(e) { return e.uuid; }.bind(this)).indexOf(this.activeArticle.uuid),
        total = infomaker.article.related.length + 1;

    position += 2;

    var text = "Aktuell artikel (" + position + " av " + total + ")",
        displayHeadline = this.activeArticle.headline !== '' ? this.activeArticle.headline : '&nbsp;';

    $(this.articelDisplayInfo).find('div:first-of-type').text(text);
    $(this.articelDisplayInfo).find('div:last-of-type').html(displayHeadline);
};