var AjaxArticle = {

    articleData: undefined,

    init: function () {
        console.log('AjaxArticle init');
    },

    /**
     * Get article
     *
     * @param uuid
     */
    getArticle: function (uuid) {
        var deferred = $.Deferred();

        // console.log(`We have have ${uuid}`);
        $.get(infomaker.ajaxurl, {
            action: 'getArticle',
            uuid: uuid
        })
            .done(function (response) {

                this.articleData = $(response.data.output);

                deferred.resolve(this.articleData);
            }.bind(this))
            .fail(function () {
                deferred.reject();
            });

        return deferred.promise();
    },

    /**
     * Get articles
     *
     * @param uuids
     */
    getArticles: function (uuids) {
        var deferred = $.Deferred();

        // console.log(`We have have ${uuid}`);
        $.get(infomaker.ajaxurl, {
            action: 'getArticles',
            uuids: uuids.join(',')
        })
            .done(function (response) {
                deferred.resolve(response.data.articles);
            }.bind(this))
            .fail(function () {
                deferred.reject();
            });

        return deferred.promise();
    }

};

module.exports = AjaxArticle;