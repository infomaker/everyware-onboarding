var AjaxArticle = require("./ajax-article"),
  ReaderList = require("./reader-list");

ScrollPackage = {
  preloadedArticles: {},
  loadedArticles: [],
  readerList: null,
  activeArticle: {
    ele: null,
    uuid: null,
    permalink: null,
    headline: null,
    title: null,
    authors: null,
  },

  /**
   * Kick it
   */
  init: function () {
    if ($(".related-article", "#main-body").length > 0) {
      ScrollPackage.initSetup();
    }
  },

  /**
   * Scroll functionality
   */
  scroll: function () {
    this.checkActiveArticle();
  },

  /**
   * Check active article
   */
  checkActiveArticle: function () {
    $("#main-body > article, .related-article > article").each(function () {
      if (
        ScrollPackage.isScrolledToTop($(this)) &&
        ScrollPackage.activeArticle.uuid !== $(this).data("uuid")
      ) {
        var prevArticle = ScrollPackage.activeArticle;
        ScrollPackage.setActiveArticle($(this));
        var activeArticle = ScrollPackage.activeArticle;

        ScrollPackage.readerList.update(ScrollPackage.activeArticle);

        // Change title
        if ($(this).data("title")) {
          document.title = ScrollPackage.activeArticle.title;
        }
        ScrollPackage.triggerAnalytics();
        console.log("prev", prevArticle);
        TROMB.Analytics.leave(prevArticle.uuid, window.VisitUuid, prevArticle.authors);

        // Change url and do check to use same protocol as server
        window.history.replaceState({},
          ScrollPackage.activeArticle.title,
          ScrollPackage.activeArticle.permalink.replace(
            /(^\w+|^)/,
            location.protocol.replace(":", "")
          )
        );

        var paywall = $(this).hasClass(TROMB.Analytics.config.paywallSelector);
        console.log(activeArticle);

        var hit = TROMB.Analytics.hit(activeArticle.uuid, paywall, activeArticle.authors);
      }
    });
  },

  /**
   * Set active article
   *
   * @param article
   */
  setActiveArticle: function (article) {
    this.activeArticle = {
      ele: article,
      uuid: article.data("uuid"),
      permalink: article.data("permalink"),
      headline: article.data("headline"),
      title: article.data("title"),
      authors: article.data("authorUuid"),
      pageViewSent: this.activeArticle.pageViewSent === undefined // With this we don't trigger the first article
    };
  },

  /**
   * Check related articles in viewport
   */
  inViewChecker: function () {
    $(".related-article", "#main-body").inView({
      threshold: -45,
      initialThreshold: 0,
      onViewEnter: function (data) {
        var uuid = data.target.data("uuid");

        ScrollPackage.loadArticle(uuid);
      }
    });
  },

  /**
   * Load article
   *
   * @param uuid
   */
  loadArticle: function (uuid) {
    var $articleContainer = ScrollPackage.getArticleContainer(uuid);

    if (ScrollPackage.checkIfArticleLoaded(uuid)) {
      return;
    }

    // Trigger ads
    ScrollPackage.triggerArticleAds(uuid);

    // Save added article to loaded articles
    ScrollPackage.loadedArticles[uuid] = $articleContainer.find("article");

    // Trigger gallery
    $articleContainer.find(".js-image-carousel").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      speed: 200,
      dots: true,
      appendDots: $(".carousel__dots")
    });

    // Send event that we have loaded article
    window.dispatchEvent(new Event("ajaxArticleLoaded"));
  },

  /**
   * Preload articles
   */
  preloadArticles: function () {
    var uuids = [];

    $(".related-article.not-loaded", "#main-body").each(function () {
      uuids.push($(this).data("uuid"));
    });

    return AjaxArticle.getArticles(uuids).then(function (data) {
      ScrollPackage.preloadedArticles = data;

      $.each(ScrollPackage.preloadedArticles, function (uuid, data) {
        var $articleContainer = ScrollPackage.getArticleContainer(uuid);

        $articleContainer.append(data);
      });
    });
  },

  /**
   * Trigger ads for loaded content
   *
   * @param uuid
   */
  triggerArticleAds: function (uuid) {
    var $ads = ScrollPackage.getArticleContainer(uuid).find(".ad");

    // Skip it if we don't find any ads
    if ($ads.length === 0) {
      return;
    }

    // Load new ads
    $ads.each(function (idx, ele) {
      Ads.loadAsync(ele);
    });

    Ads.executeQueue();
  },

  /**
   * Check for if we are topped positioned
   *
   * @param elem
   * @returns {boolean}
   */
  isScrolledToTop: function (elem) {
    if (elem.length > 0) {
      var mainMenu = $(".site-nav-wrapper"),
        win = $(window),
        scrollPosition = win.scrollTop(),
        firstArticleHeight = elem.height(),
        elemTop = elem.offset().top - mainMenu.outerHeight(),
        elemBottom =
        elem.offset().top + firstArticleHeight - mainMenu.outerHeight();

      return scrollPosition >= elemTop && scrollPosition < elemBottom;
    }
  },

  /**
   * Trigger analytics
   */
  triggerAnalytics: function () {
    if (!this.activeArticle.pageViewSent) {
      // Update GA with new page and pageview event
      if (window.ga !== undefined) {
        ga("set", "page", this.activeArticle.permalink);
        ga("send", "pageview");
      }

      // Update piwik with new pageview
      if (window._paq !== undefined) {
        _paq.push(["setCustomUrl", this.activeArticle.permalink]);
        _paq.push(["setDocumentTitle", this.activeArticle.title]);
        _paq.push(["trackPageView"]);
      }

      // Update SIFO with new statistics about the loaded article
      // if( window._cInfo !== undefined && CAnalytics !== undefined ) {
      //     window._cInfo = [];
      //
      //     window._cInfo.push(
      //         { cmd: "_trackContentPath", val: response.data.content_path },
      //         { cmd: "_executeTracking" }
      //     );
      //
      //     new CAnalytics.Measure({
      //         BaseUrl: ("https:" === document.location.protocol ? "https://" : "http://") + "trafficgateway.research-int.se",
      //         PanelOnlyAddress: ("https:" === document.location.protocol ? "https://" : "http://") + "trafficgateway.research-int.se/PanelInfo/OiDnt",
      //         SiteId: "194db2d7-5499-454a-a309-cda6aba48b6e",
      //         MeasureType: 2,
      //         CookieExpirationDays: 700,
      //         CookieDomain: typeof window != "undefined" && typeof location != "undefined" && typeof window.location.hostname != "undefined" ? "." + window.location.hostname.split(".").slice(-2).join(".") : "vf.se",
      //         VideoPingInterval: 1e4
      //     }, window._cInfo);
      // }

      this.activeArticle.pageViewSent = true;
    }
  },

  /**
   * Helper function to get surrounding article container
   * @param uuid
   * @returns {*|jQuery}
   */
  getArticleContainer: function (uuid) {
    return $("#main-body").find(".related-article[data-uuid='" + uuid + "']");
  },

  /**
   * Check if article loaded
   *
   * @param uuid
   *
   * return boolean
   */
  checkIfArticleLoaded: function (uuid) {
    return ScrollPackage.loadedArticles.hasOwnProperty(uuid);
  },

  /**
   * Scroll to article
   *
   * @param uuid
   */
  scrollToArticle: function (uuid) {
    $(document).scrollTop(
      $("#article-" + uuid).offset().top - $(".site-nav-wrapper").height() + 5
    );
  },

  /**
   * Listen to events from ReaderList
   */
  listenToReaderListEvents: function () {
    window.addEventListener(
      "readerListArticleClicked",
      function (e) {
        var uuid = e.detail.uuid;
        ScrollPackage.scrollToArticle(uuid);
      },
      false
    );
  },

  /**
   * Setup initial article
   */
  setupFirstArticle: function () {
    var initialArticle = $("#main-body")
      .find("article")
      .first();

    // Set active article
    ScrollPackage.setActiveArticle(initialArticle);
    ScrollPackage.loadedArticles[initialArticle.data("uuid")] = initialArticle;
    ScrollPackage.readerList.update(ScrollPackage.activeArticle);
  },

  /**
   * Present reader list
   */
  presentReaderList: function () {
    $(".readerlist").animate({
      bottom: 0
    });
  },

  /**
   * Initial setup
   */
  initSetup: function () {
    this.readerList = new ReaderList();
    ScrollPackage.setupFirstArticle();

    // Wait for preload all articles before we kick it
    ScrollPackage.preloadArticles().then(function () {
      ScrollPackage.presentReaderList();

      $(window).scroll(function () {
        ScrollPackage.scroll();
      });

      ScrollPackage.inViewChecker();
      ScrollPackage.listenToReaderListEvents();
    });
  }
};

module.exports = ScrollPackage;