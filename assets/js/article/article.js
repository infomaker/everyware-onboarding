var SocialShare = require('./components/social-share'),
    ScrollPackage = require('./components/scroll-package');

$(document).ready(function() {
	SocialShare.init({
		facebook:true,
		twitter:true,
		mail:true
	});

	ScrollPackage.init();

    // Bypass auto scrolling on refresh.
    if ('scrollRestoration' in history) {
        history.scrollRestoration = 'manual';
    }
});