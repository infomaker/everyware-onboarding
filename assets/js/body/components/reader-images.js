// require('magnific-popup');
// var Masonry = require('masonry-layout');
var ImageLoaded = require('imagesloaded');

var ReaderImages = {

    formId: '#ewc-ri-form',
    progressBarId: "#ewc-ri-progress-bar",
    formMessageId: "#ewc-ri-message-wrapper",
    presentationId: "#ewc-ri-presentation",

    init: function () {
        if ($(this.formId).length) {
            ReaderImages.form();
        }

        if ($(this.presentationId).length) {
            ReaderImages.presentation();
        }
    },

    form: function () {

        // Submit form
        $(this.formId).on('submit', function (e) {
            e.preventDefault();
            ReaderImages.resetForm();

            if (e.target.checkValidity()) {

                var photographer = $(ReaderImages.formId).find('#ewc-ri-photographer').val(),
                    year = $(ReaderImages.formId).find('#ewc-ri-year').val(),
                    email = $(ReaderImages.formId).find('#ewc-ri-email').val(),
                    images = $(ReaderImages.formId).find('#ewc-ri-image'),
                    description = $(ReaderImages.formId).find('#ewc-ri-description').val(),
                    $messageWrapper = $(ReaderImages.formId).find(ReaderImages.formMessageId),
                    $progressWrapper = $(ReaderImages.formId).find("#ewc-ri-progress-wrapper"),
                    $progressBar = $progressWrapper.find(ReaderImages.progressBarId),
                    taxonomyId = $(ReaderImages.formId).find('#ri-taxonomyId').val();

                var formData;
                if (window.FormData) {
                    formData = new FormData($(ReaderImages.formId)[0]);
                }

                $.each($(images), function (i, obj) {
                    $.each(obj.files, function (j, file) {
                        formData.append('files[' + j + ']', file);
                    })
                });

                formData.append('action', 'create_reader_images');

                // Show progress bar
                $progressWrapper.fadeIn(200);

                $.ajax({
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();

                        // Update progress
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);

                                $progressBar.css('width', percentComplete + '%');

                                if (percentComplete === 100) {
                                    $progressBar.html('Behandlar bild...');
                                }

                            }
                        }, false);

                        return xhr;
                    },
                    url: infomaker.ajaxurl,
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        $messageWrapper.attr('class', 'alert alert-success').show().html('<strong>Tack för ditt bidrag!</strong> ' + data.message);
                        $(ReaderImages.formId).trigger('reset');
                    },
                    error: function (data) {
                        $messageWrapper.attr('class', 'alert alert-danger').show().html('<strong>Något gick fel!</strong> Testa igen och skulle det inte fungera nu så kontakta supporten så kollar vi närmare på det. <i>' + data.responseJSON.message + '</i>');
                    },
                    complete: function () {
                        ReaderImages.resetProgressBar();
                    }
                });
            }
        });
    },

    resetForm: function () {
        this.resetMessage();
        this.resetProgressBar();
    },

    resetMessage: function () {
        $(this.formMessageId).html('').hide();
    },

    resetProgressBar: function () {
        $(this.progressBarId).parent().hide();
        $(this.progressBarId).css('width', 0).html('Laddar upp bild...');
    },

    presentation: function () {
        $(document).ready(function () {

            // Remove loading page
            $(this.presentationId).imagesLoaded()
                .done(function () {
                    $(ReaderImages.presentationId).removeClass('loading-images');
                });

        });
    }
};

module.exports = ReaderImages;