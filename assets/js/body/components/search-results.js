var SearchResults = {

    init: function() {

        if ($('.js-search-form').length === 0)
            return

        $('input[type=radio][name=sort]').change(function() {
            $(this).closest('.js-search-form').submit()
        })
    }
};

module.exports = SearchResults;