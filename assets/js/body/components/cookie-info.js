var Cookies = require('./cookies');

CookieInfo = {
    init: function () {
        var infoPageUrl = 'http://www.minacookies.se/';
        if (Cookies.get('cookieinfo') !== 1 && !Boolean(infomaker.isApp)) {

            var cookieInfo = '' +
                '<div id="cookie-info">' +
                    '<div class="icon icons-info"></div>  Vi använder cookies för att ge dig en bättre upplevelse. Genom att fortsätta godkänner du att vi använder <a target="_blank" href="'+infoPageUrl+'">cookies</a> på sajten. <a id="cookie-accept" href="#">Jag förstår!</a>' +
                '</div>';

            $('body').append(cookieInfo);

            $("#cookie-accept").click(function (e) {
                e.preventDefault();

                $("#cookie-info").css("display", "none");
                Cookies.set('cookieinfo', 1, 365, '/');
            });
        }
    }
};

module.exports = CookieInfo;