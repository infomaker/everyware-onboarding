var LoginFormWidget = {
    init: function () {
        this.setupEvents();
    },

    setupEvents: function() {
        $('body').on('click', '.login-form__collapse-btn', function(e) {
            e.preventDefault();

            $(this).parent().find('.login-form__wrapper').stop(true, true).animate({ height: 'toggle'}, 200);
        });
    }
};

module.exports = LoginFormWidget;