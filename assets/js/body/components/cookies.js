var Cookie = {

    /**
     * setCookie() sets a new cookie
     * based on the passed in name, value and exdays
     *
     * @param name
     * @param value
     * @param {int} exdays
     * @param path
     */
    set: function (name, value, exdays, path) {
        path = path === undefined ? '/' : path;

        var date = new Date();
        date.setTime(date.getTime() + (exdays * 24 * 60 * 60 * 1000));

        var expires = "expires=" + date.toUTCString();

        document.cookie = name + "=" + JSON.stringify(value) + "; " + expires + ";path=" + path;
    },

    /**
     * getCookie() returns a cookie
     * based on the passed in name name
     *
     * @param {String} name
     * @return {string} element
     */
    get: function (name) {
        name = name + "=";

        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return JSON.parse(c.substring(name.length, c.length));
            }
        }
        return "";
    },

    delete: function (name, path) {
        path = path === undefined ? '/' : path;

        document.cookie = name + '=; Path=' + path + '; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }
};

module.exports = Cookie;