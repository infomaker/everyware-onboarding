TeaserCommentBubbles = {
    init: function (document) {
        var customerID = 8;

        // fetch all teaser uuids
        var articleIds = this.fetchTeaserIds();

        if (articleIds.length > 0) {

            var jsonObj = "{ \"customerId\": " + customerID + ",\"articleIds\": " + JSON.stringify(articleIds) + "}";

            $.get(infomaker.ajaxurl, {
                action: 'comment_count',
                data: {json: jsonObj},
                dataType: 'json'
            }).done(function (response) {
                if (response === false) {
                   console.log('No comment count could be recieved.');
                }

                var results = '';

                if (response) {
                    results = $.parseJSON(response);
                }

                $.each(results, function(i, item) {
                    var teaser = $('.teaser[data-comment-id="' + item.articleId + '"]'),
                        bubble =  teaser.find('.js-comment-count').parent(),
                        bubbleValue = bubble.find('.js-comment-count');

                    if (item.commentCount > 0) {
                        bubbleValue.text(item.commentCount);
                        bubble.css('display', 'inline-block');
                    }
                });
            });
        }
    },

    /**
     * Collects article uuids from rendered teasers
     */
    fetchTeaserIds: function() {
        var id_arr = [];
        $('.teaser').each(function(i, item) {
            if ($(item).find('.teaser__notifications').length > 0) {
                id_arr.push($(item).data('commentId'));
            }
        });
        
        return id_arr.sort();
    }
};

module.exports = TeaserCommentBubbles;