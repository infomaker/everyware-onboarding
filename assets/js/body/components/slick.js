SlickSlider = {
    init: function() {
        $('.widget-karriar__slider-container').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 4000
        });

        // Don't do this on single article because it's triggers the slideshow twice
        if($('body.single-article').length === 0) {
            $('.carousel-wrapper').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                autoplay: true,
                autoplaySpeed: 40000,
                adaptiveHeight: false
            });
        }

        // Good-to-know: We have same options in scroll-package
        $('.js-image-carousel').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            speed: 200,
            dots: true,
            appendDots: $('.carousel__dots')
        });

    }
};

module.exports = SlickSlider;