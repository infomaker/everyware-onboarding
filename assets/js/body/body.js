// var Sticky = require('./components/sticky');

$(document).ready(function () {
	/**
	 * @see https://v4-alpha.getbootstrap.com/getting-started/browsers-devices/#android-stock-browser
	 */
	var nua = navigator.userAgent,
		isAndroid = (nua.indexOf('Mozilla/5.0') > -1 && nua.indexOf('Android ') > -1 && nua.indexOf('AppleWebKit') > -1 && nua.indexOf('Chrome') === -1),
    	isiOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);

	if (isAndroid) { $('select.form-control').removeClass('form-control').css('width', '100%'); }
});

/**
 *  Copyright 2014-2017 The Bootstrap Authors
 *  Copyright 2014-2017 Twitter, Inc.
 *  Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */

/**
 * @see https://getbootstrap.com/docs/3.3/getting-started/#support-ie10-width
 */
if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
	var msViewportStyle = document.createElement('style');
	msViewportStyle.appendChild(
		document.createTextNode(
			'@-ms-viewport{width:auto!important}'
		)
	);
	document.querySelector('head').appendChild(msViewportStyle);
}