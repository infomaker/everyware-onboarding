<?php
/**
 * Determine which environment we're in
 */

if (isset($_SERVER['IMHOST'])) {
    /**
     * Set up AWS-WP specifics
     */
    require_once( dirname( __FILE__ ) . '/wp-conf/wp-config-aws.php' );
}else{
    /**
     * Set up DEV-WP Specifics
     */
    require_once( dirname( __FILE__ ) . '/wp-conf/wp-config-dev.php' );
}
/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
 define('AUTH_KEY',         '3dW<u;pV56+l_eCR{tnT6^mB,iSmk:A2y5=:3La4`t$82e6N%+L2851N+4[;cVz4');
 define('SECURE_AUTH_KEY',  'wyk]1y01/ybBV?{u^@FUsSBVk8pBum1+@Vjyv!YMp&$)q$i?_!u4Ttm +:zes7tg');
 define('LOGGED_IN_KEY',    '}T+;OI&|%8kI~uNG{1M+f=<$@HmBGytiZe[:E#%[4#F8boPL<lM(l |gf!6uzd F');
 define('NONCE_KEY',        'pceLYueVQ)9x/I0#V-nHvy$?!J)lcg$U$pmX[!^:5~~&m?Dw1j]JROQIj>YfcqfN');
 define('AUTH_SALT',        'io_wzc@]ID*I;0R^rO:bPutjG_$g`n1h#As,0`31jG&)ys>A6n*O/Dc>dI>&r>m?');
 define('SECURE_AUTH_SALT', 'RO+BSwC3wF(,y/pS8a9a^]ATHhId^D(Qt De{NLX%n_~wWwtIj8Rw#(v%cGG>GQ}');
 define('LOGGED_IN_SALT',   'd/w`m8xZfH/qGsbiz}bkH }GB{iW^n?cXpWdaT]ab]hHsADP 0mI*Z<|S.)1it!+');
 define('NONCE_SALT',       'o]%5l%5BB&S6B*<m6GTo2n_&{$%C|]5GVG1J^8?;srcm_CP`]k?LF>@EF[5RTjq4');

/**
 * WordPress Database Table prefix.
 */
$table_prefix  = 'wp_';
/* Disable auto-update */
define('WP_AUTO_UPDATE_CORE', false);
/* Disable file editing from WP-admin */
define('DISALLOW_FILE_MODS', true);
/* Multisite */
define('WP_ALLOW_MULTISITE', true);
define('WP_CACHE', true);
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', true);
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);
define('SUNRISE', 'on');
/* That's all, stop editing! Happy blogging. */
/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');
/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'vendor/autoload.php');
require_once(ABSPATH . 'wp-settings.php');
