<?php
  include('../wp-config.php');

  $redis_host = WP_REDIS_HOST;
  $redis_port = WP_REDIS_PORT;

  $redis = new Redis();
  $redis->connect($redis_host, $redis_port);

  if(strtoupper($_SERVER["REQUEST_METHOD"]) === 'POST') {
    $success = false;
    if(isset($_POST['data']['redis-key'])) {
      $count = $redis->del($_POST['data']['redis-key']);

      if($count === 1) {
        $success = true;
      }
    }

    if($success) {
      header("HTTP/1.1 200 OK");
    }
    else {
      header("Status: 404 Not Found");
    }
    die();
   }

  $it = NULL;
  $redis->setOption(Redis::OPT_SCAN, Redis::SCAN_RETRY);
  $long_store_keys = [];
  while($arr_keys = $redis->scan($it, 'RL:IM:*' )) {
    foreach($arr_keys as $str_key) {

      $key_data = $redis->get($str_key);
      $json = json_decode($key_data);

      if($json !== false && isset($json->long_blocked) && $json->long_blocked === 1) {
        $long_store_keys[] = $str_key;
      }
    }
  }
?>
<html>
  <head>
    <title>Administrate Rate Limit</title>
    <script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
    <script>
      $(function() {
        $('.delete-action input[type="button"]').click(function() {
          var row = $(this).closest('tr');
          var redisKey = $('.redis-key', row).text();
          $.post('rate-limit.php', {
            type: "POST",
            url: "rate-limit.php",
            data: { 'redis-key': redisKey },
            success: function() {
              row.fadeOut(300, function() {
                $(this).remove();
              });
            },
            error: function() {
              console.log('Error, could not remove key.');
            }
          });
        });
      });
      </script>
  </head>

  <body>
    <h1>Administrate Rate Limit</h1>
    <p>Only keys that have been long term blocked will show up here.</p>

    <table border="1" cellspacing="2" cellpadding="5">
      <tr>
        <th>Key</th>
        <th></th>
      </tr>
      <?php foreach($long_store_keys as $key) { ?>
        <tr>
          <td class="redis-key"><?php print $key; ?></td>
          <td class="delete-action"><input type="button" value="Delete" /></td>
        </tr>
      <?php } ?>
    </table>
  </body>
</html>