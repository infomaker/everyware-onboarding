<?php namespace Customer;

use Everyware\PropertyParser;
use EwTools\Handler\OpenContent;
use EwTools\Twig\View;
use OcArticle;
use WP_Post;

/**
 * ArticleTemplate
 *
 * @link    http://infomaker.se
 * @package Customer
 * @since   Customer\ArticleTemplate 1.0.0
 */
abstract class ArticleTemplate {
    
    protected static $property_map = 'Article';
    
    /**
     * Contains the article to fetch data from
     *
     * @var EwcArticle
     */
    protected $article;
    
    /**
     * ArticleTemplate constructor.
     *
     * @param EwcArticle $article
     * @since 0.1
     */
    public function __construct( EwcArticle $article ) {
        $this->article = $article;
    }
    
    /**
     * Retrieve data to be used in the View
     *
     * @since 1.0.0
     * @return array
     */
    abstract public function getViewData();
    
    /**
     * Function fetch an article and create an instance of requested class
     *
     * @param string $uuid
     * @param array  $properties
     *
     * @return null|static
     * @since 0.1
     */
    public static function create( $uuid, array $properties = [] ) {
        $article      = OpenContent::getArticle( $uuid, $properties );
        if( $article instanceof OcArticle ) {
            return static::createFromOcArticle( $article );
        }
        
        return null;
    }
    
    /**
     * @param OcArticle $article
     *
     * @since 1.0.0
     * @return static
     */
    public static function createFromOcArticle( OcArticle $article ) {
        $property_parser = new PropertyParser( new Config() );
        
        return new static( $property_parser->fromOcObject( static::$property_map, $article ) );
    }
    
    /**
     * Function create an article from the post type
     *
     * @param WP_Post $post
     * @param array   $properties
     *
     * @return null|static
     * @since    0.1
     */
    public static function createFromPost( WP_Post $post, array $properties = [] ) {
        return static::create( get_post_meta( $post->ID, 'oc_uuid', true ), $properties );
    }
    
    /**
     * @param EwcArticle $article
     * @param string     $template
     *
     * @since 1.0.0
     * @return void
     */
    public static function render( EwcArticle $article, $template = '@base/page/single-article' ) {
        $article_template = new static( $article );
        View::render( $template, $article_template->getViewData() );
    }
}