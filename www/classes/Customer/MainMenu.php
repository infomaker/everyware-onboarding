<?php

namespace Customer;

use EwTools\Handler\Menus;
use EwTools\Twig\View;

class MainMenu
{
    private static $template = '@base/page/part/main-menu';

    /**
     * Render main menu
     */
    public static function render()
    {
        View::render(self::$template, [
            'main_menu' => self::getTemplateData(),
        ]);
    }

    /**
     * Retrieve data to be used in the template
     *
     * @since 1.0.0
     * @return array
     */
    public static function getTemplateData()
    {
        $mobileMenu = Menus::getFromLocation('main-menu');

        if ( ! $mobileMenu) {
            return ['elements' => []];
        }

        return [
            'elements' => $mobileMenu->items(false)->toArray(),
        ];
    }

    /**
     * Setup menu item
     *
     * @param $item
     *
     * @return array
     */
    private function setupMenuItem($item)
    {
        return [
            'title' => $item['title'],
            'url'   => $item['url'],
        ];
    }
}