<?php namespace Customer\Utilities;

use Customer\EwcDate;
use EveryBoard_Renderer;
use Everyware\Helpers\AuthHelper;
use EwTools\Support\Utilities;
use TuloPaywall\TuloPaywallSettings;

/**
 * TwigFunctions
 *
 * @link    http://infomaker.se
 * @package Customer\Utilities
 * @since   Customer\Utilities\TwigFunctions 0.1
 */
class TwigFunctions {
    protected static $breakpoints = [
        'x-small'   => '(min-width: 480px)',
        'smaller'   => '(min-width: 640px)',
        'small'     => '(min-width: 768px)',
        'medium'    => '(min-width: 1024px)',
        'large'     => '(min-width: 1280px)',
        'x-large'   => '(min-width: 1366px)',
        'huge'      => '(min-width: 1600px)'
    ];

    /**
     * Determines if in production
     *
     * @since 1.0.0
     * @return bool
     */
    public function inProduction() {
        return is_prod();
    }

    /**
     * Determines if in dev
     *
     * @return bool
     */
    public function isDev()
    {
        return Utilities::isDev();
    }

    /**
     * Determines if in stage
     *
     * @return bool
     */
    public function isStage()
    {
        $env = getenv('ENVIRONMENT');

        return $env === 'stage';
    }
    
    /**
     * Convert an array of strings into a class string
     *
     * @param $classes
     *
     * @since 1.0.0
     * @return string
     */
    public function renderClasses( $classes ) {
        return implode( ' ', array_filter( (array)$classes ) );
    }
    
    /**
     * Use Wordpress loop to render post content
     *
     * @since 1.0.0
     * @return void
     */
    public function theContent() {
        while ( have_posts() ) : the_post();
            the_content();
        endwhile;
    }
    
    /**
     * Load the mega menu template part
     *
     * @since 1.0.0
     * @return void
     */
    public function getMegaMenu() {
        get_template_part('partials/mega-menu');
    }
    
    /**
     * @param EwcDate $date
     * @param bool    $short
     *
     * @since 1.0.0
     * @return string
     */
    public function weatherDate( EwcDate $date, $short=false ) {
        return $date->toWeatherDate();
    }

    /**
     * Pretty-print args passed to the function
     * @since 1.0.0
     * @return void
     */
    public function dump() {
        print_r( '<pre>' );
        var_dump( func_get_args() );
        print_r( '</pre>' );
    }
    
    /**
     * Pretty-print args passed to the function and die
     *
     * @since 1.0.0
     * @return void
     */
    public function dieDump() {
        dd(func_get_args());
    }

    /**
     * Get breakpoint
     *
     * @param string    $media
     * @return string
     */
    public function getMedia( $media ) {
        return self::$breakpoints[$media];
    }


    /**
     * Change long url like string into breakable string
     *
     * @param string $string
     * @return string
     */
    public function setBreakable($string) {
        $toReplace = ['.', '/', '@'];

        $replacements = array_map(function ($element) {
            return '<wbr>'.$element;
        }, $toReplace);


        return str_replace($toReplace, $replacements, $string);
    }

    /**
     * Determine if a user is logged in
     *
     * @return bool
     */
    public function isUserLoggedIn()
    {
        return AuthHelper::isUserLoggedIn();
    }

    /**
     *  Determine if a user has access
     *
     * @return bool
     */
    public function userHasAccess()
    {
        return AuthHelper::hasUserAccess();
    }

    /**
     * Render board
     *
     * @param $postId
     *
     * @return string
     */
    public function boardRender($postId)
    {
        $everyBoardRenderer = new EveryBoard_Renderer();
        $boardJson          = get_post_meta($postId, 'everyboard_json');

        return $everyBoardRenderer->render($boardJson);
    }

    /**
     * Determine if paywall is active
     *
     * @return bool
     */
    public function paywallActive()
    {
        return TuloPaywallSettings::isPaywallActive();
    }

    /**
     * Get locked article board
     *
     * @return string
     */
    public function lockedArticleOffer()
    {
        return TuloPaywallSettings::getLockedArticleOffer();
    }

    /**
     * Get locked article board no access
     *
     * @return string
     */
    public function lockedArticleOfferNoAccess()
    {
        return TuloPaywallSettings::getLockedArticleNoAccessOffer();
    }

    /**
     * Get login offer
     *
     * @return string
     */
    public function loginOffer()
    {
        return TuloPaywallSettings::getLoginOffer();
    }

    /**
     * Determine if plus badges should be presented
     *
     * @return bool
     */
    public function showPlusBadges()
    {
        return TuloPaywallSettings::showPlusBadges();
    }

    /**
     * Determine if we should show plus badges for logged in users
     *
     * @return bool
     */
    public function showPlusBadgesForLoggedInUsers()
    {
        return TuloPaywallSettings::showPlusBadgesForLoggedInUsers() && AuthHelper::hasUserAccess();
    }
}