<?php

namespace Customer;

use Everyware\OpenContentProvider;
use EwTools\Handler\OpenContentQueryBuilder;

class ScrollPackage
{
    private $article;
    private $limit = 10;
    private $fetched_articles = [];
    private $fetched_uuids = [];


    public function __construct(EwcArticle $article, $limit = 10)
    {
        $this->article = $article;
        $this->limit   = $limit;

        $this->setFetchUuid($article);
        $this->fetchArticles();
    }

    /**
     * Setup articles for scroll package by rules
     */
    private function fetchArticles()
    {
        // Linked articles
        $linkedArticles = $this->getRelatedArticles($this->getLimitLeft());
        $this->appendArticles($linkedArticles);

        // Tags
        if ($this->getLimitLeft() > 0) {
            $tags = $this->getArticlesWithSameTags($this->getLimitLeft());
            $this->appendArticles($tags);
        }

        // Categories
        if ($this->getLimitLeft() > 0) {
            $categories = $this->getArticlesWithSameCategories($this->getLimitLeft());
            $this->appendArticles($categories);
        }
    }

    /**
     * Append articles to fetched article list
     *
     * @param array $articles
     */
    private function appendArticles(array $articles = [])
    {
        if (\count($articles) === 0) {
            return;
        }

        $this->fetched_articles = array_merge($this->fetched_articles, $articles);
    }

    /**
     * Get related articles based of rules
     *
     * @return array
     */
    public function getArticles()
    {
        return $this->fetched_articles;
    }

    /**
     * Get how many articles loaded
     *
     * @return int
     */
    public function getArticleCount()
    {
        return \count($this->fetched_articles);
    }

    /**
     * Get limit left
     *
     * @return int
     */
    private function getLimitLeft()
    {
        return $this->limit - $this->getArticleCount();
    }

    /**
     * Save already fetched uuids
     *
     * @param EwcArticle $article
     */
    private function setFetchUuid(EwcArticle $article)
    {
        $this->fetched_uuids[] = $article->uuid;
    }

    /**
     * Fetch from OC with query
     *
     * @param      $query
     * @param null $limit
     *
     * @return array
     */
    protected function fetchWithQuery($query, $limit = null)
    {
        $ocProvider = OpenContentProvider::setup([
            'q'         => $query,
            'limit'     => $limit,
            'sort.name' => 'Publiceringsdag',
        ])->setPropertyMap('Article');

        $search_result = $ocProvider->queryWithRequirements();

        if (is_array($search_result)) {
            return array_map(function ($oc_article) {
                $article = new EwcArticle($oc_article);

                $this->setFetchUuid($article);

                return $article;
            }, $search_result);
        }

        return [];
    }

    /**
     * Get articles with same tags
     *
     * @param $limit
     *
     * @return array
     */
    private function getArticlesWithSameTags($limit)
    {
        if ($limit === 0 || $this->article->getTags()->count() === 0) {
            return [];
        }


        $tagUuids = $this->article->getTags()->map(function ($item) {
            return $item->uuid;
        })->all();

        $query = OpenContentQueryBuilder::where('ConceptTagUuids', $tagUuids, 'OR');
        $query->andIfNotProperty('uuid', $this->fetched_uuids);
        $query->andIf('Pubdate:[NOW-21DAY TO NOW]');

        return $this->fetchWithQuery($query->buildQueryString(), $limit);
    }

    /**
     * Get articles with same categories
     *
     * @param $limit
     *
     * @return array
     */
    private function getArticlesWithSameCategories($limit)
    {
        if ($limit === 0 || $this->article->getCategories()->count() === 0) {
            return [];
        }

        $categoryUuids = $this->article->getCategories()->map(function ($item) {
            return $item->uuid;
        })->all();

        $query = OpenContentQueryBuilder::where('ConceptCategoryUuids', $categoryUuids, 'OR');
        $query->andIfNotProperty('uuid', $this->fetched_uuids);
        $query->andIf('Pubdate:[NOW-21DAY TO NOW]');

        return $this->fetchWithQuery($query->buildQueryString(), $limit);
    }

    /**
     * Get related articles from article
     *
     * @param $limit
     *
     * @return array
     */
    private function getRelatedArticles($limit)
    {
        if ($limit === 0 || $this->article->relatedArticles()->count() === 0) {
            return [];
        }

        $relatedArticlesUuids = $this->article->relatedArticles()->map(function ($item) {
            return $item->uuid;
        })->all();

        $query = OpenContentQueryBuilder::where('uuid', $relatedArticlesUuids, 'OR');
        $query->andIfNotProperty('uuid', $this->fetched_uuids);

        return $this->fetchWithQuery($query->buildQueryString(), $limit);
    }
}