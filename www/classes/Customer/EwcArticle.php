<?php namespace Customer;

use Carbon\Carbon;
use Everyware\handler\ArticleBodyParser;
use Everyware\Newsml\NewsMLTransformerManager;
use Everyware\Newsml\Objects\ImageItem;
use Everyware\PropertyObject;
use EwTools\Support\Collection;

/**
 * EwcArticle
 *
 * @property Collection articles
 * @property string     body_raw
 * @property string     contenttype
 * @property array      channels
 * @property string     dateline
 * @property string     headline
 * @property bool       hide_ads
 * @property bool       hide_comments
 * @property array      image_uuids
 * @property bool       is_plus
 * @property string     leadin
 * @property string     main_channel
 * @property string     permalink
 * @property EwcDate    pubdate
 * @property string     section
 * @property string     status
 * @property array      teaser_image_uuids
 * @property string     teaser_headline
 * @property string     teaser_raw
 * @property string     text
 * @property EwcDate    updated
 * @property string     post_id
 * @property string     uuid
 * @property string     type
 * @property int        life_span
 * @property int        news_score
 * @property Collection authors
 * @property Collection categories
 * @property Collection places
 * @property Collection stories
 * @property Collection tags
 * @link    http://infomaker.se
 * @package Customer
 * @since   Customer\EwcArticle 0.1
 */
class EwcArticle extends PropertyObject {
    
    public function __construct( PropertyObject $object ) {
        $this->fill( $object->toArray() );
    }
    
    /**
     * Contains an instance of the parser for the article
     *
     * @var ArticleBodyParser
     */
    protected $parser;
    
    /**
     * Retrieve an array
     *
     * @since 1.0.0
     * @return array
     */
    public function getParsedBody() {
        return $this->getBodyParser()->getBody();
    }
    
    /**
     * @since 1.0.0
     * @return ArticleBodyParser
     */
    public function getBodyParser() {
        if( $this->parser === null ) {
            $this->parser = new ArticleBodyParser( $this );
        }
        
        return $this->parser;
    }
    
    public function getFirstOfSingleValue( $selectors ) {
        return $this->getFirstOf( $selectors, true );
    }
    
    public function getFirstOfMultiValue( $selectors ) {
        return $this->getFirstOf( $selectors, false );
    }
    
    public function getFirstOf( $selectors, $single_value = true ) {
        $selectors = \is_array( $selectors ) ? $selectors : $this->parseSelectors( $selectors );
        foreach ( $selectors as $selector ) {
            $value = $single_value ? $this->getSingleValue( $selector ) : $this->getMultiValue( $selector );
            if( ! empty( $value ) ) {
                return $value;
            }
        }
        
        return $single_value ? '' : [];
    }
    
    /**
     * Parse string of selectors into an array
     * Multiple selector may come separated by space, comma or pipe
     *
     * @param string $selectors
     *
     * @since 0.1
     * @return array
     */
    protected function parseSelectors( $selectors = '' ) {
        return (array)preg_split( '/\s*(,|\|)\s*/', $selectors );
    }
    
    /**
     * @since 1.0.0
     * @return mixed|null
     */
    public function getTeaserParser() {
        if(empty($this->teaser_raw)){
            return null;
        }
        $transformer = NewsMLTransformerManager::createObjectTransformer( 'teaser_raw' );
        $result      = $transformer->transform( $this );
        return \count( $result ) > 0 ? array_shift( $result ) : null;
    }
    
    /**
     * @since 1.0.0
     * @return Image|null
     */
    public function getTeaserDefaultImage() {
        $parsed_body = $this->getBodyParser();
        $item        = $parsed_body->firstOfType( 'x-im/image' );
        
        if( $item instanceof ImageItem ) {
            return new Image( $item );
        }
        
        return null;
    }
    
    public function hasRelatedArticles() {
        return $this->articles->isNotEmpty();
    }
    
    /**
     * Retrieve related categories
     *
     * @since 1.0.0
     * @return Collection
     */
    public function getCategories() {
        return Collection::make( $this->categories )->unique( 'name' );
    }
    
    /**
     * Retrieve related tags
     *
     * @since 1.0.0
     * @return Collection
     */
    public function getTags() {
        return Collection::make( $this->tags )->unique( 'name' );
    }
    
    /**
     * Retrieve related authors
     *
     * @since 1.0.0
     * @return Collection
     */
    public function getAuthors() {
        return Collection::make( $this->authors );
    }

    /**
     * Retrieve related stories
     *
     * @since 1.0.0
     * @return Collection
     */
    public function getStories()
    {
        return Collection::make($this->stories)->unique('name');
    }

    /**
     * Retrieve channels
     *
     * @since 1.0.0
     * @return Collection
     */
    public function getChannels()
    {
        return Collection::make($this->channels);
    }

    /**
     * Retrieve channels
     *
     * @since 1.0.0
     * @return Collection
     */
    public function getPlaces()
    {
        return Collection::make($this->places)->unique('name');
    }
    
    /**
     * Retrieve related article by uuid
     *
     * @param string $uuid
     *
     * @since 1.0.0
     * @return static|null
     */
    public function getRelatedArticle( $uuid = '' ) {
        $articles = $this->relatedArticles()->whereIn( 'uuid', $uuid );
        
        return $articles->isNotEmpty() ? $articles->first() : null;
    }
    
    /**
     * Retrieve related Articles
     *
     * @since 1.0.0
     * @return Collection
     */
    public function relatedArticles() {
        return Collection::make( $this->articles );
    }
    
    /**
     * @since 1.0.0
     * @return array
     */
    public function getLeadin() {
        return $this->getBodyParser()->allOfType( 'preamble' );
    }

    /**
     * Get pub date
     *
     * @since 1.0.0
     * @return EwcDate
     */
    public function getPubDate()
    {
        return EwcDate::parse($this->pubdate);
    }
    
    /**
     * @since 1.0.0
     * @return array
     */
    public function getLifespan() {
        $pubStart = $this->pubdate->copy();
        $lifeSpan = empty( $this->life_span ) ? 0 : $this->life_span;

        return [
            'start'       => $pubStart->toIso8601String(),
            'stop'        => $pubStart->addSeconds( $lifeSpan )->toIso8601String(),
            'description' => ( $lifeSpan / 60 ) / 60 . 'H', // Convert to Hours
            'duration'    => $lifeSpan
        ];
    }
}