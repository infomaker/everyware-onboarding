<?php namespace Customer;

use Everyware\Newsml\NewsMLTransformerManager;
use Everyware\Newsml\Parsers\ContentPartParser;
use Everyware\Newsml\Parsers\ElementParser;
use Everyware\Newsml\Parsers\HtmlEmbedParser;
use Everyware\Newsml\Parsers\ImageGalleryParser;
use Everyware\Newsml\Parsers\ImageParser;
use Everyware\Newsml\Parsers\LinkObjectParser;
use Everyware\Newsml\Parsers\PdfParser;
use Everyware\Newsml\Parsers\ReviewParser;
use Everyware\Newsml\Parsers\SocialEmbedParser;
use Everyware\Newsml\Parsers\TableParser;
use Everyware\Newsml\Parsers\TeaserParser;
use Everyware\Newsml\Parsers\YouplayParser;
use Everyware\Newsml\Parsers\YoutubeParser;
use Everyware\OpenContentProvider;
use Everyware\PropertyMapper;
use Everyware\Helpers\RequestHelper;
use EwTools\Handler\FeedRouter;
use EwTools\Handler\Menus;
use EwTools\Handler\Sidebars;
use EwTools\Metaboxes\EwPageMeta;
use EwTools\Models\Section;
use EwTools\Models\SettingsParameter;
use EwTools\ProjectStartup;
use EwTools\Support\Device;
use EwTools\Support\Str;
use EwTools\Support\Utilities;
use EwWidgets\LinkedImage;
use EwWidgets\PageContent;
use EwWidgets\SectionHeader;
use EwWidgets\TextBlock;

/**
 * Startup
 *
 * @link    http://infomaker.se
 * @package Customer
 * @since   Customer\Startup 1.0.0
 */
class Startup extends ProjectStartup {
    
    /**
     * Contains the customers config
     *
     * @var Config
     */
    private $config;
    
    /**
     * Project Setup
     *
     * @since 0.1
     * @return void
     */
    protected function setup() {
        $this->config = new Config();
        
        $this->setupOpenContent( $this->config );
        $this->setupNewsMLParser();
        $this->routeCustomUrls();
        
        $this->setupFilters()
             ->setupMenus()
             ->removeHeadLinks();
        $this->setupSidebars();
        
        $this->setupCustomerConstants();
        $this->setupScripts();
        $this->setupMetaboxes();
        $this->setupWidgets();
        $this->useToolsAdminStyle();
        
        $this->setupSearchPage();
        $this->setSiteTheme();
        
        $this->setupAjax();
        $this->setupRssRoute( 'route' );

        EwcTwig::setup();
    }
    
    /**
     * Setup constants for the project
     *
     * @return $this
     */
    private function setupCustomerConstants() {
        \define( 'EW_AUTH', array_get( $_SERVER, 'HTTP_X_EW_AUTH', false ) );
        \define( 'EW_TULO_AUTH', (bool)array_get( $_SERVER, 'HTTP_X_EW_TULOAUTH', false ) );
        \define( 'EW_TULO_PRODUCTS', array_get( $_SERVER, 'HTTP_X_EW_TULOPRODUCTS', false ) );
        \define( 'EW_AUTH_CLIENT', array_get( $_SERVER, 'HTTP_X_EW_AUTHCLIENT', false ) );
        \define( 'EW_AUTH_ENVIRONMENT', array_get( $_SERVER, 'HTTP_X_EW_AUTHENVIRONMENT', false ) );
        \define( 'EW_AUTHENTICATED', (bool)array_get( $_SERVER, 'HTTP_X_EW_AUTH', false ) );
        
        return $this;
    }
    
    /**
     * Setup filters
     *
     * @return $this
     */
    private function setupFilters() {
        # Hide admin bar
        add_filter( 'show_admin_bar', '__return_false' );
        
        add_filter( 'script_loader_src', [ $this, 'useStaticUrl' ] );
        add_filter( 'style_loader_src', [ $this, 'useStaticUrl' ] );
        
        return $this;
    }
    
    /**
     * @param $url
     *
     * @since 1.0.0
     * @return string
     */
    public function useStaticUrl( $url ) {
        if( is_admin() || ! Str::startsWith( $url, site_url() . '/wp-content/' ) ) {
            return $url;
        }
        
        return Utilities::getCdnUri( $url );
    }
    
    /**
     * @see   https://digwp.com/2009/06/xmlrpc-php-security/
     * @since 1.0.0
     * @return $this
     */
    private function removeHeadLinks() {
        add_action( 'init', function () {
            remove_action( 'wp_head', 'rsd_link' );
            remove_action( 'wp_head', 'wlwmanifest_link' );
        } );
        
        return $this;
    }
    
    /**
     * Setup scripts
     *
     * @return $this
     */
    private function setupScripts() {

        // Front
        add_action( 'wp_enqueue_scripts', function () {
            $script_version = is_dev() ? false : GIT_COMMIT;

            // Remove jquery
            wp_deregister_script( 'jquery' );

            // Remove the bootstrap version added by Everyboard
            wp_deregister_style( 'bootstrap-css' );
            wp_deregister_script( 'bootstrap-js' );
            wp_deregister_style( 'wp-core-ui' );

            wp_enqueue_style( 'google-font', $this->getFontsUrl(), [] );

            wp_enqueue_style('tryout-style', $this->getStyleUrl('style'), [], $script_version);
            wp_enqueue_script('tryout-body-js', $this->getScriptUrl('body'), [], $script_version, true);

            wp_localize_script('tryout-body-js', 'infomaker', [
                'ajaxurl' => site_url('/ajax.php'),
            ]);
        });

        // Admin
        add_action( 'admin_enqueue_scripts', function() {
            $script_version = is_dev() ? false : GIT_COMMIT;
            wp_enqueue_script( 'tryout-admin-js', $this->getScriptUrl( 'admin' ), [], $script_version, true );
        });

        return $this;
    }
    
    /**
     * Get fonts url
     *
     * @return string
     */
    private function getFontsUrl() {
        return "https://fonts.googleapis.com/css?family=Lato:700,900,regular%7CPlayfair+Display:900%7CMerriweather:700italic,italic&amp;subset=latin,latin-ext,latin,latin-ext,latin,latin-ext";
    }
    
    /**
     * Get script url
     *
     * @param $file_name
     *
     * @return string
     */
    private function getScriptUrl( $file_name ) {
        return Utilities::getStaticThemeUri() . Str::finish( "/assets/js/{$file_name}", Utilities::isDev() ? '.js' : '.min.js' );
    }
    
    /**
     * Get style url specific to the theme
     * ex. "https://components.gcp.stp.nwt.se/css/nwt-style.css";
     *
     * @param $file_name
     *
     * @return string
     */
    private function getStyleUrl( $file_name ) {
        return Utilities::getStaticThemeUri() . Str::finish( "/assets/css/{$file_name}", Utilities::isDev() ? '.css' : '.min.css' );
    }
    
    /**
     * Setup menus
     *
     * @return $this
     */
    private function setupMenus() {
        
        Menus::init( [
            'head'   => [
                'id'          => 'main-menu',
                'description' => 'Main menu'
            ],
        ] );
        
        add_filter( 'wp_setup_nav_menu_item', function ( $menu ) {
            if( $menu->type === 'custom' ) {
                $menu->url = Str::finish( $menu->url, '/' );
            }
            
            return $menu;
        } );
        
        return $this;
    }
    
    /**
     * Setup metaboxes
     *
     * @return $this
     */
    private function setupMetaboxes() {
        EwPageMeta::setup();
        
        return $this;
    }
    
    /**
     * Setup widgets
     *
     * @return $this
     */
    private function setupWidgets() {
        TextBlock::register();
        SectionHeader::register();
        PageContent::register();
        LinkedImage::register();

        return $this;
    }
    
    /**
     * Setup sidebars
     */
    private function setupSidebars() {
        // Article sidebars:
        // ======================================================
        Sidebars::register( 'ewc-single-article-side', [
            'name'        => 'Article side',
            'description' => 'Container rendered in right column next to each article.',
        ] );
    }
    
    /**
     * Set site theme based on first slug part
     */
    private function setSiteTheme() {
        if( is_admin() ) {
            return;
        }
        
        $section_class = Section::getClassBySlug( RequestHelper::segment( 1 ) );
        
        if( $section_class !== '' ) {
            add_filter( 'body_class', function ( $classes ) use ( $section_class ) {
                return array_merge( $classes, [ $section_class ] );
            } );
        }
    }
    
    /**
     * Redirect default searches to search/query searches
     *
     * @param string $page_name
     */
    private function setupSearchPage( $page_name = 'Sök' ) {
        $page_slug = Str::slug( $page_name );
        
        // Convert WP-search to custom search for safety
        add_action( 'template_redirect', function () use ( $page_slug ) {
            $query = $_GET;
            if( isset( $query[ 's' ] ) ) {
                $query[ 'q' ] = $query[ 's' ];
                unset( $query[ 's' ] );
                wp_redirect( home_url( "/{$page_slug}/?" ) . http_build_query( $query ) );
                exit();
            }
        } );
    }
    
    /**
     * Setup Ajax functionality
     *
     * @since 1.0.0
     * @return void
     */
    private function setupAjax() {
    }
    
    private function setupRssRoute( $route_var ) {
        FeedRouter::init();
        
        add_filter( 'query_vars', function ( $vars ) use ( $route_var ) {
            $vars[] = $route_var;
            
            return $vars;
        } );
        
        add_filter( 'request', function ( $query ) use ( $route_var ) {
            return isset( $query[ $route_var ] ) ? $_GET : $query;
        }, 10, 1 );
    }
    
    /**
     * Configure Open Content requests with Customer config
     *
     * @param Config $config
     *
     * @since 1.0.0
     * @return void
     */
    private function setupOpenContent( Config $config ) {
        $property_map       = new PropertyMapper( $config );
        $article_properties = $property_map->requiredProperties( 'Article' );
        OpenContentProvider::setupDefaultSearchProperties( $article_properties );
        OpenContentProvider::setupNotifierProperties( 'Article', $article_properties );
        OpenContentProvider::setupNotifierProperties( 'Concept', $property_map->requiredProperties( 'Concept' ) );
    }

    /**
     * Setup NewsML parser
     */
    private function setupNewsMLParser() {
        NewsMLTransformerManager::registerObjectParser( SocialEmbedParser::OBJECT_TYPE, new SocialEmbedParser() );
        NewsMLTransformerManager::registerObjectParser( TeaserParser::OBJECT_TYPE, new TeaserParser() );
        NewsMLTransformerManager::registerObjectParser( ImageParser::OBJECT_TYPE, new ImageParser() );
        NewsMLTransformerManager::registerObjectParser( ImageGalleryParser::OBJECT_TYPE, new ImageGalleryParser() );
        NewsMLTransformerManager::registerObjectParser( TableParser::OBJECT_TYPE, new TableParser() );
        NewsMLTransformerManager::registerObjectParser( ContentPartParser::OBJECT_TYPE, new ContentPartParser( new ElementParser() ) );
        NewsMLTransformerManager::registerObjectParser( ReviewParser::OBJECT_TYPE, new ReviewParser( new ElementParser() ) );
        NewsMLTransformerManager::registerObjectParser( LinkObjectParser::OBJECT_TYPE, new LinkObjectParser() );
        NewsMLTransformerManager::registerObjectParser( HtmlEmbedParser::OBJECT_TYPE, new HtmlEmbedParser() );
        NewsMLTransformerManager::registerObjectParser( PdfParser::OBJECT_TYPE, new PdfParser() );
        NewsMLTransformerManager::registerObjectParser( YoutubeParser::OBJECT_TYPE, new YoutubeParser() );
        NewsMLTransformerManager::registerObjectParser( YouplayParser::OBJECT_TYPE, new YouplayParser() );
    }
    
    /**
     * @param array $query_vars
     *
     * @since 1.0.0
     * @return array
     */
    private function routeArchiveUrls( array $query_vars = [] ) {
        if( isset( $query_vars[ 'year' ] ) ) {
            $query_vars[ 'post_type' ] = 'article';
        }
        
        return $query_vars;
    }
    
    /**
     * @since 1.0.0
     * @return void
     */
    private function routeCustomUrls() {
        add_filter( 'request', function ( array $query_vars = [] ) {
            $query_vars = $this->routeArchiveUrls( $query_vars );
            
            return $query_vars;
        } );
    }
}