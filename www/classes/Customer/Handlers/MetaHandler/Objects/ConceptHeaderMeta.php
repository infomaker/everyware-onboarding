<?php

namespace Customer\Handlers\MetaHandler\Objects;

use Customer\EwcConcept;

class ConceptHeaderMeta extends BaseHeaderMeta
{
    protected $concept;

    public function __construct(EwcConcept $concept)
    {
        $this->concept = $concept;
        $this->setupProperties();
    }

    /**
     * Get permalink
     *
     * @return string
     */
    public function getPermalink()
    {
        return $this->concept->permalink;
    }

    /**
     * Setup properties
     */
    public function setupProperties()
    {
        $this->title              = $this->getTitle();
        $this->description        = $this->getDefaultDescription();
        $this->contentType        = 'concept';
        $this->recommendable      = 'false';
        $this->presentIsPlus      = false;
        $this->presentPublishTime = false;
    }

    /**
     * Get object
     *
     * @return \Customer\EwcConcept
     */
    public function getObject()
    {
        return $this->concept;
    }
}