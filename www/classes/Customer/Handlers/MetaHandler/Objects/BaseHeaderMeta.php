<?php

namespace Customer\Handlers\MetaHandler\Objects;

use Customer\Handlers\MetaHandler\Traits\MetaTrait;
use EwTools\Support\Str;

abstract class BaseHeaderMeta
{
    use MetaTrait;

    abstract public function getPermalink();

    abstract public function setupProperties();

    abstract public function getObject();

    /**
     * Get default title
     *
     * @return string
     */
    public function getDefaultTitle()
    {
        return Str::append($this->getCachedSiteMeta('name'), $this->appendSiteTitle(get_the_title()), ' - ');
    }

    /**
     * Get Default description
     *
     * @return mixed|string
     */
    public function getDefaultDescription()
    {
        return $this->getCachedSiteMeta('description');
    }

    public function appendSiteTitle($string)
    {
        return Str::append($this->getCachedSiteMeta('name'), html_entity_decode($string), ' - ');
    }

    /**
     * Get cached site meta
     *
     * @param string $metaName
     *
     * @return mixed|string
     */
    protected function getCachedSiteMeta($metaName = '')
    {
        $cacheKey = "site-{$metaName}";

        // Use cached data if any
        if (false === ($metaInfo = get_transient($cacheKey)) && false !== ($metaInfo = get_bloginfo($metaName))) {
            set_transient($cacheKey, $metaInfo, HOUR_IN_SECONDS);
        }

        return $metaInfo;
    }
}