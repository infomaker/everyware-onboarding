<?php

namespace Customer\Handlers\MetaHandler\Objects;

use Customer\EwcArticle;
use Everyware\Newsml\Objects\TeaserItem;

class ArticleHeaderMeta extends BaseHeaderMeta
{
    protected $article;

    public function __construct(EwcArticle $article)
    {
        $this->article = $article;

        $this->setupProperties();
    }

    /**
     * Setup properties
     */
    public function setupProperties()
    {
        $this->title            = $this->setTitle();
        $this->uuid             = $this->article->uuid;
        $this->categories       = $this->article->getCategories();
        $this->tags             = $this->article->getTags();
        $this->places           = $this->article->getPlaces();
        $this->authors          = $this->article->getAuthors();
        $this->newsScore        = $this->article->news_score;
        $this->lifeSpan         = $this->article->getLifespan();
        $this->channels         = $this->article->getChannels();
        $this->mainChannel      = $this->article->main_channel;
        $this->stories          = $this->article->getStories();
        $this->isPlus           = $this->article->is_plus;
        $this->contentType      = $this->article->contenttype;
        $this->publishTime      = $this->article->pubdate;
        $this->modificationTime = $this->article->updated;
        $this->articleType      = $this->article->type;
        $this->pageType         = 'article';
        $this->twitterHandle    = $this->setTwitterHandle();
        $this->description      = $this->setDescription();
    }

    /**
     * Get permalink
     *
     * @return string
     */
    public function getPermalink()
    {
        return $this->article->permalink;
    }

    /**
     * Set Twitter handle
     *
     * @return array
     */
    private function setTwitterHandle()
    {
        return $this->article->getAuthors()->map(function ($author) {
            return $author->twitter['handle'];
        })->filter()->toArray();
    }

    /**
     * Set title
     *
     * @return string
     */
    private function setTitle()
    {
        $teaser = $this->article->getTeaserParser();

        if ($teaser instanceof TeaserItem) {
            return $teaser->headline ?: $this->article->headline;
        }

        return $this->article->headline;
    }

    /**
     * Get object
     *
     * @return \Customer\EwcArticle
     */
    public function getObject()
    {
        return $this->article;
    }

    /**
     * Set description
     *
     * @return string
     */
    private function setDescription()
    {
        $teaser = $this->article->getTeaserParser();

        if ($teaser instanceof TeaserItem) {
            return $teaser->text ?: $this->article->leadin;
        }

        return $this->article->leadin;
    }
}