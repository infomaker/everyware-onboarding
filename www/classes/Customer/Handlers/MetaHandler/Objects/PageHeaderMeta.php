<?php

namespace Customer\Handlers\MetaHandler\Objects;

use EwTools\Metaboxes\EwPageMeta;
use EwTools\Models\Page;
use EwTools\Support\Str;

class PageHeaderMeta extends BaseHeaderMeta
{
    protected $page;

    public function __construct(Page $page)
    {
        $this->page = $page;

        $this->setupProperties();
    }

    public function getPermalink()
    {
        return $this->page->permalink();
    }

    public function setupProperties()
    {
        $this->title              = $this->getPageTitle();
        $this->description        = $this->getPageDescription();
        $this->pageType           = 'website';
        $this->contentType        = 'page';
        $this->recommendable      = 'false';
        $this->presentIsPlus      = false;
        $this->presentPublishTime = false;
    }

    /**
     * Get site title for page
     *
     * @return string
     */
    protected function getPageTitle()
    {
        $title = '';

        if ($this->page !== null) {
            $seo_title = EwPageMeta::getMetaData($this->page, 'title');
            $title     = $seo_title ?: '';
        }

        if (Str::isEmpty($title)) {
            return $this->getTitle();
        }

        return $title;
    }

    /**
     * Get page meta
     *
     * @return string
     */
    protected function getPageDescription()
    {
        $description = '';

        if ($this->page !== null) {
            $seo_description = EwPageMeta::getMetaData($this->page, 'description');
            $description     = $seo_description ?: '';
        }

        return Str::notEmpty($description) ? $description : $this->getDefaultDescription();
    }

    /**
     * Get object
     *
     * @return \EwTools\Models\Page
     */
    public function getObject()
    {
        return $this->page;
    }
}