<?php

namespace Customer\Handlers\MetaHandler\Traits;

trait MetaTrait
{
    public $uuid;
    public $categories = [];
    public $tags = [];
    public $places = [];
    public $authors = [];
    public $newsScore;
    public $lifeSpan = [];
    public $channels = [];
    public $mainChannel;
    public $stories = [];
    public $isPlus;
    public $contentType;
    public $publishTime;
    public $modificationTime;
    public $articleType;
    public $title;
    public $description;
    public $pageType;
    public $twitterHandle;
    public $recommendable;
    public $presentPublishTime = true;
    public $presentIsPlus = true;

    public function __get($name)
    {
        return $this->$name;
    }

    public function __isset($name)
    {
        return isset($this->$name);
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    /**
     * @return bool
     */
    public function getPresentPublishTime()
    {
        return $this->presentPublishTime;
    }

    /**
     * @return bool
     */
    public function getPresentIsPlus()
    {
        return $this->presentIsPlus;
    }

    /**
     * @return mixed
     */
    public function getRecommendable()
    {
        return $this->recommendable;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title ?: get_the_title();
    }

    /**
     * @return mixed
     */
    public function getNewsScore()
    {
        return $this->newsScore;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @return array
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @return array
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @return array
     */
    public function getPlaces()
    {
        return $this->places;
    }

    /**
     * @return array
     */
    public function getAuthors()
    {
        return $this->authors;
    }

    /**
     * @return mixed
     */
    public function getNewScore()
    {
        return $this->newsScore;
    }

    /**
     * @return array
     */
    public function getLifeSpan()
    {
        return $this->lifeSpan;
    }

    /**
     * @return array
     */
    public function getChannels()
    {
        return $this->channels;
    }

    /**
     * @return mixed
     */
    public function getMainChannel()
    {
        return $this->mainChannel;
    }

    /**
     * @return array
     */
    public function getStories()
    {
        return $this->stories;
    }

    /**
     * @return mixed
     */
    public function getisPlus()
    {
        return $this->isPlus;
    }

    /**
     * @return mixed
     */
    public function getContentType()
    {
        return strtolower($this->contentType);
    }

    /**
     * @return mixed
     */
    public function getPublishTime()
    {
        return $this->publishTime;
    }

    /**
     * @return mixed
     */
    public function getModificationTime()
    {
        return $this->modificationTime;
    }

    /**
     * @return mixed
     */
    public function getArticleType()
    {
        return $this->articleType;
    }

    /**
     * @return mixed
     */
    public function getPageType()
    {
        return $this->pageType;
    }

    /**
     * @return mixed
     */
    public function getTwitterHandle()
    {
        return $this->twitterHandle;
    }
}