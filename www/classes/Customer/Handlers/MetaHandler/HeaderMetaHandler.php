<?php

namespace Customer\Handlers\MetaHandler;

use Customer\EwcArticle;
use Customer\Handlers\MetaHandler\Objects\ArticleHeaderMeta;
use Customer\Handlers\MetaHandler\Objects\BaseHeaderMeta;
use Customer\Handlers\MetaHandler\Source\Cxsense;
use Customer\Handlers\MetaHandler\Source\Facebook;
use Customer\Handlers\MetaHandler\Source\General;
use Customer\Handlers\MetaHandler\Source\Twitter;

class HeaderMetaHandler
{
    protected $metaObject;
    protected $metaData = [];

    public function __construct(BaseHeaderMeta $metaObject)
    {
        $this->metaObject = $metaObject;
    }

    /**
     * Sets up all general meta
     *
     * @return $this
     */
    public function setupGeneral()
    {
        $general = new General($this->metaObject);

        $this->metaData = array_merge($this->metaData, $general->getMeta());

        return $this;
    }

    /**
     * Setup Cxsense data
     *
     * @return $this
     */
    public function setupCxsenseData()
    {
        $cxsense = new Cxsense($this->metaObject);

        $this->metaData = array_merge($this->metaData, $cxsense->getMeta());

        return $this;
    }

    /**
     * Setup Facebook data
     *
     * @return $this
     */
    public function setupFacebookData()
    {
        $facebook = new Facebook($this->metaObject);

        $this->metaData = array_merge($this->metaData, $facebook->getMeta());

        return $this;
    }

    /**
     * Setup Twitter data
     *
     * @return $this
     */
    public function setupTwitterData()
    {
        $twitter = new Twitter($this->metaObject);

        $this->metaData = array_merge($this->metaData, $twitter->getMeta());

        return $this;
    }

    /**
     * Append meta to header
     */
    public function appendData()
    {
        add_filter('header_meta', function ($header_meta) {
            return array_replace($header_meta, $this->metaData);
        });
    }
}