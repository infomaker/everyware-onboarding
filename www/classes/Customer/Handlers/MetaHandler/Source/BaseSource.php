<?php

namespace Customer\Handlers\MetaHandler\Source;

use Customer\Handlers\MetaHandler\Objects\BaseHeaderMeta;
use EwTools\Handler\Imengine;

abstract class BaseSource
{
    protected $metaObject;
    protected $metaPrefix = '';
    protected $metaData = [];

    public function __construct(BaseHeaderMeta $metaObject)
    {
        $this->metaObject = $metaObject;

        $this->setupMeta();
    }

    /**
     * Get meta data
     *ä
     * @return array
     */
    public function getMeta()
    {
        return $this->metaData;
    }

    /**
     * Setup meta data
     */
    abstract protected function setupMeta();

    /**
     * Add multi value
     *
     * @param        $values
     * @param        $metaName
     * @param string $propertyName
     */
    protected function addMultiValue($values, $metaName, $propertyName = 'property')
    {
        if ( ! is_array($values) || count($values) === 0) {
            return;
        }

        $metaData = collect($values)->map(function ($value) use ($metaName, $propertyName) {
            $name = $value;

            if (is_object($value)) {
                $name = $value->name;
            } elseif (is_array($value)) {
                $name = $value['name'];
            }

            return [
                $propertyName => "{$this->metaPrefix}:{$metaName}",
                'content'  => $name,
            ];
        });

        $this->mergeData($metaData->toArray());
    }

    /**
     * Setup single value
     *
     * @param        $value
     * @param        $metaName
     * @param string $propertyName
     * @param bool   $useRaw
     */
    protected function addSingleValue($value, $metaName, $propertyName = 'property', $useRaw = false)
    {
        if ( ! $value) {
            return;
        }

        $propertyKey = $this->metaPrefix ? "{$this->metaPrefix}:{$metaName}" : $metaName;

        $this->mergeData([
            $propertyKey => [
                $propertyName => $propertyKey,
                'content'  => $value,
                'use_raw' => $useRaw
            ],
        ]);
    }

    /**
     * Merge meta data together
     *
     * @param array $metaData
     */
    protected function mergeData(Array $metaData)
    {
        $this->metaData = array_merge($this->metaData, $metaData);
    }

    /**
     * Get teaser image url from article
     *
     * @return bool|null|string
     */
    protected function getTeaserImageUrl()
    {
        if ($this->metaObject->getContentType() === 'article') {
            /** @var \Customer\EwcArticle $article */
            $article = $this->metaObject->getObject();

            $imageUuid = $article->getFirstOf('teaser_image_uuids');

            if(empty($imageUuid)) {
                $imageUuid = $article->getFirstOf('image_uuids');
            }
        }

        if ( ! empty($imageUuid)) {
            return Imengine::ratio(1200, '16:9')->fromUuid($imageUuid);
        }

        return false;
    }
}