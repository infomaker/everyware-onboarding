<?php

namespace Customer\Handlers\MetaHandler\Source;

class General extends BaseSource
{

    /**
     * Setup meta data
     */
    protected function setupMeta()
    {
        $this->setupDescription();
    }

    protected function setupDescription()
    {
        $this->addSingleValue($this->metaObject->getDescription(), 'description', 'name');
    }
}