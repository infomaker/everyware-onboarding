<?php

namespace Customer\Handlers\MetaHandler\Source;

use Carbon\Carbon;
use Customer\Helpers\AssetsHelper;
use EwTools\Handler\Imengine;

class Facebook extends BaseSource
{
    protected $metaPrefix = 'og';

    protected function setupMeta()
    {
        $this->setupTitle()
             ->setupDescription()
             ->setupSiteName()
             ->setupAuthors()
             ->setupType()
             ->setupUrl()
             ->setupImage()
             ->setupLocale()
             ->setupPublishTime();
    }

    /**
     * Setup title
     *
     * @return $this
     */
    private function setupTitle()
    {
        $this->addSingleValue($this->metaObject->getTitle(), 'title');

        return $this;
    }

    /**
     * Setup description
     *
     * @return $this
     */
    private function setupDescription()
    {
        $this->addSingleValue($this->metaObject->getDescription(), 'description');

        return $this;
    }

    /**
     * Setup site name
     *
     * @return $this
     */
    private function setupSiteName()
    {
        $this->addSingleValue(get_bloginfo('title'), 'site_name');

        return $this;
    }

    /**
     * Setup type
     *
     * @return $this
     */
    private function setupType()
    {
        $this->addSingleValue($this->metaObject->getPageType(), 'type');

        return $this;
    }

    /**
     * Setup url
     *
     * @return $this
     */
    private function setupUrl()
    {
        $this->addSingleValue($this->metaObject->getPermalink(), 'url');

        return $this;
    }

    /**
     * Setup image for article
     *
     * @return $this
     */
    private function setupImage()
    {
        $image = $this->getTeaserImageUrl();

        if ($image) {
            $this->addSingleValue($image, 'image', 'property', true);
            $this->addSingleValue(1200, 'image:width');
            $this->addSingleValue(675, 'image:height');
            $this->addSingleValue('image/png', 'image:type');

            return $this;
        }

        // Default
        $this->addSingleValue(AssetsHelper::getFacebookDefaultImage(), 'image');
        $this->addSingleValue(1200, 'image:width');
        $this->addSingleValue(628, 'image:height');
        $this->addSingleValue('image/png', 'image:type');

        // Default x2
//        $this->addSingleValue(AssetsHelper::getFacebookDefaultImage2x(), 'image');
//        $this->addSingleValue(2400, 'image:width');
//        $this->addSingleValue(1256, 'image:height');
//        $this->addSingleValue('image/png', 'image:type');

        return $this;
    }

    /**
     * Setup locale
     *
     * @return $this
     */
    private function setupLocale()
    {
        $this->addSingleValue('sv_SE', 'locale');

        return $this;
    }

    /**
     * Setup authors
     *
     * @return $this
     */
    private function setupAuthors()
    {
        $this->addMultiValue($this->metaObject->getAuthors(), 'article:author');

        return $this;
    }

    /**
     * Setup publishing timec
     *
     * @return $this
     */
    private function setupPublishTime()
    {
        if($this->metaObject->getPublishTime()) {
            $this->addSingleValue(Carbon::parse($this->metaObject->getPublishTime())->toIso8601String(), 'article:published_time');
        }

        return $this;
    }
}