<?php

namespace Customer\Handlers\MetaHandler\Source;

use Customer\EwcDate;

class Cxsense extends BaseSource
{
    protected $metaPrefix = 'cXenseParse';
    private $metaPropertyName = 'name';

    /**
     * Setup meta data
     */
    protected function setupMeta()
    {
        $this->setupArticleId()
             ->setupTitle()
             ->setupCategories()
             ->setupTags()
             ->setupPlaces()
             ->setupAuthors()
             ->setupNewsValue()
             ->setupLifeSpan()
             ->setupChannels()
             ->setupMainChannel()
             ->setupStories()
             ->setupIsPlus()
             ->setupContentType()
             ->setupRecommendable()
             ->setupPublishingTime()
             ->setupModificationTime();
    }

    /**
     * Setup article id
     *
     * @return $this
     */
    private function setupArticleId()
    {
        $this->addSingleValue($this->metaObject->getUuid(), 'recs:articleid', $this->metaPropertyName);

        return $this;
    }

    /**
     * Setup meta data for categories
     *
     * @return $this
     */
    private function setupCategories()
    {
        $this->addMultiValue($this->metaObject->getCategories(), 'taxonomy', $this->metaPropertyName);

        return $this;
    }

    /**
     * Setup tags
     *
     * @return $this
     */
    private function setupTags()
    {
        $tags = $this->metaObject->getTags();

        if (count($tags) === 0) {
            return $this;
        }
        
        $tagsMeta = collect($tags)->map(function ($tag) {
            switch ($tag['type']) {
                case 'organisation':
                    return [
                        $this->metaPropertyName => "{$this->metaPrefix}:company",
                        'content'  => $tag->name,
                    ];
                case 'person':
                    return [
                        $this->metaPropertyName => "{$this->metaPrefix}:person",
                        'content'  => $tag->name,
                    ];
                case 'topic':
                default:
                    return [
                        $this->metaPropertyName => "{$this->metaPrefix}:topic",
                        'content'  => $tag->name,
                    ];
            }

        });

        $this->mergeData($tagsMeta->toArray());

        return $this;
    }

    /**
     * Setup places
     *
     * @return $this
     */
    private function setupPlaces()
    {
        $this->addMultiValue($this->metaObject->getPlaces(), 'location', $this->metaPropertyName);

        return $this;
    }

    /**
     * Setup authors
     *
     * @return $this
     */
    private function setupAuthors()
    {
        $this->addMultiValue($this->metaObject->getAuthors(), 'author', 'name');

        return $this;
    }

    /**
     * Setup news value
     *
     * @return $this
     */
    private function setupNewsValue()
    {
        $this->addSingleValue($this->metaObject->getNewScore(), 'nwt-newsvalue', $this->metaPropertyName);

        return $this;
    }

    /**
     * Setup life span expiration
     *
     * @return $this
     */
    private function setupLifeSpan()
    {
        $lifeSpan = $this->metaObject->getLifeSpan();

        if ( ! isset($lifeSpan['stop'])) {
            return $this;
        }

        $this->addSingleValue($lifeSpan['stop'], 'nwt-expiration', $this->metaPropertyName);

        return $this;
    }

    /**
     * Setup channels
     *
     * @return $this
     */
    private function setupChannels()
    {
        $this->addMultiValue($this->metaObject->getChannels(), 'nwt-channel', $this->metaPropertyName);

        return $this;
    }

    /**
     * Setup main channel
     *
     * @return $this
     */
    private function setupMainChannel()
    {
        $this->addSingleValue($this->metaObject->getMainChannel(), 'nwt-mainchannel', $this->metaPropertyName);

        return $this;
    }

    /**
     * Setup stories
     *
     * @return $this
     */
    private function setupStories()
    {
        $this->addMultiValue($this->metaObject->getStories(), 'nwt-story', $this->metaPropertyName);

        return $this;
    }

    /**
     * Setup plus meta
     *
     * @return $this
     */
    private function setupIsPlus()
    {
        if ( ! $this->metaObject->getPresentIsPlus()) {
            return $this;
        }

        $paidOrFree = $this->metaObject->getisPlus() ? 'paid' : 'free';
        $plusOrFree = $this->metaObject->getisPlus() ? 'plus' : 'free';

        $this->addSingleValue($paidOrFree, 'mat-artype', $this->metaPropertyName);
        $this->addSingleValue($paidOrFree, 'recs:mat-artype', $this->metaPropertyName);
        $this->addSingleValue($plusOrFree, 'nwt-artype', $this->metaPropertyName);
        $this->addSingleValue($plusOrFree, 'recs:nwt-artype', $this->metaPropertyName);

        return $this;
    }

    /**
     * Setup content type
     *
     * @return $this
     */
    private function setupContentType()
    {
        $this->addSingleValue($this->metaObject->getArticleType(), 'nwt-contenttype', $this->metaPropertyName);

        return $this;
    }

    /**
     * Setup publishing time
     *
     * @return $this
     */
    private function setupPublishingTime()
    {
        if ( ! $this->metaObject->getPresentPublishTime()) {
            return $this;
        }

        $this->addSingleValue(EwcDate::parse($this->metaObject->getPublishTime())->toIso8601String(), 'recs:publishtime', $this->metaPropertyName);

        return $this;
    }

    /**
     * Setup modification time
     *
     * @return $this
     */
    private function setupModificationTime()
    {
        $updatedDate = EwcDate::parse($this->metaObject->getModificationTime());

        if ( ! $updatedDate->greaterThan(EwcDate::parse($this->metaObject->getPublishTime()))) {
            return $this;
        }

        $this->mergeData([
            [
                'property' => "article:modified_time",
                'content'  => $updatedDate->toIso8601String(),
            ],
        ]);

        return $this;
    }

    /**
     * Setup title
     *
     * @return $this
     */
    private function setupTitle()
    {
        $this->addSingleValue($this->metaObject->getTitle(), 'title', $this->metaPropertyName);

        return $this;
    }

    /**
     * Get meta data flat
     *
     * @return array
     */
    public function getMetaDataFlat()
    {
        $meta = array_map(function ($meta) {
            if (isset($meta['name'])) {
                return [
                    $meta['name'] => $meta['content'],
                ];
            }

            if (isset($meta['property'])) {
                return [
                    $meta['property'] => $meta['content'],
                ];
            }

            return false;
        }, $this->metaData);

        $meta = array_values($meta);

        return $meta;
    }

    private function setupRecommendable()
    {
        if ($this->metaObject->getRecommendable() === null) {
            return $this;
        }
//        <meta name="cXenseParse:recs:recommendable" content="false"/>
        $this->addSingleValue($this->metaObject->getRecommendable(), 'recs:recommendable', $this->metaPropertyName);

        return $this;
    }
}