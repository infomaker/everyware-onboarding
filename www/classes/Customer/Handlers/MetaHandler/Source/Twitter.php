<?php

namespace Customer\Handlers\MetaHandler\Source;

use Customer\Helpers\AssetsHelper;
use EwTools\Models\SettingsParameter;

class Twitter extends BaseSource
{
    protected $metaPrefix = 'twitter';

    protected function setupMeta()
    {
        $this->setupTitle()
             ->setupDescription()
             ->setupCard()
             ->setupImage()
             ->setupSite()
             ->setupCreator()
             ->setupUrl();
    }

    /**
     * Setup title
     * 
     * @return $this
     */
    private function setupTitle()
    {
        $this->addSingleValue($this->metaObject->getTitle(), 'title');

        return $this;
    }

    /**
     * Setup description
     *
     * @return $this
     */
    private function setupDescription()
    {
        $this->addSingleValue($this->metaObject->getDescription(), 'description');

        return $this;
    }

    private function setupCard()
    {
        $this->addSingleValue('summary_large_image', 'card');

        return $this;
    }

    /**
     * Setup image for article
     *
     * @return $this
     */
    private function setupImage()
    {
        $image = $this->getTeaserImageUrl();

        if ($image) {
            $this->addSingleValue($image, 'image', 'property', true);

            return $this;
        }

        // Default
        $this->addSingleValue(AssetsHelper::getTwitterDefaultImage(), 'image');

        // Default x2
        $this->addSingleValue(AssetsHelper::getTwitterDefaultImage2x(), 'image');

        return $this;
    }

    /**
     * Set twitter handle
     *
     * @return $this
     */
    private function setupSite()
    {
        $twitterHandle = SettingsParameter::getValue('twitter_handle');

        if ($twitterHandle) {
            $twitterHandle = strpos($twitterHandle, '@') === false ? "@{$twitterHandle}" : $twitterHandle;

            $this->addSingleValue($twitterHandle, 'site');

        }

        return $this;
    }

    /**
     * Setup creator
     *
     * @return $this
     */
    private function setupCreator()
    {
        $this->addMultiValue($this->metaObject->getTwitterHandle(), 'creator');

        return $this;
    }

    /**
     * Setup url
     *
     * @return $this
     */
    private function setupUrl()
    {
        $this->addSingleValue($this->metaObject->getPermalink(), 'url');

        return $this;
    }


}