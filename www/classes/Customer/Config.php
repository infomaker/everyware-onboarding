<?php namespace Customer;

use Everyware\PropertyMapConfig;
use EwTools\ProjectPluginConfig;
use EwTools\Storage\Cache;
use EwTools\Support\Str;
use EwTools\Support\Utilities;

/**
 * Config
 *
 * @link    http://infomaker.se
 * @package Customer
 * @since   Customer\Config 1.0.0
 */
class Config extends ProjectPluginConfig implements PropertyMapConfig {
    
    private static $config;
    private static $config_file_path = ABSPATH . 'config.json';
    
    public function __construct() {
        $this->name = 'Everyware Onboarding';
        $this->slug = sanitize_title( $this->name );
        $this->path = $this->slug;
    }

    /**
     * @return string
     */
    public static function getCompany() {
        return ( new static() )->name;
    }

    public static function getPluginSlug()
    {
        return (new static())->slug;
    }

    /**
     * @param string $path
     *
     * @return string
     */
    public static function pluginPath( $path = '' ) {
        return Str::finish( Utilities::pluginPath( self::getPluginSlug()), "/{$path}" );
    }

    /**
     * @return array|mixed|object
     */
    private function readConfig() {
        $key = 'ew_config_' . GIT_COMMIT;
        
        if( ( $config = Cache::get( $key ) ) === false ) {
            $config = json_decode( file_get_contents( $this->getConfigFilePath() ), true );
            if( $key !== null && is_prod() ) {
                Cache::set( $key, $config, WEEK_IN_SECONDS );
            }
        }
        
        return $config;
    }
    
    /**
     * @since 1.0.0
     * @return mixed
     */
    private function getConfigFile() {
        if( static::$config === null ) {
            static::$config = $this->readConfig();
        }
        
        return static::$config;
    }
    
    /**
     * Retrieve the map of the properties from the configuration file
     *
     * @since 1.0.0
     *
     * @param string $reference
     *
     * @return array
     */
    public function getPropertyMap( $reference = '') {
        $map = $this->getFromConfig( 'typePropertyMap' );
        
        if ( empty($reference) ) {
            return $map;
        }
        
        return array_key_exists($reference, $map) ? $map[$reference] : $map;
    }
    
    /**
     * Retrieve reference classes for properties from the configuration file
     *
     * @since 1.0.0
     * @return array
     */
    public function getPropertyClassReference() {
        return $this->getFromConfig( 'typePropertyClassReference' );
    }
    
    /**
     * Retrieve hole or a specific part of the configuration
     *
     * @param string $name
     *
     * @since 1.0.0
     * @return array
     */
    public function getFromConfig( $name = null ) {
        if ( $name !== null ) {
            return array_get( $this->getConfigFile(), $name, [] );
        }
        
        return $this->getConfigFile();
    }
    
    /**
     * Retrieve the absolute path to the config file
     *
     * @since 1.0.0
     * @return string
     */
    public function getConfigFilePath() {
        return static::$config_file_path;
    }
}