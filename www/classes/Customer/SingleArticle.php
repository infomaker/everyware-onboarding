<?php namespace Customer;

use Customer\Handlers\MetaHandler\HeaderMetaHandler;
use Customer\Handlers\MetaHandler\Objects\ArticleHeaderMeta;
use Customer\Presentations\ArticleBodyPresentation;
use Everyware\Concepts\Author;
use Everyware\Newsml\Item;
use Everyware\PropertyMapper;
use EwTools\Handler\OpenContent;
use OcArticle;
use WP_Post;

/**
 * SingleArticle
 *
 * @link    http://infomaker.se
 * @package Customer
 * @since   Customer\SingleArticle 1.0.0
 */
class SingleArticle extends ArticleTemplate
{

    /**
     * Retrieve data to be used in the View
     *
     * @since 1.0.0
     * @return array
     */
    public function getViewData()
    {
        $template_data = [
            'uuid'      => $this->article->uuid,
            'permalink' => $this->getPermalink(),
            'authors'   => $this->getBylines(),
            'headline'  => $this->article->headline,
            'leadin'    => $this->getLeadin(),
            'pubdate'   => $this->article->pubdate->toText('YYYY-MM-dd'),
            'updated'   => $this->article->updated->toText('YYYY-MM-dd'),
            'body'      => $this->getBody(),
            'is_plus'   => $this->article->is_plus,
        ];

        return ['article' => $template_data];
    }

    /**
     * Setup meta data for Single article
     */
    public function setupHeaderMeta()
    {
        $headerMetaHandler = new HeaderMetaHandler(new ArticleHeaderMeta($this->article));
        $headerMetaHandler->setupCxsenseData()
                          ->setupFacebookData()
                          ->setupTwitterData()
                          ->appendData();
    }

    /**
     * Get permalink for article
     *
     * @return string
     */
    public function getPermalink()
    {
        return $this->article->permalink;
    }

    /**
     * Get if article is plus
     *
     * @return bool
     */
    public function isPlus()
    {
        return $this->article->is_plus;
    }

    /**
     * Get uuid
     *
     * @return string
     */
    public function getUuid()
    {
        return $this->article->uuid;
    }

    /**
     * Setup body
     *
     * @return array
     */
    private function getBody()
    {
        $articleBodyPresentation = new ArticleBodyPresentation($this->article);

        return $articleBodyPresentation->generateAndGetContent();
    }

    /**
     * @since 1.0.0
     * @return array
     */
    public function getBylines()
    {
        return $this->article->getAuthors()->map(function (Author $author) {
            return $author->getBylineInfo();
        })->toArray();
    }

    /**
     * @since 1.0.0
     * @return array
     */
    private function getLeadin()
    {
        return array_map(function (Item $item) {
            return $item->toArray();
        }, $this->article->getLeadin());
    }

    /**
     * Function fetch an article and create an instance of requested class
     *
     * @param string $uuid
     * @param array  $properties
     *
     * @return null|static
     * @since 0.1
     */
    public static function create($uuid, array $properties = [])
    {
        // Fix fall back properties if not provided
        $properties = ! empty($properties) ? $properties : (new PropertyMapper(new Config()))->requiredProperties('Article');

        $article = OpenContent::getArticle($uuid, $properties);

        if ($article instanceof OcArticle) {
            return static::createFromOcArticle($article);
        }

        return null;
    }

    /**
     * Function create an article from the post type
     *
     * @param WP_Post $post
     * @param array   $properties
     *
     * @return null|static
     * @since    0.1
     */
    public static function createFromPost(WP_Post $post, array $properties = [])
    {
        // Fix fall back properties if not provided
        $properties = ! empty($properties) ? $properties : (new PropertyMapper(new Config()))->requiredProperties('Article');

        return static::create(get_post_meta($post->ID, 'oc_uuid', true), $properties);
    }

}