<?php namespace Customer\Helpers;

use Everyware\Helpers\RequestHelper;
use EwTools\Handler\Menus;
use EwTools\Models\Page;
use EwTools\Storage\Cache;
use EwTools\Support\Str;

use EwTools\Models\Section;

/**
 * MenuHelpers
 *
 * @link    http://infomaker.se
 * @package Customer\Utilities
 * @since   Customer\Utilities\MenuHelpers 0.1
 */
class MenuHelpers {

    /**
     * Make sure custom menu-items have trailing slashes och their urls
     *
     * @since 1.0.0
     * @return void
     */
    public static function addTrailingSlashes() {
        add_filter('wp_setup_nav_menu_item', function($menu) {
            if ( $menu->type === 'custom' ) {
                $menu->url = Str::finish($menu->url, '/');
            }
            return $menu;
        });
    }

    /**
     * Build the mega menu combined of all locations
     *
     * @since 1.0.0
     * @return array
     */
    public static function getMegaMenu() {
        return [
            'main_menu'      => static::getItemsFromLocation( 'main-menu' ),
        ];
    }

    /**
     * Retrieve the cached mega menu or get a new one and cache it for later use
     *
     * @since 1.0.0
     * @return array|mixed
     */
    public static function getCachedMegaMenu() {
        $mega_menu = Cache::get( 'page_mega_menu', [] );
        if( empty( $mega_menu )) {
            $mega_menu = static::getMegaMenu();
            static::cacheMegaMenu($mega_menu);
        }

        return $mega_menu;
    }

    /**
     * Cache menu
     *
     * @param $mega_menu
     *
     * @since 1.0.0
     * @return void
     */
    public static function cacheMegaMenu($mega_menu) {
        Cache::set('page_mega_menu', $mega_menu, 7 * DAY_IN_SECONDS );
    }

    /**
     * Create and cache Mega menu whenever someone saves a menu
     *
     * @since 1.0.0
     * @return void
     */
    public static function init() {
        add_action('wp_update_nav_menu', function() {
            static::cacheMegaMenu(static::getMegaMenu());
        });
    }

    /** Setup mega menu in top of page
     *
     * @return array
     */
    public static function setupMegaMenu() {
        $current_section_class = Section::getClassBySlug( RequestHelper::segment( 1 ) );

        return array_merge( static::getCachedMegaMenu(), [
            'current_section_class' => $current_section_class === '' ? 'no-current-section' : $current_section_class,
        ] );
    }

    /**
     * Setup sub menu
     *
     * @param Page $page
     *
     * @return array
     */
    public static function setupSubMenu( Page $page ) {
        $meta = $page->getMeta( 'ew_page_meta_box' );

        if( $meta[ 'use_submenu' ] !== "on" && Str::isEmpty( $meta[ 'menu' ] ) ) {
            return [];
        }

        $sub_menu = Menus::where( [ 'id' => $meta[ 'menu' ] ] );

        if( $sub_menu ) {
            return [
                'current_post_id' => $page->id,
                'name'            => $sub_menu->name,
                'items'           => $sub_menu->items()
            ];
        }

        return [];
    }

    /**
     * Get menu items for location
     *
     * @param $location
     *
     * @return array|\EwTools\Support\Collection
     */
    private static function getItemsFromLocation( $location ) {
        return ( $menu = Menus::getFromLocation( $location ) ) !== false ? $menu->items() : [];
    }
}