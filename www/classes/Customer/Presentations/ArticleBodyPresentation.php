<?php

namespace Customer\Presentations;

use Customer\EwcArticle;
use Customer\Image;
use Everyware\handler\NewRelicLog;
use Everyware\Newsml\Item;
use Everyware\Newsml\Objects\ImageItem;
use EwTools\Storage\Cache;
use EwTools\Twig\View;

class ArticleBodyPresentation
{
    private $article;

    public function __construct(EwcArticle $article)
    {
        $this->article = $article;
    }

    public function generateAndGetContent()
    {
        $articleBody   = $this->article->getBodyParser();
        $convertedBody = [];

        foreach ($articleBody->getBody() as $index => $item) {
            if ( ! $item instanceof Item) {
                continue;
            }
            $type = $item->getType();
            switch ($type) {
                case 'x-im/image':
                    if ($item instanceof ImageItem) {
                        $convertedBody[] = $this->image($item);
                    }
                    break;
                case 'x-im/link':
                    $convertedBody[] = $item->toArray();
                    break;
                case 'x-im/socialembed':
                    $convertedBody[] = $this->socialEmbed($item);
                    break;
                case 'x-im/content-part':
                    $convertedBody[] = $this->contentPart($item);
                    break;
                case 'x-im/htmlembed':
                    $convertedBody[] = $this->htmlEmbed($item);
                    break;
                default:
                    if ($type !== 'headline' && $type !== 'preamble') {
                        $convertedBody[] = $this->element($item);
                    }
                    break;
            }
        }

        return $convertedBody;
    }

    /**
     * Get element
     *
     * @param Item $item
     *
     * @return string
     */
    protected function element(Item $item)
    {
        $type        = $item->getType();
        $valid_types = [
            'blockquote',
            'body',
            'dateline',
            'drophead',
            'headline',
            'preamble',
            'pagedateline',
            'preleadin',
            'subheadline1',
        ];

        if ( ! \in_array($type, $valid_types, true)) {
            NewRelicLog::error(sprintf('Unknown block of type: %s in Article body.', $type));

            return '';
        }

        return View::generate( '@base/page/article/parts/body/' . $type . '.twig', [
            'content' => $item->get('content'),
        ] );
    }

    /**
     * @param ImageItem $item
     *
     * @since 1.0.0
     * @return string
     */
    private function image(ImageItem $item)
    {
        $image = new Image( $item );

        return View::generate( '@base/page/article/parts/body/image.twig', $image->getArticleImage() );
    }

    /**
     * @param Item $item
     *
     * @since 1.0.0
     * @return string
     */
    private function htmlEmbed(Item $item)
    {
        return $item->get('content');
    }

    /**
     * @param Item $item
     *
     * @since 1.0.0
     * @return string
     */
    private function contentPart(Item $item)
    {
        $content_part_text = array_map([&$this, 'element'], (array)$item->get('text'));

        $item->set('text', implode('', $content_part_text));

        return $this->generateArticlePart('content-part', $item->toArray());
    }

    /**
     * Social embed
     *
     * @param \Everyware\Newsml\Item $item
     *
     * @return string
     */
    private function socialEmbed(Item $item)
    {
        $links = $item->get('content');

        if (empty($links)) {
            return '';
        }

        if ( ! ($linkItem = array_shift($links)) instanceof Item) {
            return '';
        }

        switch ($linkItem->getType()) {
            case 'x-im/tweet':
                return $this->twitter($linkItem);
                break;
            case 'x-im/facebook-post':
                return $this->facebook($linkItem);
                break;
            case 'x-im/instagram':
                return $this->instagram($linkItem);
                break;
            default:
                return '';
                break;
        }
    }

    /**
     * Display a Instagram post
     *
     * @param Item $item
     *
     * @return string
     */
    private function instagram(Item $item)
    {
        $embed_url = $item->embed_url ?? $item->url;
        $key       = md5("instagram_{$embed_url}");

        if (($content = Cache::get($key)) === false) {
            try {
                $content = file_get_contents('https://api.instagram.com/oembed/?url=' . $item->url);
            } catch (\Exception $e) {
                return '';
            }

            Cache::set($key, $content, 5 * MINUTE_IN_SECONDS);
        }

        return $this->generateArticlePart('instagram', [
            'content' => json_decode($content)->html,
        ]);
    }

    /**
     * Facebook content part
     *
     * @param Item $item
     *
     * @since 1.0.0
     * @return string
     */
    private function facebook(Item $item)
    {
        return $this->generateArticlePart('facebook', [
            'url' => $item->url,
        ]);
    }

    /**
     * Twitter content part
     *
     * @param Item $item
     *
     * @since 1.0.0
     * @return string
     */
    private function twitter(Item $item)
    {
        return $this->generateArticlePart('twitter', [
            'url' => $item->url,
        ]);
    }

    /**
     * @param $part
     * @param $data
     *
     * @since 1.0.0
     * @return string
     */
    private function generateArticlePart($part, $data)
    {
        return View::generate("@base/page/article/parts/body/{$part}", $data);
    }
}