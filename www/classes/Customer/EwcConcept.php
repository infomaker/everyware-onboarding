<?php namespace Customer;

use Everyware\OpenContentProvider;
use Everyware\PropertyMapper;
use Everyware\PropertyObject;
use EwConcepts\Models\Page;
use EwTools\Handler\OpenContentQueryBuilder;
use OcArticle;
use OcObject;

/**
 * EwcConcept
 *
 * @property array  articles
 * @property string meta_data
 * @property string contenttype
 * @property string description
 * @property string description_short
 * @property string name
 * @property array  related_links
 * @property string permalink
 * @property string type
 * @property string uuid
 * @link    http://infomaker.se
 * @package Customer
 * @since   Customer\EwcConcept 1.0.0
 */
class EwcConcept extends PropertyObject {
    
    public function __construct( PropertyObject $object ) {
        $this->fill( $object->toArray() );
        $this->set( 'permalink', Page::generatePermalink( $this->type, $this->name, $this->uuid ) );
    }
    
    /**
     * Helper function to fetch related articles
     *
     * @param int $limit
     *
     * @since 0.1
     * @return array
     */
    public function getArticles( $limit = 30 ) {
        $query = OpenContentQueryBuilder::where( 'ConceptUuids', $this->uuid );
        
        $content_provider = OpenContentProvider::setup( [
            'q'                      => $query->buildQueryString(),
            'contenttypes'           => [ 'Article' ],
            'sort.indexfield'        => 'Pubdate',
            'sort.Pubdate.ascending' => 'false',
            'limit'                  => $limit
        ] )->setPropertyMap( 'Article' );
        
        return $content_provider->queryWithRequirements();
    }
}