<?php namespace Customer;

use Everyware\Concepts\Category;
use Everyware\handler\NewRelicLog;
use Everyware\Newsml\Objects\TeaserItem;
use EwTools\Handler\Imengine;
use EwTools\Support\Str;
use EwTools\Models\SettingsParameter;

/**
 * TeaserArticle
 *
 * @property array  imageuuids
 * @property string headline
 * @property string text
 * @link    http://infomaker.se
 * @package Customer
 * @since   Customer\TeaserArticle 1.0.0
 */
class TeaserArticle extends ArticleTemplate {
    
    protected static $properties = [
        'image'     => '',
        'post_time' => '',
        'category'  => '',
        'title'     => '',
        'text'      => '',
        'extract'   => '',
        'url'       => '',
        'is_plus'   => '',
        'tag'       => '',
    ];

    private $classess = [];
    
    /**
     * @since 1.0.0
     * @return array|mixed
     */
    private function getImageUrl() {
        $images = $this->article->getFirstOfMultiValue( 'teaser_image_uuids|image_uuids' );
        if(\count( $images ) > 0 ) {
            try {
                $imageuuid = $images[ 0 ];
                
                return [
                    'alt' => '',
                    'url' => Imengine::ratio( 600, '16:9' )->fromUuid( $imageuuid )
                ];
            } catch ( \RuntimeException $exception ) {
                NewRelicLog::error( 'Failed to create cloudfront image url: ', $exception );
            }
        }

        return null;
    }
    
    private function getCategory() {
        $category = $this->article->getCategories()->first();
        
        if( $category instanceof Category ) {
            return [
                'name'  => Str::slug( $category->name ),
                'title' => $category->name
            ];
        }
        
        return null;
        
    }

    protected function getHeadline() {
        if(isset($this->article->teaser_headline)){
            return  $this->article->teaser_headline;
        }

        return $this->article->headline;
    }

    /**
     * @since 1.0.0
     * @return string
     */
    private function getArticleLeadin() {
        $leadins = $this->article->getLeadin();

        if(\count( $leadins ) > 0 ) {
            $leadin = $leadins[0]->content;
            return $this->truncateString($leadin, 160);
        }

        return '';
    }

    /**
     * @since 1.0.0
     *
     * @param     $string
     *
     * @param int $length
     *
     * @return string
     */
    protected function truncateString($string, $length = 60)
    {
        if (empty($string)) {
            return '';
        }

        return (mb_strlen($string) > $length) ? trim(mb_substr($string, 0, $length - 3)) . '...' : $string;
    }

    /**
     * @since 1.0.0
     * @return string
     */
    private function getPostTime() {
        $pubdate = $this->article->pubdate;
        if( $pubdate instanceof EwcDate ) {
            return $pubdate->toPresentationDate( true );
        }

        return '';
    }

    /**
     * Append css class to teaser
     *
     * @param $class
     *
     * @return $this
     */
    public function appendClass($class)
    {
        $this->classess[] = $class;

        return $this;
    }

    /**
     * Get classes for the teaser
     *
     * @return array
     */
    private function getClasses()
    {
        return array_unique($this->classess);
    }

    /**
     * Get classes as string
     *
     * @return string
     */
    private function getClassesAsString()
    {
        return implode(' ', $this->getClasses());
    }

    /**
     * Retrieve data to be used in the View
     *
     * @since 1.0.0
     * @return array
     */
    public function getViewData() {
        $teaser = $this->article->getTeaserParser();

        return array_replace( [
            'images'         => [$this->getImageUrl()],
            'pubdate'        => $this->getPostTime(),
            'category'       => $this->getCategory(),
            'leadin'         => $this->getArticleLeadin(),
            'headline'       => $this->truncateString($this->getHeadline()),
            'url'            => $this->article->permalink,
            'is_plus'        => $this->article->is_plus,
            'tag'            => '',
            'teaser_classes' => $this->getClassesAsString(),
        ], $teaser instanceof TeaserItem ? $this->createTeaser( $teaser ) : [] );
    }

    private function createTeaser( TeaserItem $teaser ) {
        $image = $teaser->hasImage() ? new Image( $teaser->getImage() ) : $this->article->getTeaserDefaultImage();

        $teaser_data = array_filter( [
            'headline' => $this->truncateString($this->getHeadline()),
            'leadin'   => $this->truncateString($teaser->text, 160),
            'image'    => $image instanceof Image ? $image->getTeaserImage() : null,
        ] );

        return $teaser_data;
    }
}