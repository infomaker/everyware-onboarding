<?php namespace Customer;

use Everyware\handler\NewRelicLog;
use Everyware\Newsml\Objects\ImageItem;
use EwTools\Handler\Imengine;
use RuntimeException;

/**
 * Image
 *
 * @link    http://infomaker.se
 * @package Customer
 * @since   Customer\Image 1.0.0
 */
class Image {
    
    /**
     * @var ImageItem
     */
    private $image;
    
    public function __construct( ImageItem $image ) {
        $this->image = $image;
    }
    
    /**
     * @param int    $width
     * @param string $ratio
     *
     * @since  1.0.0
     * @return string
     */
    public function getRatioUrl( $width, $ratio = '16:9' ) {
        $this->image->hasCrop( $ratio );
        try {
            
            if( ! $this->image->hasCrop( $ratio ) ) {
                $imengine = $this->image->hasOriginalSizes() ? Imengine::autoCrop( $this->image->width, $this->image->height, $width, $ratio ) : Imengine::ratio( $width, $ratio );
                
                return $imengine->fromUuid( $this->image->uuid );
            }
            $crop = $this->image->getCrop( $ratio );
            
            return Imengine::cropResize( $crop[ 'width' ], $crop[ 'height' ], $width, $this->calculateRatioHeight( $width, $ratio ), $crop[ 'x' ], $crop[ 'y' ] )->fromUuid( $this->image->uuid );
            
        } catch ( RuntimeException $e ) {
            NewRelicLog::error( 'Failed to fetch imengine url', $e );
        }
        
        return '';
    }
    
    /**
     * @since 1.0.0
     * @return array
     */
    public function getTeaserImage() {
        return [
            'url' => $this->getRatioUrl( 700 ),
            'alt' => '',
        ];
    }

    public function getGalleryThumb() {
        return [
            'uuid'    => $this->image->get('uuid'),
            'text'    => $this->image->get( 'text' ),
            'alttext' => $this->image->get( 'alttext' ),
            'images'  => [
                [
                    'url' => $this->getRatioUrl( 55, '1:1' )
                ]
            ]
        ];
    }

    public function getArticleImage() {
        return [
            'image' => [
                'uuid'    => $this->image->get('uuid'),
                'text'    => $this->image->get('text'),
                'alttext' => $this->image->get('alttext'),
                'authors' => $this->image->get('author'),
                'src'     => [
                    'default' => $this->getRatioUrl(900),
                    'small'   => $this->getRatioUrl(300),
                    'medium'  => $this->getRatioUrl(600),
                    'max'     => $this->getRatioUrl(1200),
                ]
            ]
        ];
    }
    
    /**
     * @param $width
     *
     * @since 1.0.0
     * @return int
     */
    protected function calculateRatioHeight( $width, $ratio ) {
        [$ratio_width, $ratio_height] = explode( ':', $ratio );
        
        return (int)round( ( $width / (int)$ratio_width ) * (int)$ratio_height );
    }

    /**
     * Generate src string
     *
     * @param int|string $size
     *
     * @since 0.1
     * @return null|string
     * @throws \RuntimeException
     */
    public function src( $size ) {
    	if(!empty($this->options)) {
            return Imengine::ratio( (int)$size )->with($this->options)->fromUuid( $this->image->uuid );
	    }

        return Imengine::ratio( (int)$size )->quality(80)->fromUuid( $this->image->uuid );
    }

    /**
     * Generate a srcset string for images
     *
     * @param array $sizes
     *
     * @since 0.1
     * @return string
     * @throws \RuntimeException
     */
    public function srcset( array $sizes = [] ) {
        return implode(',', array_map( function ( $size ) {
            return Imengine::ratio( (int)$size )->fromUuid( $this->image->uuid ) . " {$size}w";
        }, $sizes ));
    }
}