<?php namespace Customer;

use Everyware\PagePart;
use EwTools\Metaboxes\EwPageMeta;
use EwTools\Models\Page;
use EwTools\Models\SettingsParameter;
use EwTools\Support\Str;
use EwTools\Support\Utilities;

/**
 * EwcHeader
 *
 * @link    http://infomaker.se
 * @package Customer
 * @since   Customer\EwcHeader 1.0.0
 */
class EwcHeader implements PagePart {
    
    private $page;
    
    public function __construct() {
        $this->page = Page::current();
    }
    
    public function getTemplateData() {
        return [
            'page_title' => apply_filters( 'page_title', $this->getSiteTitle() ),
            'head_links' => $this->getHeadLinks(),
            'head_meta'  => $this->getMetaLinks(),
        ];
    }
    
    /**
     * Get site title for page
     *
     * @return string
     */
    protected function getSiteTitle() {
        $title = '';
        
        if( $this->onPage() ) {
            $seo_title = EwPageMeta::getMetaData( $this->page, 'title' );
            $title     = $seo_title ?: '';
        }
        
        if( Str::isEmpty( $title ) ) {
            return Str::append( $this->getCachedSiteMeta( 'name' ), html_entity_decode( get_the_title() ), ' - ' );
        }
        
        return $title;
    }
    
    /**
     * Get cached site meta
     *
     * @param string $meta_name
     *
     * @return mixed|string
     */
    protected function getCachedSiteMeta( $meta_name = '' ) {
        $cache_key = "site-{$meta_name}";
        
        // Use cached data if any
        if( false === ( $meta_info = get_transient( $cache_key ) ) ) {
            
            // Store the fetched data in cache for 1h.
            if( false !== ( $meta_info = get_bloginfo( $meta_name ) ) ) {
                set_transient( $cache_key, $meta_info, HOUR_IN_SECONDS );
            }
        }
        
        return $meta_info;
    }
    
    /**
     * Get meta links
     *
     * @return array
     */
    protected function getMetaLinks() {
        // Global meta data
        $head_meta = [
            [ 'name' => 'description', 'content' => $this->getPageMetaDescription() ],
            [ 'property' => 'fb:app_id', 'content' => SettingsParameter::getValue( 'facebook_app_id' ) ],
        ];
        if( ! is_single() && ! is_404() ) {
            $head_meta = array_merge( $head_meta, [
                [ 'property' => 'og:title', 'content' => $this->getSiteTitle() ],
                [ 'property' => 'og:description', 'content' => $this->getPageMetaDescription() ],
                [ 'property' => 'og:type', 'content' => 'website' ],
                [ 'property' => 'og:site_name', 'content' => wp_get_theme()->get( 'Name' ) ],
                [ 'property' => 'og:locale', 'content' => 'sv_SE' ],
                [ 'property' => 'og:image', 'content' => Utilities::getStaticThemeUri() . '/assets/images/logo.png' ],
                [ 'property' => 'og:image:type', 'content' => 'image/png' ],
                [ 'property' => 'fb:page_id', 'content' => SettingsParameter::getValue( 'facebook_app_id' ) ],
            ] );
        }
        
        return apply_filters( 'header_meta', $head_meta );
    }
    
    /**
     * Get page meta
     *
     * @return string
     */
    private function getPageMetaDescription() {
        $description = '';
        
        if( $this->onPage() ) {
            $seo_description = EwPageMeta::getMetaData( $this->page, 'description' );
            $description     = $seo_description ?: '';
        }
        
        return Str::notEmpty( $description ) ? $description : $this->getCachedSiteMeta( 'description' );
        
    }
    
    /**
     * Get head links
     *
     * @return array
     */
    private function getHeadLinks() {
        return apply_filters( 'header_links', [
            [
                'rel'  => 'Shortcut Icon',
                'href' => Utilities::getStaticThemeUri() . '/assets/images/favicon.png',
                'type' => 'image/png'
            ]
        ] );
    }
    
    private function onPage() {
        return $this->page !== null;
    }
}