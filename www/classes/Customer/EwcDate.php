<?php namespace Customer;

use Carbon\Carbon;
use EwTools\Support\Date;

/**
 * EWC_Date
 *
 * @link    http://infomaker.se
 * @package WWW\Classes\Customer
 * @since   WWW\Classes\Customer\EwcDate 0.1
 */
class EwcDate extends Date {

    /**
     * @param Carbon|null $other
     * @param bool        $absolute
     * @param bool        $short
     *
     * @return mixed
     */
    public function diffForHumans( Carbon $other = null, $absolute = false, $short = true ) {
        return str_replace( 'före', 'sedan', parent::diffForHumans( $other, $absolute, $short ) );
    }
    
    /**
     * Get date presentation for article by it's rules
     *
     * @return string
     */
    public function getArticleDatePresentation() {
        return $this->format( 'Y-m-d' );
    }
    
    /**
     * The presentation of a date used for teasers
     *
     * @since 0.1
     * @return string
     */
    public function getTeaserDatePresentation() {
        return $this->toPresentationDate( true );
    }
    
    /**
     * Get the date formatted into a translated and readable string
     *
     * @param bool $short
     *
     * @since 0.1
     * @return string
     */
    public function toPresentationDate( $short = false ) {
        $now = static::now();
        
        if( $this->isToday() ) {
            $hours_ago = $this->diffInHours($now);
            
            if ( $hours_ago < 1 ) {
                return $this->diffInMinutes($now). 'min ago';
            }
            
            if ( $hours_ago < 6 ) {
                return "{$hours_ago}h ago";
                
            }
            return 'today ' . $this->toText( 'HH:mm' );
        }
        
        if( $this->isYesterday() ) {
            return 'yesterday ' . $this->toText( 'HH:mm' );
        }
        
        if( ! $this->isSameYear( Date::now() ) ) {
            return $this->format( 'Y-m-d' );
        }
        
        if( $short ){
            return rtrim( $this->format( 'jS M'));
        }

        return rtrim( $this->format( 'jS M, Y'));
    }
    
    public function toWeekDay($short = false) {
        return $this->toText( $short ? 'EEE' : 'EEEE' );
    }
    
    public function toWeatherDate() {
        return rtrim( $this->toText( 'EEE d MMM' ), '.' );
    }
}