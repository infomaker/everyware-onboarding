<?php namespace Customer;

use Customer\Utilities\TwigFunctions;
use EwTools\Support\Str;
use EwTools\Twig\ViewSetup;

class EwcTwig extends ViewSetup {

    /**
     * Setup your Twig-environment with settings folders to be loaded, states etc.
     *
     * @since 0.1
     * @return void
     * @throws \Twig_Error
     */
    public function setupTwig() {

        $this->cache = is_prod();
        $this->debug = !is_prod();
        
        // Load folders for Twig
        // We will add the paths to the base theme after the path for the theme to make sure that Twig will search for paths in child-theme first
        $this->registerTwigFolder( 'base', get_stylesheet_directory() . '/views' );
        $this->registerTwigFolder( 'base', get_template_directory() . '/views');
        $this->registerTwigFolder( 'widgets', Config::pluginPath( '/widgets' ) );
        $this->registerTwigFolder( 'plugins', Config::pluginPath( '/plugins' ) );
        $this->registerTwigFolder( Config::getPluginSlug(), Config::pluginPath( '/src/views' ) );
        
        // All methods from TwigFunctions will be added to twig as global functions.
        // The method-name will be converted to underscore to match PSR-1 for "global functions"
        // Ex. methodName => method_name
        $twig_functions = new TwigFunctions();
        foreach ( get_class_methods( $twig_functions ) as $func_name ) {
            $this->addFunction( Str::snake( $func_name ), [ $twig_functions, $func_name ] );
        }

        $this->addWpFunction('wp_editor');
        $this->addWpFunction( 'get_template_part' );
    }
}