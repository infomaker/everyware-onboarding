<?php namespace Customer;

use Everyware\PagePart;
use EwTools\Models\Page;
use EwTools\Models\SettingsParameter;
use EwTools\Handler\Menus;


/**
 * EwcFooter
 *
 * @link    http://infomaker.se
 * @package Customer
 * @since   Customer\EwcFooter 1.0.0
 */
class EwcFooter implements PagePart {
    
    public function getTemplateData() {
        
        return [
            'about'        => $this->getFooterMenuById('footer-menu-about'),
            'advert'       => $this->getFooterMenuById('footer-menu-ads'),
            'order'        => $this->getFooterMenuById('footer-menu-order'),
            'services'     => $this->getFooterMenuById('footer-menu-services'),
            'apps'         => $this->getAppsSection(),
            'social_media' => $this->getSocialMediaList(),
            'copyright'    => [
                'year'    => EwcDate::now()->year,
                'company' => Config::getCompany()
            ],

        ];
    }

    /**
    * Setup menu item
    *
    * @param $item
    *
    * @return array
    */
   private function setupMenuItem($item) {
       return [
           'title'    => $item['title'],
           'url'      => $item['url']
       ];
    }

    /**
     * Get Footer Menu by id
     * 
     * @param $menuId
     *
     * @return array
     */
    private function getFooterMenuById($menuId) {
      
        $footerMenu = Menus::getFromLocation($menuId);

        if(!$footerMenu) {
          return [];
        }

        $items = $footerMenu->items(true)->map(function ($item) {
          return $this->setupMenuItem($item);
        })->toArray();

        return $this->createSection($footerMenu->name, $items);
    }
    
    private function getAppsSection() {

        $android_app_url = SettingsParameter::getValue( 'android_app_url' );
        $ios_app_url = SettingsParameter::getValue( 'ios_app_url' );

        return $this->createSection( 'Ladda ner appar', [
            [
                'name' => 'Android',
                'icon' => 'android',
                'url'  => $android_app_url
            ],
            [
                'name' => 'iOS',
                'icon' => 'os',
                'url'  => $ios_app_url
            ]
        ] );
    }
    
    private function getSocialMediaList() {
        $domain = site_url();
        $facebook_url = SettingsParameter::getValue( 'facebook_url' );
        $twitter_url = SettingsParameter::getValue( 'twitter_url' );
        $instagram_url = SettingsParameter::getValue( 'instagram_url' );
        $linkedin_url = SettingsParameter::getValue( 'linkedin_url' );
        return [
            [
                'title' => 'Rss',
                'name'  => 'rss',
                'icon'  => 'rss',
                'url' => "{$domain}/feed/"
            ],
            [
                'title' => 'Facebook',
                'name'  => 'facebook',
                'icon'  => 'facebook',
                'url'   => $facebook_url
            ],
            [
                'title' => 'Twitter',
                'name'  => 'twitter',
                'icon'  => 'twitter',
                'url'   => $twitter_url
            ],
            [
                'title' => 'Instagram',
                'name'  => 'instagram',
                'icon'  => 'instagram',
                'url'   => $instagram_url
            ],
            [
                'title' => 'Linked In',
                'name'  => 'linked-in',
                'icon'  => 'lndi',
                'url'   => $linkedin_url
            ]
        ];
    }

    /**
     * @param $title
     * @param $list
     *
     * @return array
     */
    private function createSection( $title, $list ) {
        return [
            'section_title' => $title,
            'section_list'  => $list
        ];
    }

    /**
     * @param      $title
     * @param null $url
     *
     * @return array
     */
    private function createPageLink( $title, $url = null ) {
        if( $url === null ) {
            $page = Page::createFromTitle( $title );
            $url  = $page instanceof Page ? $page->permalink() : '#';
        }
        
        return [
            'title' => $title,
            'url'   => $url
        ];
    }
    
}