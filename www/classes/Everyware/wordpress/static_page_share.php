<?php namespace WWW\Classes\Everyware\Wordpress;

/**
 * Class Static_Page_Share
 * @package WWW\Classes\Everyware\Wordpress
 */
class Static_Page_Share {

	/**
	 * Static_Page_Share constructor.
	 */
	function __construct() {
        global $pagenow;

        if( is_admin() && $pagenow === 'post.php' || $pagenow === 'post-new.php' ) {
            add_action( 'add_meta_boxes', array( &$this, 'setup_metabox' ) );
            add_action( 'save_post', array( &$this, 'update_page' ) );
        }

        add_filter('the_content', function($original_content) {
            global $post;

            $content_data = get_post_meta( $post->ID, 'network_page_content', true );

            if( !empty( $content_data ) ) {
                $decoded_data = json_decode( base64_decode( $content_data ) );

                $blog_id = intval( $decoded_data->blog_id );
                if( get_current_blog_id() !== $blog_id ) {
                    switch_to_blog( $blog_id );
                }

                $content = '';
                $cache_key = "network_page_" . $decoded_data->page_id . '_siteid_' . $decoded_data->blog_id;
                if( false === ( $content = get_transient( $cache_key ) ) ) {
                    if( false !== ( $post = get_post( $decoded_data->page_id ) ) ) {
                        set_transient( $cache_key, $post->post_content, DAY_IN_SECONDS * 7);
                        $content = $post->post_content;
                    }
                }

                restore_current_blog();
                return wpautop( stripslashes($content) );
            }

            return $original_content;
        });
    }


    /**
     * Function to initialize metabox on page sidebar
     */
    function setup_metabox() {
        add_meta_box( 'static_page_side_meta', 'Network Share', array( &$this, 'render_side_meta' ), 'Page', 'side', 'core', null );
    }

    /**
     * Function to render the metabox
     */
    function render_side_meta() {
        $network_pages = $this->get_network_shared_pages();
        $is_shared = isset( $_REQUEST['post'] ) ? get_post_meta( $_REQUEST['post'] , 'network_shared_page', true) === "1" ? true : false : false;
        $share_data = isset( $_REQUEST['post'] ) ? get_post_meta( $_REQUEST['post'], 'network_page_content', true) : '';
    ?>
        <p>
            <input name="network-sharable" id="network-checkbox" type="checkbox" <?php echo $is_shared ? 'checked="checked' : '' ?> />
            <label for="network-checkbox">Share this page on network
            </label>
        </p>

        <strong><label for="network-select">Link network page:</label></strong>
        <select name="network-page-content" id="network-select">
            <option value="false">(No network page)</option>
            <?php
            foreach( $network_pages as $page ): ?>
                <optgroup label="<?php echo $page['name'];?>">
                    <?php foreach ( $page['pages'] as $shared_page ):
                            $page_data = base64_encode(
                                json_encode(
                                    array(
                                        "blog_id" => $page["siteid"],
                                        "page_id" => $shared_page["page_id"]
                                    )
                                )
                            );
                        ?>
                        <option value="<?php echo $page_data; ?>" <?php echo $page_data === $share_data ? 'selected' : '' ?>>
                            <?php echo $shared_page['page_title']; ?>
                        </option>
                    <?php endforeach; ?>
                </optgroup>
            <?php endforeach; ?>
        </select>
    <?php
    }

    /**
     * @return array
     *
     * Function to get all pages that is network sharable
     */
    private function get_network_shared_pages() {
        $ret_pages = [];
        $sites = wp_get_sites();

        $current_blog_id = get_current_blog_id();

        foreach( $sites as $site ) {

            if( isset( $site['blog_id'] ) ) {
                $switched = false;
                $site_id = intval( $site['blog_id'] );

                if( $current_blog_id !== $site_id) {
                    switch_to_blog( $site_id );
                    $current_blog_id = $site_id;
                    $switched = true;
                }

                $args = array(
                    'post_type'         => 'page',
                    'posts_per_page'    => -1,
                    'orderby'           => 'title',
                    'order'             => 'ASC',
                    'meta_key'          => 'network_shared_page',
                    'meta_value'        => true
                );

                $posts = get_posts( $args );

                $site_pages = array(
                    'name'      => get_bloginfo('name'),
                    'siteid'    => $site_id,
                    'pages' => []
                );

                foreach( $posts as $post ) {
                    array_push( $site_pages['pages'], [
                        'page_title' => $post->post_title,
                        'page_id' => $post->ID
                    ]);
                }

                if( count( $site_pages['pages'] ) > 0 ) {
                    array_push( $ret_pages, $site_pages );
                }

                if( $switched ) {
                    restore_current_blog();
                }
            }
        }

        return $ret_pages;
    }

    /**
     * Function called when page is saved to update/remove transients/meta data
     */
    function update_page() {
        if( isset( $_REQUEST['post_ID'] ) ) {
            $network_share = isset( $_REQUEST['network-sharable'] ) && $_REQUEST['network-sharable'] === 'on' ? true : false;
            $cache_key = "network_page_" . $_REQUEST['post_ID'] . '_siteid_' . get_current_blog_id();

            if( $network_share ) {
                update_post_meta( $_REQUEST['post_ID'], 'network_shared_page', true );
                set_transient( $cache_key, $_REQUEST['content'], 3600 * 24 * 7);
            } else {
                delete_transient( $cache_key );
                delete_post_meta( $_REQUEST['post_ID'], 'network_shared_page');
            }

            if( isset( $_REQUEST['network-page-content'] ) && $_REQUEST['network-page-content'] !== "false" ) {
                update_post_meta( $_REQUEST['post_ID'], 'network_page_content', $_REQUEST['network-page-content'] );
            } else {
                delete_post_meta( $_REQUEST['post_ID'], 'network_page_content' );
            }
        }
    }
}