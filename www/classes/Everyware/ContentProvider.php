<?php namespace Everyware;

/**
 * Interface ContentProvider
 *
 * @link    http://infomaker.se
 * @package Everyware
 * @since   Everyware\ContentProvider 0.1
 */
interface ContentProvider {
    
    /**
     *
     * @param array $params
     *
     * @since 0.1
     * @return array
     */
    public function queryWithRequirements( array $params );
    
    /**
     * @param string $reference
     *
     * @since 1.0.0
     * @return void
     */
    public function setPropertyMap( $reference );
}