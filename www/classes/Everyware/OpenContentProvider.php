<?php namespace Everyware;

use Customer\Config;
use OcAPI;

/**
 * OpenContentProvider
 *
 * @link    http://infomaker.se
 * @package Everyware
 * @since   Everyware\OpenContentProvider 0.1
 */
class OpenContentProvider implements ContentProvider {
    
    /**
     * @var PropertyMapper
     */
    protected $property_mapper;
    
    /**
     * @var string
     */
    protected $content_reference;
    
    /**
     * @var PropertyParser
     */
    protected $property_parser;
    
    /**
     * @var OcAPI
     */
    private $oc;
    
    /**
     * @var string
     */
    private $query_params;
    
    /**
     * @var bool
     */
    private $use_cache;
    
    /**
     * OpenContentProvider constructor.
     *
     * @since    0.1
     *
     * @param OcAPI $oc
     * @param array $query_params
     * @param bool  $use_cache
     *
     * @internal param string $query
     */
    public function __construct( OcAPI $oc, array $query_params = [], $use_cache = true ) {
        $this->oc           = $oc;
        $this->query_params = $query_params;
        $this->use_cache    = $use_cache;
    }
    
    /**
     * Setup properties that will be sent to a search-request to Open Content if no properties has been specified.
     *
     * @param array $properties
     *
     * @since 1.0.0
     * @return void
     */
    public static function setupDefaultSearchProperties( array $properties = [] ) {
        add_filter( 'ew_apply_default_search_properties', function ( $filtered_properties ) use ( $properties ) {
            return array_merge( $filtered_properties, $properties );
        } );
    }
    
    /**
     * Setup properties that the notifier will send to each contenttype
     *
     * @param string $contenttype
     * @param array  $properties
     *
     * @since 1.0.0
     * @return void
     */
    public static function setupNotifierProperties( $contenttype, array $properties = [] ) {
        $contenttype_var = strtolower( $contenttype );
        
        add_filter( "ew_notifier_update_{$contenttype_var}_properties", function ( $filtered_properties ) use ( $properties ) {
            return array_merge( $filtered_properties, $properties );
        } );
    }
    
    /**
     * Fetch content using the combined parameters from setup and the specific requirements from the request.
     *
     * @param array $requirements
     *
     * @since    0.1
     * @return array
     */
    public function queryWithRequirements( array $requirements = [] ) {
        if( $this->content_reference ) {
            $requirements[ 'properties' ] = $this->mergeMappedProperties( $this->content_reference, array_get( $requirements, 'properties', [] ) );
            
            return $this->mapResult( $this->getContent( $requirements ) );
        }
        
        return $this->getContent( $requirements );
    }
    
    /**
     * @param $reference
     *
     * @since 1.0.0
     * @return $this
     */
    public function setPropertyMap( $reference ) {
        $config                  = new Config();
        $this->property_mapper   = new PropertyMapper( $config );
        $this->property_parser   = new PropertyParser( $config );
        $this->content_reference = $reference;
        
        return $this;
    }
    
    /**
     * @param array $result
     *
     * @since 1.0.0
     * @return array
     */
    private function mapResult( array $result = [] ) {
        return $this->property_parser->fromSearch( $this->content_reference, $result )->all();
    }
    
    /**
     * Clean search result from extra info
     *
     * @param $result
     *
     * @since 0.1
     * @return array
     */
    private function cleanResult( array $result = [] ) {
        unset( $result[ 'facet' ], $result[ 'hits' ], $result[ 'duration' ] );
        
        return $result;
    }
    
    /**
     * For convenience setup of class
     *
     * @param array $query_params
     * @param bool  $use_cache
     *
     * @since 0.1
     * @return static
     */
    public static function setup( array $query_params = [], $use_cache = true ) {
        return new static( new OcAPI(), $query_params, $use_cache );
    }
    
    /**
     * @param string $content_reference
     * @param array  $properties
     *
     * @since 1.0.0
     * @return array
     */
    public function mergeMappedProperties( $content_reference, array $properties = [] ) {
        return array_unique( array_merge( $properties, $this->property_mapper->requiredProperties( $content_reference ) ) );
    }
    
    /**
     * @param $requirements
     *
     * @since 1.0.0
     * @return array
     */
    private function getContent( $requirements ) {
        return $this->cleanResult( $this->oc->search( array_replace( $this->query_params, $requirements ), $this->use_cache ) );
    }
}