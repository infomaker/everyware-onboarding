<?php namespace Everyware;

/**
 * PropertyMapper
 *
 * @link    http://infomaker.se
 * @package Everyware
 * @since   Everyware\PropertyMapper 1.0.0
 */
class PropertyMapper {
    
    /**
     * @var array
     */
    private $property_map;
    
    /**
     * PropertyMapper constructor.
     *
     * @since 0.1
     * *
     *
     * @param PropertyMapConfig $config
     */
    public function __construct( PropertyMapConfig $config ) {
        $this->property_map = $config->getPropertyMap();
    }
    
    /**
     * @param string $object_map
     *
     * @since 1.0.0
     * @return array
     */
    public function requiredProperties( $object_map ) {
        if( array_key_exists( $object_map, $this->property_map ) ) {
            return array_flatten( $this->extractProperties( $this->property_map[ $object_map ] ) );
        }
        
        return [];
    }
    
    /**
     *
     *
     * @param $properties
     *
     * @since 1.0.0
     * @return array
     */
    private function extractProperties( $properties ) {
        return array_map( function ( $property = [] ) {
            $property_name = $property[ 'name' ];
            if( $this->hasReference( $property ) ) {
                $referenced_properties = $this->requiredProperties( $this->getReference( $property ) );
                
                array_walk( $referenced_properties, function( &$property, $key, $relation ) {
                    $property = "{$relation}.$property";
                }, $property_name );
                
                return $referenced_properties;
            }
            
            return $property_name;
        }, $properties );
    }
    
    private function hasReference( $property ) {
        return isset( $property[ 'propertyMapReference' ] );
    }
    
    private function getReference( $property ) {
        return $property[ 'propertyMapReference' ];
    }
}