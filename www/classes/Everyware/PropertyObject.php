<?php namespace Everyware;

use ArrayAccess;
use Customer\EwcDate;
use EwTools\Interfaces\Support\Arrayable;
use EwTools\Interfaces\Support\Jsonable;
use JsonSerializable;

/**
 * PropertyObject
 *
 * @link    http://infomaker.se
 * @package Everyware
 * @since   Everyware\PropertyObject 1.0.0
 */
class PropertyObject implements ArrayAccess, Arrayable, Jsonable, JsonSerializable {
    
    /**
     * All of the properties set on the container.
     *
     * @var array
     */
    protected $properties = [];
    
    /**
     * Fill up properties dynamically
     *
     * @param array $properties
     *
     * @since 0.1
     * @return void
     */
    public function fill( array $properties = [] ) {
        foreach ( $properties as $key => $value ) {
            $this->set( $key, $value );
        }
    }
    
    /**
     * Get an property from the container.
     *
     * @param string $key
     * @param mixed  $default
     *
     * @since 0.1
     * @return mixed
     */
    public function get( $key, $default = null ) {
        if( array_key_exists( $key, $this->properties ) ) {
            return $this->properties[ $key ];
        }
        
        return value( $default );
    }
    
    /**
     * Retrieve a bool version of a value
     *
     * @param $key
     *
     * @since 1.0.0
     * @return EwcDate
     */
    public function getBoolValue( $key ) {
        return filter_var( $this->getSingleValue( $key ), FILTER_VALIDATE_BOOLEAN );
    }
    
    /**
     * Retrieve a date version of a value
     *
     * @param $key
     *
     * @since 1.0.0
     * @return EwcDate
     */
    public function getDateValue( $key ) {
        return EwcDate::createFromOcString( $this->getSingleValue( $key ) );
    }
    
    /**
     * Get property as multi value (array)
     *
     * @param string $property
     *
     * @since 0.1
     * @return array
     */
    public function getMultiValue( $property ) {
        return $this->offsetExists( $property ) ? (array)$this->get( $property ) : [];
    }
    
    /**
     * Get property as a single value
     *
     * @param string $property
     *
     * @since 0.1
     * @return string
     */
    public function getSingleValue( $property ) {
        $value = $this->getMultiValue( $property );
        
        return ! empty( $value ) ? array_shift( $value ) : '';
    }
    
    /**
     * Get an property from the container.
     *
     * @param string $key
     * @param mixed  $value
     *
     * @since 0.1
     * @return void
     */
    public function set( $key, $value ) {
        $this->properties[ $key ] = $value;
    }
    
    /**
     * Get the properties from the container.
     *
     * @since 0.1
     * @return array
     */
    public function getProperties() {
        return $this->properties;
    }
    
    /**
     * Convert the Fluent instance to an array.
     *
     * @since 0.1
     * @return array
     */
    public function toArray() {
        return $this->properties;
    }
    
    /**
     * Convert the object into something JSON serializable.
     *
     * @since 0.1
     * @return array
     */
    public function jsonSerialize() {
        return $this->toArray();
    }
    
    /**
     * Convert the Fluent instance to JSON.
     *
     * @param int $options
     *
     * @since 0.1
     * @return string
     */
    public function toJson( $options = 0 ) {
        return json_encode( $this->jsonSerialize(), $options );
    }
    
    /**
     * Determine if the given offset exists.
     *
     * @param string $offset
     *
     * @since 0.1
     * @return bool
     */
    public function offsetExists( $offset ) {
        return isset( $this->{$offset} );
    }
    
    /**
     * Get the value for a given offset.
     *
     * @param string $offset
     *
     * @since 0.1
     * @return mixed
     */
    public function offsetGet( $offset ) {
        return $this->{$offset};
    }
    
    /**
     * Set the value at the given offset.
     *
     * @param string $offset
     * @param mixed  $value
     *
     * @since 0.1
     * @return void
     */
    public function offsetSet( $offset, $value ) {
        $this->{$offset} = $value;
    }
    
    /**
     * Unset the value at the given offset.
     *
     * @param string $offset
     *
     * @since 0.1
     * @return void
     */
    public function offsetUnset( $offset ) {
        unset( $this->{$offset} );
    }
    
    /**
     * Handle dynamic calls to the container to set properties.
     *
     * @param string $method
     * @param array  $parameters
     *
     * @since 0.1
     * @return $this
     */
    public function __call( $method, $parameters ) {
        $this->properties[ $method ] = count( $parameters ) > 0 ? $parameters[ 0 ] : true;
        
        return $this;
    }
    
    /**
     * Dynamically retrieve the value of an property.
     *
     * @param string $key
     *
     * @since 0.1
     * @return mixed
     */
    public function __get( $key ) {
        return $this->get( $key );
    }
    
    /**
     * Dynamically set the value of an property.
     *
     * @param string $key
     * @param mixed  $value
     *
     * @since 0.1
     * @return void
     */
    public function __set( $key, $value ) {
        $this->set( $key, $value );
    }
    
    /**
     * Dynamically check if an property is set.
     *
     * @param string $key
     *
     * @since 0.1
     * @return bool
     */
    public function __isset( $key ) {
        return isset( $this->properties[ $key ] );
    }
    
    /**
     * Dynamically unset an property.
     *
     * @param string $key
     *
     * @since 0.1
     * @return void
     */
    public function __unset( $key ) {
        unset( $this->properties[ $key ] );
    }
}