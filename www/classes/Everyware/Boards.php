<?php

namespace Everyware;

class Boards
{
    /**
     * Defaults for fetching pages in Wordpress
     *
     * @var array
     */
    protected static $default_options = [
        'sort_order'     => 'ASC',
        'sort_column'    => 'post_title',
        'post_type'      => 'everyboard',
        'posts_per_page' => -1,
        'post_status'    => 'publish',
    ];

    /**
     * Retrieve all pages with optional options for filtering
     *
     * @param array $options
     *
     * @uses  get_pages()
     *
     * @since 0.1
     * @return array
     */
    public static function get($options = [])
    {
        return get_posts(array_replace_recursive(static::$default_options, $options));
    }

    /**
     * Retrieve an array of page data for use in select-box "value" and "text" and "id".
     * Add options to filter the resulted list
     *
     * @param array $options
     * @param bool  $use_hierarchy
     *
     * @uses  get_permalink()
     *
     * @since 0.1
     * @return array
     */
    public static function getSelectData($options = [])
    {
        $boards = static::get($options);

        return array_map(function ($board) {
            return [
                'text'  => $board->post_title,
                'value' => $board->ID,
            ];
        }, $boards);
    }
}