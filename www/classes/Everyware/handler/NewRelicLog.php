<?php namespace Everyware\handler;

use Exception;

/**
 * NewRelicLog
 *
 * @link    http://infomaker.se
 * @package Everyware\handler
 * @since   Everyware\handler\NewRelicLog 1.0.0
 */
class NewRelicLog {
    
    /**
     * Send error to New Relic if enabled else to log
     *
     * @param string         $message
     * @param Exception|null $exception
     */
    public static function error( $message, $exception = null ) {
        
        if( is_prod() && extension_loaded( 'newrelic' ) ) {
            newrelic_notice_error( $message, $exception );
        } else {
            error_log( $exception instanceof Exception ? sprintf( '%s: %s', $message, $exception->getMessage() ) : $message );
        }
    }
}