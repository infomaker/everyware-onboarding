<?php namespace Everyware\handler;

use Everyware\PagePart;
use EwTools\Twig\View;

/**
 * ViewHandler
 *
 * @link    http://infomaker.se
 * @package Everyware\handler
 * @since   Everyware\handler\ViewHandler 1.0.0
 */
class ViewHandler {
    
    public static function renderPagePart( $template, PagePart $part) {
        View::render( "@base/page/part/{$template}", $part->getTemplateData() );
    }
    
    public static function generatePagePart( $template, PagePart $part) {
       return View::generate( "@base/page/part/{$template}", $part->getTemplateData() );
    }
}