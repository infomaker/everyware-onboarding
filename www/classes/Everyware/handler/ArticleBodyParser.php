<?php namespace Everyware\handler;

use Everyware\Newsml\Item;
use Everyware\Newsml\NewsMLTransformerManager;
use Everyware\PropertyObject;

/**
 * ArticleBodyParser
 *
 * @link    http://infomaker.se
 * @package Everyware\handler
 * @since   Everyware\handler\ArticleBodyParser 1.0.0
 */
class ArticleBodyParser {
    
    /**
     * Contains a sortable map of all items
     *
     * @var array
     */
    private $item_map;
    
    /**
     * Contains a list of all valid body items
     *
     * @var array
     */
    private $body = [];
    
    public function __construct( PropertyObject $object, $property_name = 'body_raw' ) {
        $transformer   = NewsMLTransformerManager::createTransformer( $property_name );
        $body_elements = $transformer->transform( $object );
        
        foreach ( $body_elements as $index => $body_element ) {
            if( $body_element instanceof Item ) {
                $this->addItem( $body_element );
            }
        }
    }
    
    private function addItem( Item $item ) {
        $this->item_map[] = $item->getType();
        $this->body[]     = $item;
    }
    
    /**
     * @since 1.0.0
     * @return array
     */
    public function getBody() {
        return $this->body;
    }
    
    /**
     * Determine if the body contains a certain type
     *
     * @param string $type
     *
     * @since 1.0.0
     * @return bool
     */
    public function hasType( $type ) {
        return in_array( $type, $this->item_map, true );
    }
    
    /**
     * Retrieve type of first item
     *
     * @since 1.0.0
     * @return mixed
     */
    public function firstType() {
        return collect( $this->item_map )->first();
    }
    
    /**
     * Retrieve the item of the first item found of specified type
     *
     * @param string $type
     *
     * @since 1.0.0
     * @return int|string|false the key for needle if it is found in the
     * array, false otherwise.
     */
    public function firstOfType( $type ) {
        return array_search( $type, $this->item_map, true );
    }
    
    /**
     * @param $type
     *
     * @since 1.0.0
     * @return array
     */
    public function allPosOfType( $type ) {
        return array_keys( $this->item_map, $type );
    }
    
    /**
     * @param $type
     *
     * @since 1.0.0
     * @return void
     */
    public function removeType( $type ) {
        foreach($this->allPosOfType( $type ) as $key) {
            unset($this->item_map[$key], $this->body[$key]);
        }
    }
    
    /**
     *
     *
     * @param $type
     *
     * @since 1.0.0
     * @return array
     */
    public function allOfType( $type ) {
        $items = [];
    
        foreach($this->allPosOfType( $type ) as $key) {
            $items[] = $this->body[$key];
        }
        
        return $items;
    }
}