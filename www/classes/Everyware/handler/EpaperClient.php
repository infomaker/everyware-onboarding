<?php namespace Everyware\handler;

use EwTools\Support\Str;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Epaper
 *
 * @link    http://infomaker.se
 * @package Everyware\handler
 * @since   Everyware\handler\EpaperClient 1.0.0
 */
class EpaperClient {
    
    private static $base_url = 'http://dev.im-epaper.se/api/v2/products';
    
    private static $client;
    
    private $id;
    
    /**
     * Epaper constructor.
     *
     * @since 0.1
     * * @param $id
     */
    public function __construct( $id ) {
        $this->id = $id;
    }
    
    public static function create( $id ) {
        return new static( $id );
    }
    
    public function getLatestCover() {
        return $this->createCoverUrl( $this->getLatest() );
    }
    
    public function getLatest() {
        return $this->get( 'latest' );
    }
    
    /**
     * @param $path
     *
     * @since 1.0.0
     * @return null|\Psr\Http\Message\StreamInterface
     */
    public function get( $path ) {
        try {
            $res = $this->getClient()->request( 'GET', $this->createUrl( $path ) );
            
            return json_decode( $res->getBody(), true );
        } catch ( GuzzleException $e ) {
            NewRelicLog::error( 'Problem getting data for Epaper', $e );
        }
        
        return null;
    }
    
    private function createUrl( $path ) {
        return Str::finish( static::$base_url . "/{$this->id}/", $path );
    }
    
    private function createCoverUrl( array $issue = [] ) {
        if( isset( $issue[ 'cloudfront' ] ) ) {
            return Str::finish( $issue[ 'cloudfront' ] . $issue[ 'publicpath' ], implode( '/', [
                $issue[ 'pubdate' ],
                $this->getThumbnail( $issue[ 'parts' ] )
            ] ) );
        }
        
        return '';
    }
    
    public function getThumbnail( array $parts = [], $size = 'large' ) {
        $part       = array_shift( $parts );
        $thumbnails = (array)$part[ 'thumbnails' ];
        
        return "thumbnails/{$thumbnails[$size]}";
    }
    
    /**
     * @since 1.0.0
     * @return Client
     */
    private function getClient() {
        
        if( static::$client === null ) {
            static::$client = new Client();
        }
        
        return static::$client;
    }
}