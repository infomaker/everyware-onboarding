<?php namespace Everyware\handler;

use EwTools\Storage\Cache;
use EwTools\Support\Str;
use EwTools\Support\Utilities;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use SMHI\Models\Forecast;
use SMHI\Settings;
use WP_Http;

/**
 * ForecastClient
 *
 * @link    http://infomaker.se
 * @package Everyware\handler
 * @since   Everyware\handler\Client 1.0.0
 */
class ForecastClient {
    
    private static $base_uri = 'http://ppdyn.smhi.se/produktportal-1.0/ws/products/weatherdata/';
    
    /**
     * @var WP_Http
     */
    private static $client;
    
    /**
     * Settings for forecast
     *
     * @var Settings
     */
    private $settings;
    
    public function __construct() {
        $this->settings = new Settings();
    }
    
    /**
     * @since 1.0.0
     * @return static
     */
    public static function create() {
        return new static();
    }
    
    /**
     * @todo  : Change to use Guzzle instead of curl
     * @since 1.0.0
     * @return array|Forecast
     */
    public function get() {

        if (!$this->settings->hasAllRequiredSettings()) {
            NewRelicLog::error('SMHI Plugin: Missing required settings');

            return [];
        }

        $cache_key = 'ewc_smhi_' . implode( '_', $this->settings->getLocation() );
        
        if( ( $forecast = Cache::get( $cache_key ) ) instanceof Forecast ) {
            return $forecast;
        }
        
        $response = $this->getClient()->get( $this->getForecastPath(), [
            'headers' => [
                'Key' => $this->settings->getApiKey(),
            ]
        ] );
        
        // Check for error
        if( is_wp_error( $response ) ) {
            NewRelicLog::error( 'Problem getting data from SMHI' );
            print_r( '<pre>' );
            var_dump($response->get_error_messages()  );
            print_r( '</pre>' );
            return null;
        }
        
        $forecast = new Forecast( json_decode( wp_remote_retrieve_body( $response ), true ) );
        Cache::set( $cache_key, $forecast, $forecast->getTTL() );
        
        return $forecast;
    }
    
    /**
     * @since 1.0.0
     * @return string
     */
    private function getForecastPath() {
        $location = $this->settings->getLocation();
        
        return static::$base_uri . "{$location['lat']}/{$location['long']}/forecast";
    }
    
    /**
     * @since 1.0.0
     * @return WP_Http
     */
    private function getClient() {
        
        if( static::$client === null ) {
            static::$client = new WP_Http();
        }
        
        return static::$client;
    }
    
    /**
     * @since 1.0.0
     * @return array
     */
    public function getLocation() {
        return $this->settings->getLocation();
    }
    
    public function getPageUrl() {
        return $this->settings->getPageUrl();
    }
}