<?php namespace Everyware;

/**
 * Interface PagePart
 *
 * @link    http://infomaker.se
 * @package Everyware
 * @since   Everyware\PagePart 1.0.0
 */
interface PagePart {
    
    /**
     * Retrieve data to be used in the template
     *
     * @since 1.0.0
     * @return array
     */
    public function getTemplateData();
}