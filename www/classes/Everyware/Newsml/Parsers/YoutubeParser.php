<?php namespace Everyware\Newsml\Parsers;

use Everyware\Newsml\Item;
use Everyware\Newsml\TypeParser;
use SimpleXMLElement;
use SimpleXMLIterator;

/**
 * YoutubeParser
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml\Parsers
 * @since   Everyware\Newsml\Parsers\YoutubeParser 1.0.0
 */
class YoutubeParser implements ItemParser, TypeParser {
    
    const OBJECT_TYPE = 'x-im/youtube';
    
    /**
     * @var LinkParser
     */
    protected $link_parser;
    
    /**
     * ObjectParser constructor.
     *
     * @since 0.1
     *
     * @param ItemParser[]
     */
    public function __construct() {
        $this->link_parser = new LinkParser();
    }
    
    /**
     * @param SimpleXMLIterator $element
     *
     * @since 1.0.0
     * @return array
     */
    public function parse( SimpleXMLIterator $element ) {
        $item = new Item( $element );
        $item->fill( [
            'type'  => $item->getType(),
            'uri'   => $this->createUri( $item->getItemAttribute( 'uri', '' ) ),
            'thumb' => $this->getThumbData( $element )
        ] );
        
        return [ $item ];
    }
    
    private function getThumbData( SimpleXMLIterator $element ) {
        $links   = array_flatten( $this->link_parser->parseLinks( $element ), 1 );
        $content = [];
        foreach ( $links as $link ) {
            if( $link instanceof Item ) {
                if( $link->getType() === 'image/jpg' ) {
                    return $this->parseThumb( $link );
                }
            }
        }
        
        return $content;
    }
    
    /**
     * @param $uri
     *
     * @since 1.0.0
     * @return mixed
     */
    private function createUri( $uri ) {
        return str_replace( 'watch?v=', 'embed/', $uri );
    }
    
    /**
     * Retrieve the type to pars
     *
     * @since 1.0.0
     * @return string
     */
    public function typeToParse() {
        return static::OBJECT_TYPE;
    }
    
    /**
     * @param Item $link
     *
     * @since 1.0.0
     * @return array
     */
    private function parseThumb( Item $link ) {
        return array_replace( [
            'width'  => 0,
            'height' => 0,
            'url'    => $link->getItemAttribute( 'url', '' )
        ], (array)$link->getChild( 'data', [] ) );
    }
}