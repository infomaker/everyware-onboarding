<?php namespace Everyware\Newsml\Parsers;

use Everyware\Newsml\Item;
use Everyware\Newsml\TypeParser;
use SimpleXMLIterator;

/**
 * SocialEmbedParser
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml\Parsers
 * @since   Everyware\Newsml\Parsers\SocialEmbedParser 1.0.0
 */
class SocialEmbedParser implements ItemParser, TypeParser {
    
    const OBJECT_TYPE = 'x-im/socialembed';
    
    /**
     * @var LinkParser
     */
    protected $link_parser;
    
    /**
     * ObjectParser constructor.
     *
     * @since 0.1
     *
     * @param ItemParser[]
     */
    public function __construct() {
        $this->link_parser = new LinkParser( [
            FacebookPostLinkParser::LINK_TYPE => new FacebookPostLinkParser(),
            InstagramLinkParser::LINK_TYPE    => new InstagramLinkParser(),
            TwitterLinkParser::LINK_TYPE      => new TwitterLinkParser()
        ] );
    }
    
    /**
     * @param SimpleXMLIterator $element
     *
     * @since 1.0.0
     * @return array
     */
    public function parse( SimpleXMLIterator $element ) {
        $item = new Item( $element );
        $item->fill( [
            'type'    => $item->getType(),
            'content' => array_flatten( $this->link_parser->parseLinks( $element ), 1 )
        ] );
        
        return [ $item ];
    }
    
    /**
     * Retrieve the type to pars
     *
     * @since 1.0.0
     * @return string
     */
    public function typeToParse() {
        return static::OBJECT_TYPE;
    }
}