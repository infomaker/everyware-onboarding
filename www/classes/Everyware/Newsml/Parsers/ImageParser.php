<?php namespace Everyware\Newsml\Parsers;

use Everyware\Newsml\Item;
use Everyware\Newsml\TypeParser;
use SimpleXMLIterator;

/**
 * ImageParser
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml\Parsers
 * @since   Everyware\Newsml\Parsers\ImageParser 1.0.0
 */
class ImageParser implements ItemParser, TypeParser {
    
    const OBJECT_TYPE = 'x-im/image';
    
    /**
     * @var LinkParser
     */
    protected $link_parser;
    
    /**
     * ObjectParser constructor.
     *
     * @since 0.1
     *
     * @param ItemParser[]
     */
    public function __construct() {
        $this->link_parser = new LinkParser([
            ImageLinkParser::LINK_TYPE => new ImageLinkParser()
        ]);
    }
    
    /**
     * @param SimpleXMLIterator $element
     *
     * @since 1.0.0
     * @return mixed
     */
    public function parse( SimpleXMLIterator $element ) {
        return array_flatten( $this->link_parser->parseLinks( $element ), 1 );
    }
    
    /**
     * Retrieve the type to pars
     *
     * @since 1.0.0
     * @return string
     */
    public function typeToParse() {
        return static::OBJECT_TYPE;
    }
}