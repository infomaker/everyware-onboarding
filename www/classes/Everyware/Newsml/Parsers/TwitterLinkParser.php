<?php namespace Everyware\Newsml\Parsers;

use Everyware\Newsml\Item;
use SimpleXMLIterator;

/**
 * TwitterLinkParser
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml\Parsers
 * @since   Everyware\Newsml\Parsers\TwitterLinkParser 1.0.0
 */
class TwitterLinkParser implements ItemParser {
    
    const LINK_TYPE = 'x-im/tweet';
    
    /**
     * @param SimpleXMLIterator $element
     *
     * @since 1.0.0
     * @return array
     */
    public function parse( SimpleXMLIterator $element ) {
        $item  = new Item( $element );
        $item->fill( $item->getItemAttributes() );
    
        return [ $item ];
    }
}