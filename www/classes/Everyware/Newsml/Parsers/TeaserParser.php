<?php namespace Everyware\Newsml\Parsers;

use Everyware\Newsml\Objects\TeaserItem;
use Everyware\Newsml\TypeParser;
use SimpleXMLIterator;

/**
 * TeaserParser
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml\Parsers
 * @since   Everyware\Newsml\Parsers\TeaserParser 1.0.0
 */
class TeaserParser implements ItemParser, TypeParser {
    
    const OBJECT_TYPE = 'x-im/teaser';
    
    /**
     * @var LinkParser
     */
    protected $link_parser;
    
    /**
     * ObjectParser constructor.
     *
     * @since 0.1
     *
     * @param ItemParser[]
     */
    public function __construct() {
        $this->link_parser = new LinkParser([
            ImageLinkParser::LINK_TYPE => new ImageLinkParser()
        ]);
    }
    
    /**
     * @param SimpleXMLIterator $element
     *
     * @since 1.0.0
     * @return mixed
     */
    public function parse( SimpleXMLIterator $element ) {
        $item = new TeaserItem( $element );
        $item->fill( array_replace( $this->getData( $element->data ), [
            'type'  => $item->getType(),
            'links' => $this->link_parser->parseLinks( $element )
        ] ) );
        
        return [ $item ];
    }
    
    protected function getData( $data = null ) {
        $default_data = [
            'headline' => '',
            'text'     => '',
            'subject'  => ''
        ];
        
        if( $data === null ) {
            return $default_data;
        }
        
        return array_replace( $default_data, array_filter( [
            'headline' => (string)$data->title,
            'text'     => (string)$data->text,
            'subject'  => (string)$data->subject
        ] ) );
    }
    
    /**
     * Retrieve the type to pars
     *
     * @since 1.0.0
     * @return string
     */
    public function typeToParse() {
        return static::OBJECT_TYPE;
    }
}