<?php namespace Everyware\Newsml\Parsers;

use Everyware\Newsml\Item;
use Everyware\Newsml\TypeParser;
use SimpleXMLIterator;

/**
 * ContentPartParser
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml\Parsers
 * @since   Everyware\Newsml\Parsers\ContentPartParser 1.0.0
 */
class ContentPartParser implements ItemParser, TypeParser {
    
    const OBJECT_TYPE = 'x-im/content-part';
    
    /**
     * @var LinkParser
     */
    protected $link_parser;
    
    /**
     * @var ItemParser[]
     */
    private $text_parsers;
    
    /**
     * ContentPartParser constructor.
     *
     * @since 0.1
     * * @param ElementParser $parser
     */
    public function __construct( ElementParser $parser ) {
        
        $this->text_parsers = [
            ElementParser::TAG => $parser
        ];
        
        $this->link_parser = new LinkParser();
    }
    
    /**
     * @param SimpleXMLIterator $element
     *
     * @since 1.0.0
     * @return array
     */
    public function parse( SimpleXMLIterator $element ) {
        $item = new Item( $element );
        
        $item->fill( array_replace( $this->getData( $element ), [
            'type'         => $item->getType(),
            'content_type' => $this->getContentType( $element )
        ] ) );
        
        return [ $item ];
    }
    
    /**
     * @param SimpleXMLIterator $element
     *
     * @return array
     * @since 1.0.0
     */
    protected function getData( SimpleXMLIterator $element ) {
        $default_data = [
            'title'   => '',
            'text'    => [],
            'subject' => ''
        ];
        
        if( ! property_exists( $element, 'data' ) ) {
            return $default_data;
        }
        
        $data = $element->data->children();
        
        foreach ( $data as $tag => $item ) {
            $default_data[$tag] = $tag === 'text' ? $this->parseText( $item->children() ) : (string)$item;
        }
        
        return $default_data;
    }
    
    /**
     * @param SimpleXMLIterator $text_iterator
     *
     * @since 1.0.0
     * @return string
     */
    private function parseText( SimpleXMLIterator $text_iterator ) {
        $items = [];
        foreach ( $text_iterator as $tag => $item ) {
            $parser  = array_get( $this->text_parsers, $tag, null );
            $items[] = $parser instanceof ItemParser ? $parser->parse( $item ) : new Item( $item );
        }
        
        return array_flatten( $items, 1 );
    }
    
    /**
     * Retrieve the type of the content part
     *
     * @param SimpleXMLIterator $element
     *
     * @since 1.0.0
     * @return string
     */
    private function getContentType( SimpleXMLIterator $element ) {
        $links = $this->link_parser->parseLinks( $element );
        if( ! empty( $links ) ) {
            foreach ( $links as $index => $link ) {
                if( $link instanceof Item ) {
                    $attributes = $link->getItemAttributes();
                    if( array_get( $attributes, 'rel', '' ) === 'content-part' ) {
                        return str_replace( 'im:/content-part/', '', array_get( $attributes, 'uri', '' ) );
                    }
                }
            }
        }
        
        return '';
    }
    
    /**
     * Retrieve the type to pars
     *
     * @since 1.0.0
     * @return string
     */
    public function typeToParse() {
        return static::OBJECT_TYPE;
    }
}