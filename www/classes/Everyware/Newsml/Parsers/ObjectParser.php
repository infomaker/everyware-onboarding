<?php namespace Everyware\Newsml\Parsers;

use SimpleXMLIterator;

/**
 * ObjectParser
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml\Parsers
 * @since   Everyware\Newsml\Parsers\ObjectParser 1.0.0
 */
class ObjectParser implements ItemParser {
    
    const TAG = 'object';
    
    /**
     * Contains a group of parsers
     * @var ItemParser[]
     */
    private $parsers;
    
    /**
     * ObjectParser constructor.
     *
     * @since 0.1
     *
     * @param ItemParser[]
     */
    public function __construct( array $parsers ) {
        $this->parsers = $parsers;
    }
    
    private function getType( SimpleXMLIterator $el ) {
        return array_get( array_get( (array)$el->attributes(), '@attributes', [] ), 'type', '' );
    }
    
    private function getParser( SimpleXMLIterator $el ) {
        return array_get( $this->parsers, $this->getType($el), null );
    }
    
    /**
     * @param SimpleXMLIterator $element
     *
     * @since 1.0.0
     * @return mixed
     */
    public function parse( SimpleXMLIterator $element ) {
        $parser = $this->getParser($element);
        
        if( $parser instanceof ItemParser ) {
            return $parser->parse($element);
        }
    
        return [];
    }
}