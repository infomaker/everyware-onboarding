<?php namespace Everyware\Newsml\Parsers;

use Everyware\Newsml\Item;
use SimpleXMLIterator;

/**
 * InstagramLinkParser
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml\Parsers
 * @since   Everyware\Newsml\Parsers\InstagramLinkParser 1.0.0
 */
class InstagramLinkParser implements ItemParser {
    
    const LINK_TYPE = 'x-im/instagram';
    
    /**
     * @param SimpleXMLIterator $element
     *
     * @since 1.0.0
     * @return array
     */
    public function parse( SimpleXMLIterator $element ) {
        $item = new Item( $element );
        $item->fill( array_replace( $item->getItemAttributes(), [
            'embed_url' => 'https://api.instagram.com/oembed/?url=' . $item->getItemAttribute( 'url' )
        ] ) );
        
        return [ $item ];
    }
}