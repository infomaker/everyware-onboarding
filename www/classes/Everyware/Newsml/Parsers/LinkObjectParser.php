<?php namespace Everyware\Newsml\Parsers;

use Everyware\Newsml\Item;
use Everyware\Newsml\TypeParser;
use SimpleXMLIterator;

/**
 * LinkObjectParser
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml\Parsers
 * @since   Everyware\Newsml\Parsers\LinkObjectParser 1.0.0
 */
class LinkObjectParser implements ItemParser, TypeParser {
    
    const OBJECT_TYPE = 'x-im/link';
    
    /**
     * @var LinkParser
     */
    protected $link_parser;
    
    /**
     * ObjectParser constructor.
     *
     * @since 0.1
     */
    public function __construct() {
        $this->link_parser = new LinkParser();
    }
    
    /**
     * @param SimpleXMLIterator $element
     *
     * @since 1.0.0
     * @return mixed
     */
    public function parse( SimpleXMLIterator $element ) {
        
        $item = new Item( $element );
        $item->fill( $this->getLinkAttributes($element) );
        
        return [ $item ];
    }
    
    /**
     * @param SimpleXMLIterator $element
     *
     * @since 1.0.0
     * @return array
     */
    private function getLinkAttributes( SimpleXMLIterator $element ) {
        $links = $this->link_parser->parseLinks( $element );
        if( ! empty( $links ) ) {
            foreach ( $links as $index => $link ) {
                if( $link instanceof Item ) {
                    $attributes = $link->getItemAttributes();
                    if( array_get( $attributes, 'rel', '' ) === 'self' ) {
                        return $attributes;
                    }
                }
            }
        }
        
        return [];
    }
    
    /**
     * Retrieve the type to pars
     *
     * @since 1.0.0
     * @return string
     */
    public function typeToParse() {
        return static::OBJECT_TYPE;
    }
}