<?php namespace Everyware\Newsml\Parsers;

use Everyware\handler\NewRelicLog;
use SimpleXMLElement;
use SimpleXMLIterator;

/**
 * GroupParser
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml\Parsers
 * @since   Everyware\Newsml\Parsers\GroupParser 1.0.0
 */
class GroupParser implements ItemParser {
    
    const TAG = 'group';
    
    /**
     * @var ItemParser[]
     */
    private $parsers;
    
    public function __construct( ElementParser $element_parser, ObjectParser $object_parser ) {
        $this->parsers = [
            ElementParser::TAG => $element_parser,
            ObjectParser::TAG  => $object_parser,
            self::TAG  => $this
        ];
    }
    
    /**
     * @param SimpleXMLIterator $element
     *
     * @since 1.0.0
     * @return array
     */
    public function parse( SimpleXMLIterator $element ) {
        $items = [];
        foreach ( $element as $tag => $item ) {
            $parser = array_get( $this->parsers, $tag, null );
        
            if( $parser instanceof ItemParser ) {
                $items[] = $parser->parse($item);
            }
        }
    
        return array_flatten($items, 1);
    }
}