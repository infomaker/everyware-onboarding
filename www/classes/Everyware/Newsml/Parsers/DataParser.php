<?php namespace Everyware\Newsml\Parsers;

use SimpleXMLIterator;

/**
 * DataParser
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml\Parsers
 * @since   Everyware\Newsml\Parsers\DataParser 1.0.0
 */
class DataParser implements ItemParser {
    
    /**
     * Contains a group of parsers
     *
     * @var ItemParser[]
     */
    private $parsers;
    
    /**
     * ObjectParser constructor.
     *
     * @since 0.1
     *
     * @param ItemParser[]
     */
    public function __construct( array $parsers = [] ) {
        $this->parsers = $parsers;
    }
    
    /**
     * @param SimpleXMLIterator $element
     *
     * @since 1.0.0
     * @return array
     */
    public function parse( SimpleXMLIterator $element ) {
        if( property_exists( $element, 'data' ) ) {
            $data_iterater = $element->data->children();
            
            $data = [];
            foreach ( $data_iterater as $tag => $item ) {
                $parser       = array_get( $this->parsers, $tag, null );
                $data[ $tag ] = $parser instanceof ItemParser ? $parser->parse( $item ) : (string)$item;
            }
            
            return $data;
        }
        
        return [];
    }
}