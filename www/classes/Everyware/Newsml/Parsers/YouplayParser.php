<?php namespace Everyware\Newsml\Parsers;

use Everyware\Newsml\Item;
use Everyware\Newsml\TypeParser;
use SimpleXMLIterator;

/**
 * YouplayParser
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml\Parsers
 * @since   Everyware\Newsml\Parsers\YouplayParser 1.0.0
 */
class YouplayParser implements ItemParser, TypeParser {
    
    const OBJECT_TYPE = 'x-im/youplay';
    
    /**
     * @var LinkParser
     */
    protected $link_parser;
    
    /**
     * @var DataParser
     */
    protected $data_parser;
    
    /**
     * ObjectParser constructor.
     *
     * @since 0.1
     *
     * @param ItemParser[]
     */
    public function __construct() {
        $this->link_parser = new LinkParser( [
            ImageLinkParser::LINK_TYPE => new ImageLinkParser()
        ] );
        
        $this->data_parser = new DataParser();
    }
    
    /**
     * @param SimpleXMLIterator $element
     *
     * @since 1.0.0
     * @return array
     */
    public function parse( SimpleXMLIterator $element ) {
        $item = new Item( $element );
        $item->fill( $this->data_parser->parse( $element ) );
        $item->set( 'thumb', $this->getThumbData( $element ) );
        
        return [ $item ];
    }
    
    protected function getThumbData( SimpleXMLIterator $element ) {
        $links   = array_flatten( $this->link_parser->parseLinks( $element ), 1 );
        $content = [];
        foreach ( $links as $link ) {
            if( $link instanceof Item ) {
                if( $link->getType() === 'image/jpg' ) {
                    return $this->parseThumb( $link );
                }
            }
        }
        
        return $content;
    }
    
    /**
     * Retrieve the type to pars
     *
     * @since 1.0.0
     * @return string
     */
    public function typeToParse() {
        return static::OBJECT_TYPE;
    }
    
    /**
     * @param Item $link
     *
     * @since 1.0.0
     * @return array
     */
    private function parseThumb( Item $link ) {
        return array_replace( [
            'width'  => 0,
            'height' => 0,
            'url'    => $link->getItemAttribute( 'url', '' )
        ], (array)$link->getChild( 'data', [] ) );
    }
}