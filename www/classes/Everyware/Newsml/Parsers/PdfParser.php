<?php namespace Everyware\Newsml\Parsers;

use Everyware\Newsml\Item;
use Everyware\Newsml\TypeParser;
use SimpleXMLIterator;

/**
 * PdfParser
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml\Parsers
 * @since   Everyware\Newsml\Parsers\PdfParser 1.0.0
 */
class PdfParser implements ItemParser, TypeParser {
    
    const OBJECT_TYPE = 'x-im/pdf';
    
    /**
     * @var LinkParser
     */
    protected $link_parser;
    
    /**
     * ObjectParser constructor.
     *
     * @since 0.1
     *
     * @param ItemParser[]
     */
    public function __construct() {
        $this->link_parser = new LinkParser();
    }
    
    /**
     * @param SimpleXMLIterator $element
     *
     * @since 1.0.0
     * @return array
     */
    public function parse( SimpleXMLIterator $element ) {
        
        $item = new Item( $element );
        $item->fill( $this->getLinkAttributes( $element ) );
        
        return [ $item ];
    }
    
    /**
     * @param SimpleXMLIterator $element
     *
     * @since 1.0.0
     * @return array
     */
    private function getLinkAttributes( SimpleXMLIterator $element ) {
        $links = $this->link_parser->parseLinks( $element );
        if( ! empty( $links ) ) {
            foreach ( $links as $index => $link ) {
                if( $link instanceof Item ) {
                    $attributes = $link->getItemAttributes();
                    if( array_get( $attributes, 'rel', '' ) === 'self' ) {
                        return [
                            'title' => $this->getTitle( $link ),
                            'file'  => $this->getFileName( $link ),
                            'url'   => $this->createUrl($link),
                        ];
                    }
                }
            }
        }
        
        return [];
    }
    
    /**
     * @param Item $item
     *
     * @return mixed|string
     * @since 1.0.0
     */
    private function getFileName( Item $item ) {
        return str_replace( 'im://pdf/', '', $item->getItemAttribute( 'uri', '' ) );
    }
    
    /**
     * @param Item $item
     *
     * @return \SimpleXMLElement|string
     * @since 1.0.0
     */
    private function getTitle( Item $item ) {
        $data = $item->getChild( 'data', null );
        
        return isset( $data->text ) ? (string)$data->text : '';
    }
    
    /**
     * Retrieve the type to pars
     *
     * @since 1.0.0
     * @return string
     */
    public function typeToParse() {
        return static::OBJECT_TYPE;
    }
    
    /**
     * @param Item $item
     *
     * @since 1.0.0
     * @return string
     */
    private function createUrl( Item $item ) {
        if( ! defined( 'CF_PDF' ) ) {
            error_log( 'CF_PDF is not defined and can therefor not serve PDFs' );
            
            return '';
        }
        
        return CF_PDF . '/' . $this->getFileName( $item );
    }
}