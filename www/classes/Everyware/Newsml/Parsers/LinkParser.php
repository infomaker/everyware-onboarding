<?php namespace Everyware\Newsml\Parsers;

use Everyware\Newsml\Item;
use SimpleXMLElement;
use SimpleXMLIterator;

/**
 * LinkParser
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml\Parsers
 * @since   Everyware\Newsml\Parsers\LinkParser 1.0.0
 */
class LinkParser {
    
    /**
     * Contains a group of parsers
     *
     * @var ItemParser[]
     */
    private $parsers;
    
    /**
     * ObjectParser constructor.
     *
     * @since 0.1
     *
     * @param ItemParser[]
     */
    public function __construct( array $parsers = [] ) {
        $this->parsers = $parsers;
    }
    
    /**
     * Retrieve type of an item
     *
     * @param SimpleXMLElement $el
     *
     * @since 1.0.0
     * @return string
     */
    public function getType( SimpleXMLElement $el ) {
        return array_get( array_get( (array)$el->attributes(), '@attributes', [] ), 'type', '' );
    }
    
    /**
     * Retrieve the correct parser for the link type
     *
     * @param SimpleXMLElement $el
     *
     * @since 1.0.0
     * @return ItemParser|null
     */
    public function getLinkParser( SimpleXMLElement $el ) {
        return array_get( $this->parsers, $this->getType( $el ), null );
    }
    
    /**
     * Determine if an element has links
     *
     * @param SimpleXMLElement $el
     *
     * @since 1.0.0
     * @return bool
     */
    public function hasLinks( SimpleXMLElement $el ) {
        return isset( $el->links->link );
    }
    
    /**
     * Determine if an element has links
     *
     * @param SimpleXMLElement $el
     *
     * @since 1.0.0
     * @return bool
     */
    public function getLinks( SimpleXMLElement $el ) {
        return isset( $el->links->link );
    }
    
    /**
     * @param SimpleXMLElement $el
     *
     * @since 1.0.0
     * @return null|SimpleXMLElement
     */
    public function getLinkIterator( SimpleXMLElement $el ) {
        
        if( isset( $el->links->link ) ) {
            return $el->links->link;
        }
        
        return null;
    }
    
    /**
     * @param SimpleXMLIterator $el
     *
     * @since 1.0.0
     * @return Item[]
     */
    public function parseLinks( SimpleXMLIterator $el ) {
        
        $links = [];
        if( isset( $el->links->link ) ) {
            foreach ( $el->links->link as $tag => $item ) {
                $parser  = $this->getLinkParser( $item );
                $links[] = $parser instanceof ItemParser ? $parser->parse( $item ) : new Item( $item );
            }
        }
        
        return array_flatten( $links, 1 );
    }
}