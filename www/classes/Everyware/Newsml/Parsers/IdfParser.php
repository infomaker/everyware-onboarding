<?php namespace Everyware\Newsml\Parsers;

use SimpleXMLIterator;

/**
 * IdfParser
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml\Parsers
 * @since   Everyware\Newsml\Parsers\IdfParser 1.0.0
 */
class IdfParser implements ItemParser {
    
    /**
     * @var array
     */
    protected $parsers;
    
    /**
     * IdfParser constructor.
     *
     * @param ElementParser $element_parser
     * @param ObjectParser  $object_parser
     * @param GroupParser   $group_parser
     *
     * @since 0.1
     */
    public function __construct( ElementParser $element_parser, ObjectParser $object_parser, GroupParser $group_parser ) {
        $this->parsers = [
            ElementParser::TAG => $element_parser,
            ObjectParser::TAG  => $object_parser,
            GroupParser::TAG   => $group_parser
        ];
    }
    
    /**
     * @param SimpleXMLIterator $element
     *
     * @since 1.0.0
     * @return array
     */
    public function parse( SimpleXMLIterator $element ) {
        $items = [];
        
        foreach ( $element as $tag => $item ) {
            $parser = array_get( $this->parsers, $tag, null );
            
            if( $parser instanceof ItemParser ) {
                $items[] = $parser->parse($item);
            }
        }
        
        return array_flatten($items, 1);
    }
}