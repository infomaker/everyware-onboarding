<?php namespace Everyware\Newsml\Parsers;

use Everyware\Newsml\Item;
use SimpleXMLIterator;

/**
 * ElementParser
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml\Parsers
 * @since   Everyware\Newsml\Parsers\ElementParser 1.0.0
 */
class ElementParser implements ItemParser {
    const TAG = 'element';
    
    /**
     * Contains a list of inline elements allowed to pass through the filter
     *
     * @var array
     */
    protected static $allowed_tags = [
        'span',
        'i',
        'b',
        'em',
        'strong',
        'sup',
        'sub',
        'br',
        'hr',
        'a',
        'script',
        'iframe'
    ];
    
    /**
     * @param SimpleXMLIterator $element
     *
     * @since 1.0.0
     * @return array
     */
    public function parse( SimpleXMLIterator $element ) {
        $item = new Item($element);
        $item->fill([
            'type' => $item->getType(),
            'content' => $item->getFilteredContent(static::$allowed_tags)
        ]);
        return [ $item ];
    }
}