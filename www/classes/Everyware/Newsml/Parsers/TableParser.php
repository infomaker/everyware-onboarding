<?php namespace Everyware\Newsml\Parsers;

use Everyware\Newsml\Item;
use Everyware\Newsml\TypeParser;
use SimpleXMLIterator;

/**
 * TableParser
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml\Parsers
 * @since   Everyware\Newsml\Parsers\TableParser 1.0.0
 */
class TableParser extends LinkParser implements ItemParser, TypeParser {
    
    const OBJECT_TYPE = 'x-im/table';
    
    /**
     * @param SimpleXMLIterator $element
     *
     * @since 1.0.0
     * @return array
     */
    public function parse( SimpleXMLIterator $element ) {
        $item = new Item( $element );
        $table = $item->getChild( 'data', null );
        $item->fill( [
            'content' => $table instanceof SimpleXMLIterator ? $table->tbody->asXML() : ''
        ] );
        
        return [ $item ];
    }
    
    /**
     * Retrieve the type to pars
     *
     * @since 1.0.0
     * @return string
     */
    public function typeToParse() {
        return static::OBJECT_TYPE;
    }
}