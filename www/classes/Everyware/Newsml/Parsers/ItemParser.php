<?php namespace Everyware\Newsml\Parsers;

use SimpleXMLIterator;

/**
 * Interface ItemParser
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml\Parsers
 * @since   Everyware\Newsml\Parsers\ItemParser 1.0.0
 */
interface ItemParser {
    
    /**
     * @param SimpleXMLIterator $element
     *
     * @since 1.0.0
     * @return array
     */
    public function parse( SimpleXMLIterator $element );
}