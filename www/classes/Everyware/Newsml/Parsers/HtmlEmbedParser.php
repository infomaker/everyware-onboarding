<?php namespace Everyware\Newsml\Parsers;

use Everyware\Newsml\Item;
use Everyware\Newsml\TypeParser;
use SimpleXMLIterator;

/**
 * HtmlEmbedParser
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml\Parsers
 * @since   Everyware\Newsml\Parsers\HtmlEmbedParser 1.0.0
 */
class HtmlEmbedParser implements ItemParser, TypeParser {
    
    const OBJECT_TYPE = 'x-im/htmlembed';
    
    /**
     * @param SimpleXMLIterator $element
     *
     * @since 1.0.0
     * @return array
     */
    public function parse( SimpleXMLIterator $element ) {
        $item = new Item( $element );
        $item->fill( [
            'type'    => $item->getType(),
            'format'  => isset( $element->data->format ) ? (string)$element->data->format : 'text',
            'content' => isset( $element->data->text ) ? (string)$element->data->text : ''
        ] );
        
        return [ $item ];
    }
    
    /**
     * Retrieve the type to pars
     *
     * @since 1.0.0
     * @return string
     */
    public function typeToParse() {
        return static::OBJECT_TYPE;
    }
}