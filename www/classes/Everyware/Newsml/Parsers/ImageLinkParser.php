<?php namespace Everyware\Newsml\Parsers;

use Everyware\Newsml\Item;
use Everyware\Newsml\Objects\ImageItem;
use SimpleXMLIterator;

/**
 * ImageLinkParser
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml\Parsers
 * @since   Everyware\Newsml\Parsers\ImageLinkParser 1.0.0
 */
class ImageLinkParser extends LinkParser implements ItemParser {
    
    const LINK_TYPE = 'x-im/image';
    
    /**
     * @param SimpleXMLIterator $element
     *
     * @since 1.0.0
     * @return array
     */
    public function parse( SimpleXMLIterator $element ) {
        $item = new ImageItem( $element );
        $item->fill( array_replace( $this->getImageData( $item ), $this->getImageLinks( $element ) ) );
        
        return [ $item ];
    }
    
    /**
     * @param SimpleXMLIterator $element
     *
     * @since 1.0.0
     * @return array
     */
    public function getImageLinks( SimpleXMLIterator $element ) {
        $links = [];
        foreach ( $this->parseLinks( $element ) as $link ) {
            if( $link instanceof Item ) {
                $attributes = $link->getItemAttributes();
                $link_rel   = array_get($attributes, 'rel', 'link' );
                $title = array_get( $attributes, 'title', '' );
                
                if( $link_rel === 'crop' && $title !== '' ) {
                    $links[ $link_rel ][$title] = $this->extractCropValues($attributes);
                } else {
                    $links[ $link_rel ][] = $link->getItemAttributes();
                }
            }
        }
        
        return $links;
    }
    
    /**
     *
     *
     * @param $crop
     *
     * @since 1.0.0
     * @return array
     */
    protected function extractCropValues( $crop ) {
        
        $values = array_map( [
            $this,
            'formatFloatCropNumber'
        ], explode( '/', str_replace( 'im://crop/', '', $crop[ 'uri' ] ) ) );
        
        return [
            'x'  => $values[ 0 ],
            'y'  => $values[ 1 ],
            'width'  => $values[ 2 ],
            'height' => $values[ 3 ]
        ];
    }
    
    /**
     * @param Item $item
     *
     * @since 1.0.0
     * @return array
     */
    protected function getImageData( Item $item ) {
        return array_replace( [
            'type'   => $item->getType(),
            'uuid'   => $item->getItemAttribute( 'uuid' ),
            'width'  => 0,
            'height' => 0
        ], (array)$item->getChild( 'data', [] ) );
    }
    
    /**
     * Format image crop number
     *
     * @param $number
     *
     * @return float
     */
    protected function formatFloatCropNumber( $number ) {
        $number = (float)sprintf( '%.5f', $number + 0 );
        if( strlen( $number ) === 1 ) {
            if( $number == 1 ) {
                $number = 0.99999;
            } else if( $number == 0 ) {
                $number = 0.00001;
            }
        }
        
        return $number;
    }
}