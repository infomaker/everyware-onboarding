<?php namespace Everyware\Newsml;

use Everyware\handler\NewRelicLog;
use Everyware\Newsml\Parsers\ObjectParser;
use Everyware\PropertyObject;
use RuntimeException;
use SimpleXMLIterator;

/**
 * NewsMLObjectTransformer
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml
 * @since   Everyware\Newsml\NewsMLObjectTransformer 1.0.0
 */
class NewsMLObjectTransformer implements Transformer {
    
    const VALID_TAG = 'object';
    
    /**
     * @var ObjectParser
     */
    private $parser;
    
    /**
     * @var string
     */
    private $newsml_property_key;
    
    /**
     * NewsMLTransformer constructor.
     *
     * @param ObjectParser $parser
     * @param string       $newsml_property_key
     *
     * @since 0.1
     */
    public function __construct( ObjectParser $parser, $newsml_property_key ) {
        $this->parser          = $parser;
        $this->newsml_property_key = $newsml_property_key;
    }
    
    /**
     * @param string $newsml
     *
     * @since 1.0.0
     * @return array
     */
    private function parse( $newsml ) {
        try {
            $iterator = new SimpleXMLIterator( $newsml );
            
            if ( $iterator->getName() === static::VALID_TAG ) {
                return $this->parser->parse($iterator);
            }
        } catch( RuntimeException $exception) {
            NewRelicLog::error(sprintf('Failed parsing NewsML from %s', $this->newsml_property_key ), $exception);
        }
        return [];
    }
    
    /**
     * @param PropertyObject $object
     *
     * @since 1.0.0
     * @return array
     */
    public function transform( PropertyObject $object ) {
        $newsml = $object->get( $this->newsml_property_key );
        return $this->parse($newsml);
    }
}