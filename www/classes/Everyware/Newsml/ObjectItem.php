<?php namespace Everyware\Newsml;

/**
 * ObjectItem
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml
 * @since   Everyware\Newsml\ObjectItem 1.0.0
 */
class ObjectItem extends Item {
    
    public function getContent() {
        return $this->removeLinebreak( $this->el->asXML() );
    }
    
    public function getFilteredContent() {
        return $this->filterInlineContent( $this->getContent() );
    }
    
    /**
     * @since 1.0.0
     * @return array
     */
    public function getAllowedTags() {
        return [];
    }
}