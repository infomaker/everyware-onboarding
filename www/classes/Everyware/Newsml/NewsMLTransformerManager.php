<?php namespace Everyware\Newsml;

use Everyware\Newsml\Parsers\ElementParser;
use Everyware\Newsml\Parsers\GroupParser;
use Everyware\Newsml\Parsers\IdfParser;
use Everyware\Newsml\Parsers\ItemParser;
use Everyware\Newsml\Parsers\ObjectParser;

/**
 * NewsMLTransformerManager
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml
 * @since   Everyware\Newsml\NewsMLTransformerManager 1.0.0
 */
class NewsMLTransformerManager {
    
    /**
     * Contains all registered Parsers
     *
     * @var ItemParser[]
     */
    private static $parsers = [];
    
    /**
     * NewsMLTransformerManager Singleton constructor.
     *
     * @since 0.1
     */
    private function __construct() {
    }
    
    /**
     * @param string     $type
     * @param ItemParser $parser
     *
     * @since 1.0.0
     * @return void
     */
    public static function registerObjectParser( $type, ItemParser $parser ) {
        static::$parsers[ $type ] = $parser;
    }
    
    /**
     * @param string $newsml_property_key
     *
     * @since 1.0.0
     * @return NewsMLTransformer
     */
    public static function createTransformer( $newsml_property_key ) {
        $element_parser = new ElementParser();
        $object_parser  = new ObjectParser( static::$parsers );
        $group_parser   = new GroupParser( $element_parser, $object_parser );
        $idf_parser     = new IdfParser( $element_parser, $object_parser, $group_parser );
        
        return new NewsMLTransformer( $idf_parser, $newsml_property_key );
    }
    
    public static function createObjectTransformer( $newsml_property_key ) {
        return new NewsMLObjectTransformer( new ObjectParser( static::$parsers ), $newsml_property_key );
    }
}