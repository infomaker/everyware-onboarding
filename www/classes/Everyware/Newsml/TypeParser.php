<?php namespace Everyware\Newsml;

/**
 * Interface TypeParser
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml
 * @since   Everyware\Newsml\TypeParser 1.0.0
 */
interface TypeParser {
    
    /**
     * Retrieve the type to pars
     *
     * @since 1.0.0
     * @return string
     */
    public function typeToParse();
}