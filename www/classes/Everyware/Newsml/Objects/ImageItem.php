<?php namespace Everyware\Newsml\Objects;

use Everyware\Newsml\Item;

/**
 * ImageItem
 *
 * @property int    height
 * @property array  links
 * @property string uuid
 * @property string type
 * @property int    width
 * @property string  uri
 * @link    http://infomaker.se
 * @package Everyware\Newsml\Objects
 * @since   Everyware\Newsml\Objects\ImageItem 1.0.0
 */
class ImageItem extends Item {
    
    public function getCrop( $ratio ) {
        return array_get($this->getMultiValue('crop'), $ratio, []);
    }
    
    public function hasCrop( $ratio ) {
        return !empty($this->getCrop($ratio));
    }
    
    public function hasOriginalSizes() {
        return isset($this->width, $this->height);
    }
    
    public function fileType() {
        return pathinfo($this->uri, PATHINFO_EXTENSION);
    }
}