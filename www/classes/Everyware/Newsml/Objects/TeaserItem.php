<?php namespace Everyware\Newsml\Objects;

use Everyware\Newsml\Item;

/**
 * TeaserItem
 *
 * @property array links
 * @property string type
 * @property string headline
 * @property string text
 * @property string subject
 * @link    http://infomaker.se
 * @package Everyware\Newsml\Objects
 * @since   Everyware\Newsml\Objects\TeaserItem 1.0.0
 */
class TeaserItem extends Item {
    
    protected $has_image;
    
    /**
     * @since 1.0.0
     * @return ImageItem|null
     */
    public function getImage() {
        foreach ( $this->links as $link ) {
            if ( $link instanceof ImageItem ) {
                return $link;
            }
        }
        return null;
    }
    
    /**
     * @since 1.0.0
     * @return bool
     */
    public function hasImage() {
        return $this->getImage() !== null;
    }
}