<?php namespace Everyware\Newsml;

use Everyware\PropertyObject;
use SimpleXMLIterator;

/**
 * Item
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml
 * @since   Everyware\Newsml\Item 1.0.0
 */
class Item extends PropertyObject {
    
    /**
     * @var SimpleXMLIterator
     */
    protected $el;
    
    /**
     * Item constructor.
     *
     * @param SimpleXMLIterator $el
     *
     * @since 0.1
     */
    public function __construct( SimpleXMLIterator $el ) {
        $this->fill(array_get( (array)$el->attributes(), '@attributes', [] ));
        $this->el              = $el;
    }
    
    /**
     * Filter the content of the blocks from unwanted elements.
     * This function will be applied to every block
     *
     * @param string $text
     * @param array  $allowed_tags
     *
     * @return string
     * @since 0.1
     */
    public function filterInlineContent( $text = '', array $allowed_tags = [] ) {
        if( ! empty( $allowed_tags ) ) {
            return trim( strip_tags( $text, '<' . implode( '><', $allowed_tags ) . '>' ) );
        }
        
        return $text;
    }
    
    /**
     * @since 1.0.0
     * @return array
     */
    public function getItemAttributes() {
        return $this->getProperties();
    }
    
    /**
     * @since 1.0.0
     *
     * @param string $key
     * @param string $default
     *
     * @return mixed
     */
    public function getItemAttribute( $key = '', $default = '' ) {
        $item_attributes = $this->getItemAttributes();
        if( array_key_exists( $key, $item_attributes ) ) {
            return $item_attributes[ $key ];
        }
        
        return $default;
    }
    
    /**
     * @since 1.0.0
     * @return string
     */
    public function getType() {
        return array_get( $this->getItemAttributes(), 'type', '' );
    }
    
    /**
     * @param string $name
     * @param mixed  $default
     *
     * @since 1.0.0
     * @return mixed|SimpleXMLElement
     */
    public function getChild( $name, $default = false ) {
        if( isset( $this->el->$name ) ) {
            return $this->el->$name;
        }
        
        return $default;
    }
    
    /**
     * @param $xml
     *
     * @return string
     * @since 0.1
     */
    public function removeLinebreak( $xml ) {
        return implode( '', array_map( function ( $line ) {
            return trim( $line );
        }, explode( PHP_EOL, $xml ) ) );
    }
    
    /**
     * XML version of content
     *
     * @since 1.0.0
     * @return string
     */
    public function getContent() {
        return $this->removeLinebreak( $this->el->asXML() );
    }
    
    /**
     * Filtered content
     *
     * @param array $allowed_tags
     *
     * @since 1.0.0
     * @return string
     */
    public function getFilteredContent( array $allowed_tags = [] ) {
        return $this->filterInlineContent( $this->getContent(), $allowed_tags );
    }
}
