<?php namespace Everyware\Newsml;

use Everyware\handler\NewRelicLog;
use Everyware\Newsml\Parsers\IdfParser;
use Everyware\PropertyObject;
use RuntimeException;
use SimpleXMLIterator;

/**
 * NewsMLTransformer
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml
 * @since   Everyware\Newsml\NewsMLTransformer 1.0.0
 */
class NewsMLTransformer implements Transformer {
    
    const VALID_TAG = 'idf';
    
    /**
     * @var IdfParser
     */
    private $parser;
    
    /**
     * @var string
     */
    private $newsml_property_key;
    
    /**
     * NewsMLTransformer constructor.
     *
     * @param IdfParser $parser
     * @param string    $newsml_property_key
     *
     * @since 0.1
     */
    public function __construct( IdfParser $parser, $newsml_property_key ) {
        $this->parser          = $parser;
        $this->newsml_property_key = $newsml_property_key;
    }
    
    /**
     * @param string $newsml
     *
     * @since 1.0.0
     * @return array
     */
    private function parse( $newsml ) {
        try {
            $iterator = new SimpleXMLIterator( $newsml );
            
            if ( $iterator->getName() === static::VALID_TAG ) {
                return $this->parser->parse($iterator);
            }
        } catch( RuntimeException $exception) {
            NewRelicLog::error(sprintf('Failed parsing NewsML from %s', $this->newsml_property_key ), $exception);
        }
        return [];
    }
    
    /**
     * @param PropertyObject $object
     *
     * @since 1.0.0
     * @return array
     */
    public function transform( PropertyObject $object ) {
        $newsml = $object->get( $this->newsml_property_key );
        return $this->parse($newsml);
    }
}