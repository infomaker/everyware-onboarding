<?php namespace Everyware\Newsml;

use Everyware\PropertyObject;

/**
 * Interface Transformer
 *
 * @link    http://infomaker.se
 * @package Everyware\Newsml
 * @since   Everyware\Newsml\Transformer 1.0.0
 */
interface Transformer {
    
    /**
     * Transform
     *
     * @param PropertyObject $oc_object
     *
     * @since 1.0.0
     * @return mixed
     */
    public function transform(PropertyObject $oc_object);
}