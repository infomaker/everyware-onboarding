<?php namespace Everyware\Concepts;

use Customer\EwcConcept;

/**
 * Place
 *
 * @link    http://infomaker.se
 * @package Everyware\Concepts
 * @since   Everyware\Concepts\Place 0.1
 */
class Place extends EwcConcept {
    protected static $relation_property = 'Places';
}