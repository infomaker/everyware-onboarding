<?php namespace Everyware\Concepts;

use Customer\EwcConcept;

/**
 * Tag
 *
 * @link    http://infomaker.se
 * @package Everyware\Concepts
 * @since   Everyware\Concepts\Tag 0.1
 */
class Tag extends EwcConcept {
    protected static $relation_property = 'Tags';
}