<?php namespace Everyware\Concepts;

use Customer\EwcConcept;
use EveryBoard_Renderer;
use Everyware\ContentProvider;
use EwConcepts\Models\Page;
use EwTools\Handler\OpenContentQueryBuilder;
use EwTools\Models\Everyboard;
use EwTools\Support\Str;
use EwTools\Twig\View;

/**
 * ConceptPage
 *
 * @link    http://infomaker.se
 * @package Customer\Templates
 * @since   Customer\Templates\ConceptPage 0.1
 */
class ConceptPage {
    
    /**
     * @var string
     */
    protected $template = '@base/pages/automatic-page.twig';
    
    /**
     * @var Everyboard
     */
    protected $board;
    
    /**
     * @var Page
     */
    protected $page;
    
    /**
     * @var string
     */
    protected $page_title;
    
    /**
     * @var ContentProvider
     */
    private $content_provider;
    
    /**
     * ConceptPage constructor.
     *
     * @param Page            $page
     * @param ContentProvider $contentProvider
     *
     * @since 0.1
     */
    public function __construct( Page $page, ContentProvider $contentProvider ) {
        $this->content_provider = $contentProvider;
        $this->page             = $page;
        $this->board            = $this->page->getBoard();
    }
    
    /**
     * Fetches the board json and returns it as raw html.
     *
     * @since 0.1
     *
     * @param array $articles
     *
     * @return string
     */
    protected function getBoardContent( array $articles = [] ) {
        return $this->board->generate($articles);
    }
    
    /**
     * Use Concept Page data to set the title of the page
     *
     * @since 0.1
     *
     * @param string $concept_name
     *
     * @return void
     */
    protected function setPageTitle( $concept_name = null ) {
        $this->page_title = $concept_name ? "{$this->page->title} | {$concept_name}" : $this->page->title;
        add_filter( 'page_title', function () {
            return htmlspecialchars_decode( $this->page_title );
        } );
    }
    
    /**
     * Fetches articles using the injected content provider.
     *
     * @param array $concept_uuids
     *
     * @since 0.1
     * @return array
     */
    protected function getArticles( array $concept_uuids ) {
        $this->content_provider->setPropertyMap( false );
        
        return $this->content_provider->queryWithRequirements( [
            'q'                      => OpenContentQueryBuilder::propertySearch( 'ConceptUuids', $concept_uuids ),
            'contenttypes'           => [ 'Article' ],
            'sort.indexfield'        => 'Pubdate',
            'sort.Pubdate.ascending' => 'false',
            'limit'                  => 50,
        ] );
    }
    
    /**
     * @param array $uuid
     *
     * @since 0.1
     * @return void
     */
    protected function renderArticles( array $uuid = [] ) {
        View::render( $this->template, [
            'content' => $this->getBoardContent( $this->getArticles( $uuid ) )
        ] );
    }
    
    /**
     * @param null $name
     *
     * @since 0.1
     * @return array
     */
    protected function getConcepts( $name = null ) {
        $query     = OpenContentQueryBuilder::where( 'Type', $this->page->types )->andIfProperty( 'Status', 'usable' );
        $whitelist = $this->page->getWhitelist();
        
        if( $name !== null ) {
            $query->andIfProperty( 'Name', $name );
            
        } else if( ! empty( $whitelist ) ) {
            $query->andIfProperty( 'Name', $whitelist );
        }
        
        $this->content_provider->setPropertyMap( 'Concept' );
        
        return $this->content_provider->queryWithRequirements( [
            'q'               => $query->buildQueryString(),
            'contenttypes'    => [ 'Concept' ],
            'sort.indexfield' => 'Name',
            'limit'           => 100
        ] );
    }
    
    /**
     * @param array $concepts
     *
     * @since 0.1
     * @return array
     */
    protected function createConceptTeasers( array $concepts = [] ) {
        
        $concepts = array_map( function ( EwcConcept $concept ) {
            return View::generate( '@base/page/partials/teaser/teaser.twig', [
                'title' => $concept->name,
                'text'  => $concept->description_short,
                'url'   => Page::generatePermalink($this->page->slug, $concept->name, $concept->uuid)
            ] );
        }, $concepts );
        
        return implode( '', $concepts );
    }
    
    /**
     * @param array $concepts
     *
     * @since 0.1
     * @return void
     */
    protected function renderConcepts( array $concepts = [] ) {
        View::render( $this->template, [
            'content' => $this->createConceptTeasers( $concepts )
        ] );
    }
    
    /**
     * @since 0.1
     * @return void
     */
    public function renderCollectionPage() {
        $this->setPageTitle();
        $this->renderConcepts( $this->getConcepts() );
    }
    
    /**
     * @param string     $concept_name
     * @param null|array $uuid
     *
     * @since 0.1
     * @return void
     */
    public function renderConceptPage( $concept_name, $uuid = null ) {
        
        $this->setPageTitle( Str::title( urldecode( $concept_name ) ) );
        
        // Render articles from specified concept
        if( $uuid !== null ) {
            $this->renderArticles( (array)$uuid );
            exit;
        }
        
        $concepts = $this->getConcepts( $concept_name );
        
        // Render Articles if only one concept was found
        if( count( $concepts ) === 1 ) {
            $this->renderArticles( array_map( function ( EwcConcept $concept ) {
                return $concept->uuid;
            }, $concepts ) );
            exit;
        }
        
        $this->renderConcepts( $concepts );
    }
}