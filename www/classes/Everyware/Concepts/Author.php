<?php namespace Everyware\Concepts;

use Customer\EwcArticle;
use Customer\EwcConcept;
use EwTools\Handler\Imengine;
use EwTools\Models\SettingsParameter;
use EwTools\Support\Collection;
use EwTools\Support\Str;
use SimpleXMLElement;

/**
 * Author
 *
 * @property array  content
 * @property string avatar_uuid
 * @link  http://infomaker.se
 * @since Everyware\Concepts\Author 0.1
 */
class Author extends EwcConcept {
    
    protected static $relation_property = 'Authors';
    
    public function getBylineInfo() {
        $contact_info = $this->getContactInfo();
        $contact      = [];
        foreach ( $contact_info as $type => $value ) {
            if( $value === '' ) {
                continue;
            }
            
            $name = $value;
            if( $type === 'phone' ) {
                $value = "tel:{$value}";
            }
            if( $type === 'email' ) {
                $value = "mailto:{$value}";
            }
            
            $contact[] = [ 'name' => $name, 'url' => $value ];
        }
        
        $template_data = [
            'name'              => $this->name,
            'permalink'         => $this->permalink,
            'description'       => $this->description,
            'description_short' => $this->description_short,
            'contact'           => $contact
        ];
        
        if( ! empty( $this->avatar_uuid ) ) {
            $template_data[ 'images' ] = [
                [ 'url' => Imengine::fit( 400 )->quality( 80 )->fromUuid( $this->avatar_uuid ) ]
            ];
        }
        
        return $template_data;
    }
    
    /**
     * @since 0.1
     * @return array
     */
    public function getContactInfo() {
        $meta_data    = $this->meta_data;
        $contact_info = [
            'email' => '',
            'phone' => ''
        ];
        
        if( ! empty( $meta_data ) ) {
            $content = new SimpleXMLElement( $meta_data );
            foreach ( $content->children() as $object ) {
                if( (string)$object[ 'type' ] === 'x-im/contact-info' ) {
                    return array_replace( $contact_info, (array)$object->data );
                }
            }
        }
        
        return $contact_info;
    }
    
    /**
     * Getter for an authors twitter account if set
     *
     * @since 0.1
     * @return string
     */
    public function getTwitterAccount() {
        $twitter_url = $this->getTwitterLinks()->first();
        
        return $twitter_url ? '@' . ltrim( parse_url( $twitter_url )[ 'path' ], '/' ) : '';
    }
    
    /**
     * Create a url to send correcting mails
     *
     * @param EwcArticle $article
     *
     * @since 1.0.0
     * @return string
     */
    public function correctionUrl( EwcArticle $article ) {
        $base_url = SettingsParameter::getValue( 'correct_article_url' ) ?: site_url();
        
        return "{$base_url}?" . http_build_query( [
                'referring-article' => $article->post_id,
                'article-title'     => get_the_title( $article->post_id ),
                'article-author'    => $this->name,
                'article-email'     => $this->content[ 'email' ]
            ] );
    }
    
    /**
     * Retrieve a collection of twitter links
     *
     * @since 1.0.0
     * @return Collection
     */
    public function getTwitterLinks() {
        return collect( $this->related_links )->filter( function ( $link ) {
            return Str::contains( $link, 'twitter.com' );
        } );
    }
    
    /**
     * Retrieve a collection of twitter accounts
     *
     * @param $authors
     *
     * @since 0.1
     * @return Collection
     */
    public static function getTwitterAccounts( $authors ) {
        return collect( $authors )->map( function ( $author ) {
            if( $author instanceof self ) {
                return $author->getTwitterAccount();
            }
            
            return false;
        } )->filter();
    }
}