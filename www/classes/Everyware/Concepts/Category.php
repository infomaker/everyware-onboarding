<?php namespace Everyware\Concepts;

use Customer\EwcConcept;

/**
 * Category
 *
 * @link    http://infomaker.se
 * @package Everyware\Concepts
 * @since   Everyware\Concepts\Category 1.0.0
 */
class Category extends EwcConcept {
    protected static $relation_property = 'Categories';
}