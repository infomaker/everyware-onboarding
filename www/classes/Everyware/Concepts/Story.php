<?php namespace Everyware\Concepts;

use Customer\EwcConcept;

/**
 * Story
 *
 * @link    http://infomaker.se
 * @package Everyware\Concepts
 * @since   Everyware\Concepts\Story 0.1
 */
class Story extends EwcConcept {
    protected static $relation_property = 'Stories';
}