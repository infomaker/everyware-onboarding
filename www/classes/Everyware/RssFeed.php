<?php namespace Everyware;

/**
 * Interface RssFeed
 *
 * @link    http://infomaker.se
 * @package Everyware
 * @since   Everyware\RssFeed 1.0.0
 */
interface RssFeed {
    
    /**
     * @since 1.0.0
     * @return array
     */
    public function getFeed();
}