<?php

namespace Everyware\Helpers;

use EwTools\Support\Arr;

class RequestHelper
{

	/**
	 * Get a segment from the URI (1 based index).
	 *
	 * @param  int $index
	 * @param  string|null $default
	 *
	 * @return string|null
	 */
	public static function segment($index, $default = null)
	{
		return Arr::get(self::segments(), $index - 1, $default);
	}

	/**
	 * Get all of the segments for the request path.
	 *
	 * @return array
	 */
	public static function segments()
	{
		$segments = explode('/', trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/'));

		return array_values(array_filter($segments, function ($v) {
			return $v !== '';
		}));
	}

	/**
	 * Get segments out off string
	 *
	 * @param $url
	 * @return array
	 */
	public static function getSegmentsFromString($url)
	{
		$segments = explode('/', trim(parse_url($url, PHP_URL_PATH), '/'));

		return array_values(array_filter($segments, function ($v) {
			return $v !== '';
		}));
	}

    /**
     * Get specific part of segment from string
     *
     * @param $url
     * @param $index
     *
     * @return mixed
     */
    public static function getSegmentFromString($url, $index)
    {
        return Arr::get(self::getSegmentsFromString($url), $index - 1);
    }
}