<?php

namespace Everyware\Helpers;

class AuthHelper
{
    /**
     * Check if user is logged in
     *
     * @return bool
     */
    public static function isUserLoggedIn()
    {
        return isset($_SERVER['HTTP_X_EW_AUTH']) && ($_SERVER['HTTP_X_EW_AUTH'] === 'true' || $_SERVER['HTTP_X_EW_AUTH'] === 'noaccess');
    }

    /**
     * Get user logged in status
     *
     * @return string
     */
    public static function getUserLoggedInStatus()
    {
        return $_SERVER['HTTP_X_EW_AUTH'] ?? 'closed';
    }

    /**
     * Check logged in user and that it have correct product
     *
     * @return bool
     */
    public static function hasUserAccess()
    {
        return isset($_SERVER['HTTP_X_EW_AUTH']) && $_SERVER['HTTP_X_EW_AUTH'] === 'true';
    }
}