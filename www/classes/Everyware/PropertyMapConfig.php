<?php namespace Everyware;

/**
 * Interface PropertyMapConfig
 *
 * @link    http://infomaker.se
 * @package Everyware
 * @since   Everyware\PropertyMapConfig 1.0.0
 */
interface PropertyMapConfig {
    
    /**
     * Retrieve the map of the properties from the configuration file
     *
     * @since 1.0.0
     * @return array
     */
    public function getPropertyMap();
    
    /**
     * Retrieve reference classes for properties from the configuration file
     *
     * @since 1.0.0
     * @return array
     */
    public function getPropertyClassReference();
    
    /**
     * Retrieve hole or a specific part of the configuration
     *
     * @param string $name
     *
     * @since 1.0.0
     * @return array
     */
    public function getFromConfig( $name = null );
    
    /**
     * Retrieve the absolute path to the config file
     *
     * @since 1.0.0
     * @return string
     */
    public function getConfigFilePath();
}