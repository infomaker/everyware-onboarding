<?php namespace Everyware;

use AbstractOcObject;
use Customer\Config;
use Customer\EwcDate;
use Everyware\handler\NewRelicLog;
use EwTools\Support\Collection;

/**
 * PropertyParser
 *
 * @link    http://infomaker.se
 * @package Everyware
 * @since   Everyware\PropertyParser 1.0.0
 */
class PropertyParser {
    
    /**
     * Contains project config
     *
     * @var Config
     */
    protected $config;
    
    /**
     * @var array
     */
    private static $default_transforms = [
        'toSingleValue'
    ];
    
    /**
     * PropertyParser constructor.
     *
     * @since 0.1
     * *
     *
     * @param PropertyMapConfig $config
     */
    public function __construct( PropertyMapConfig $config ) {
        $this->config = $config;
    }
    
    /**
     * @param string           $reference
     * @param AbstractOcObject $oc_object
     *
     * @return PropertyObject
     * @since 1.0.0
     */
    public function fromOcObject( $reference, AbstractOcObject $oc_object ) {
        
        $properties = $this->extractProperties( $oc_object->get_all_properties(), $this->config->getPropertyMap( $reference ) );
        if( $oc_object instanceof \OcArticle ) {
            $properties = array_replace( $properties, [
                'permalink' => $oc_object->get_permalink(),
                'post_id'   => $oc_object->get_post()->ID
            ] );
        }
        
        return $this->getReferenceClass( $reference, $properties );
    }
    
    /**
     * Parse a collection of objects
     *
     * @param string $reference
     * @param array  $collection
     *
     * @return Collection
     * @since 1.0.0
     */
    public function fromSearch( $reference, array $collection = [] ) {
        return Collection::make( $collection )->map( function ( $object ) use ( $reference ) {
            if( $this->validObject( $object ) ) {
                return $this->fromOcObject( $reference, $object );
            }
            
            return null;
        } )->filter();
    }
    
    /**
     * Determine if an object is valid for parsing
     *
     * @param $object
     *
     * @since 1.0.0
     * @return bool
     */
    public function validObject( $object ) {
        return $object instanceof AbstractOcObject;
    }
    
    /**
     * Retrieve a date version of a value
     *
     * @param $property
     *
     * @since 1.0.0
     * @return EwcDate
     */
    public function toBoolValue( $property ) {
        return filter_var( $this->toSingleValue( $property ), FILTER_VALIDATE_BOOLEAN );
    }
    
    /**
     * Retrieve a date version of a value
     *
     * @param $property
     *
     * @since 1.0.0
     * @return EwcDate
     */
    public function toDateValue( $property ) {
        return EwcDate::createFromOcString( $this->toSingleValue( $property ) );
    }
    
    /**
     * Get property as multi value (array)
     *
     * @param string $property
     *
     * @since 0.1
     * @return array
     */
    public function toMultiValue( $property ) {
        return $property !== null ? (array)$property : [];
    }
    
    /**
     * Get property as a single value
     *
     * @param string $property
     *
     * @since 0.1
     * @return string
     */
    public function toSingleValue( $property ) {
        $value = $this->toMultiValue( $property );
        
        return ! empty( $value ) ? array_shift( $value ) : '';
    }
    
    /**
     * @param string $reference
     * @param array  $properties
     *
     * @return PropertyObject
     * @since 1.0.0
     */
    private function getReferenceClass( $reference, array $properties = [] ) {
        $property_object = new PropertyObject();
        $property_object->fill( $properties );
        
        $class_name = array_get( $this->config->getPropertyClassReference(), $reference, '' );
        
        $class_instance = ! empty( $class_name ) ? new $class_name( $property_object ) : $property_object;
        
        if( ! $class_instance instanceof PropertyObject ) {
            NewRelicLog::error( sprintf( 'The reference class "%s" for propertyObject "%s" should extend "%s" and will therefor not be used.', $class_name, $reference, PropertyObject::class ) );
            
            return $property_object;
        }
        
        return $class_instance;
    }
    
    /**
     * @param array $properties
     * @param array $mapped_properties
     *
     * @return array
     * @since 1.0.0
     */
    private function extractProperties( array $properties = [], array $mapped_properties = [] ) {
        
        $extracted_properties = [];
        foreach ( $mapped_properties as $property_map => $map_info ) {
            $property   = array_get( $properties, strtolower( $map_info[ 'name' ] ), [] );
            $transforms = $this->getTransforms( $map_info );
            if( $this->hasReference( $map_info ) ) {
                $extracted_properties[ $property_map ] = $this->fromSearch( $this->getReference( $map_info ), $property );
                continue;
            }
            foreach ( $transforms as $transform ) {
                $property = $this->$transform( $property );
            }
            
            $extracted_properties[ $property_map ] = $this->hasReference( $map_info ) ? $this->fromSearch( $this->getReference( $map_info ), $property ) : $property;
        }

        return $extracted_properties;
    }
    
    private function hasReference( $property_map ) {
        return isset( $property_map[ 'propertyMapReference' ] );
    }
    
    private function getReference( $property_map ) {
        return $property_map[ 'propertyMapReference' ];
    }
    
    /**
     * @param $property_map
     *
     * @since 1.0.0
     * @return array
     */
    private function getTransforms( array $property_map = [] ) {
        return array_get( $property_map, 'transforms', static::$default_transforms );
    }
}