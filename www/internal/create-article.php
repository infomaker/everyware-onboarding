<?php
/**
 * This file will be called from command line through a LUA-script and it will create an oc article in WP
 * from the given uuid. It will print the articles url to be used by the script for redirecting.
 */

if( isset( $_GET[ 'uuid' ] ) ) {
    if( isset( $_GET[ 'host' ] ) ) {
        $_SERVER[ 'HTTP_HOST' ] = $_GET[ 'host' ];
    }
    require __DIR__ . '/wp-bootstrap.php';
    header( 'HTTP/1.0 200 OK' );
    
    $uuid    = wp_kses( array_get($_GET, 'uuid', ''), [] );
    $oc_api = new OcAPI();
    $article = $oc_api->get_single_article( $uuid );
    
    if ( $article instanceof OcArticle) {
        print( $article->get_permalink() );
    }
    die();
}
print( '' );