<?php
/** Amazon Access Id/Keys */
define('AWS_ACCESS_KEY_ID', '');
define('AWS_SECRET_ACCESS_KEY', '');

/** The name of the database for WordPress */
define('DB_NAME', 'everyware');
/** MySQL database username */
define('DB_USER', 'admin');
/** MySQL database password */
define('DB_PASSWORD', 'EveryWare');
/** MySQL hostname */
define('DB_HOST', 'mysql');
/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');
/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
/**  For developers: WordPress debugging mode. */
define('WP_DEBUG', true);
define('PHP_BIN', '/usr/bin/php');
//
// Redis configuration
// https://wordpress.org/plugins/redis-cache/other_notes/
define('WP_REDIS_HOST', 'redis');
define('WP_REDIS_PORT', '6379');
define('WP_REDIS_CLIENT', 'pecl');
define('WP_REDIS_MAXTTL', '1209600'); // 14 days max TTL
define('WP_CACHE_KEY_SALT', $_SERVER['IMHOST']);
/** Set domain */
define('DOMAIN_CURRENT_SITE', 'ew.onboarding.localhost');

define('CF_IMENGINE', 'https://imengine.tryout.infomaker.io');
define('CF_STATIC', getenv('CF_STATIC'));

define('GIT_COMMIT', '1337');

/**  Set url to https if forwarded from loadbalancer and protocol is https */
//if ( $_SERVER['HTTP_X_FORWARDED_PROTO'] == "https" ) {
//	$_SERVER['HTTPS'] = 'on';
//}
