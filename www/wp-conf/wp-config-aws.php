<?php
/** Amazon Access Id/Keys */
define('AWS_ACCESS_KEY_ID', getenv('AWS_ACCESS_KEY_ID'));
define('AWS_SECRET_ACCESS_KEY', getenv('AWS_SECRET_KEY'));
define('S3BUCKET_IMENGINE', getenv('S3BUCKET_IMENGINE'));
/** CloudFront server */
define('CF_IMENGINE', getenv('CF_IMENGINE'));
define('CF_STATIC', getenv('CF_STATIC'));
/** The name of the database for WordPress */
define('DB_NAME', getenv('DB_NAME' ));
/** MySQL database username */
define('DB_USER', getenv('DB_USER' ));
/** MySQL database password */
define('DB_PASSWORD', getenv('DB_PASSWORD' ));
/** MySQL hostname */
define('DB_HOST', getenv('DB_HOST'));
/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');
/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
/**  For developers: WordPress debugging mode. */
define('WP_DEBUG', false);
define('PHP_BIN', '/usr/bin/php');
/**
 * Redis configuration.
 * REDIS_OB_HOST = Wordpress Object Cache
 * REDIS_FC_HOST = Nginx Front Cache
 * https://wordpress.org/plugins/redis-cache
 */
define('WP_REDIS_HOST', getenv('REDIS_OB_HOST'));
define('WP_REDIS_PORT', '6379');
define('WP_REDIS_CLIENT', 'pecl');
define('WP_REDIS_MAXTTL', '604800'); // 7 days max TTL
define('WP_CACHE_KEY_SALT', $_SERVER['IMHOST']);
/**
 * WP-SES definitions.
 * Variables are fetched from Beanstalk configuration.
 */
define('WP_SES_ACCESS_KEY', getenv('AWS_ACCESS_KEY_ID'));
define('WP_SES_SECRET_KEY', getenv('AWS_SECRET_ACCESS_KEY'));
define('WP_SES_FROM', getenv('AWS_SES_FROM'));
define('WP_SES_RETURNPATH', getenv('AWS_SES_RETURNPATH'));
define('WP_SES_ENDPOINT', getenv('AWS_SES_ENDPOINT'));
/**  Hardcoded config. */
define('WP_SES_REPLYTO','headers');  // Enable these settings in production
define('WP_SES_HIDE_VERIFIED',true);
define('WP_SES_HIDE_STATS',false);
define('WP_SES_AUTOACTIVATE',true);



/**  Set url to https if forwarded from loadbalancer and protocol is https */
if ( $_SERVER['HTTP_X_FORWARDED_PROTO'] == "https" ) {
	$_SERVER['HTTPS'] = 'on';
}

/* Set correct domain according to ENVIRONMENT var. */
	define('DOMAIN_CURRENT_SITE', 'ew.tryout.infomaker.io');
	define('BLOG_ID_CURRENT_SITE', 2);
