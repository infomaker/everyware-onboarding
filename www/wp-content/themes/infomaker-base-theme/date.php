<?php

use Customer\EwcArticle;
use Customer\EwcDate;
use Customer\TeaserArticle;
use Everyware\OpenContentProvider;
use EwTools\Handler\OpenContentQueryBuilder;
use EwTools\Twig\View;

$date_params = array_filter( [
    'year'  => get_query_var( 'year', null ),
    'month' => get_query_var( 'monthnum', null ),
    'day'   => get_query_var( 'day', null )
] );

$start_date = EwcDate::create( ...array_values( $date_params ) );
$limit      = 30;

if( ! $start_date->isFuture() ) {
    $end_date = $start_date->isToday() ? EwcDate::now() : $start_date->copy()->endOfDay();
    
    if( isset( $date_params[ 'day' ] ) ) {
        $start_date->startOfDay();
    } else if( isset( $date_params[ 'month' ] ) ) {
        $start_date->startOfMonth();
        
        if( ! $start_date->isCurrentMonth() ) {
            $end_date->endOfMonth();
        }
    } else {
        $start_date->startOfYear();
        if( ! $start_date->isCurrentYear() ) {
            $end_date->endOfYear();
        }
    }
    
    $span = OpenContentQueryBuilder::dateSpanSearch( 'Pubdate', $start_date->toOcString(), $end_date->toOcString() );
    
    $provider = OpenContentProvider::setup( [
        'q'                      => OpenContentQueryBuilder::query( $span )->buildQueryString(),
        'contenttypes'           => [ 'Article' ],
        'sort.indexfield'        => 'Pubdate',
        'sort.Pubdate.ascending' => 'false',
        'limit'                  => 30 * ( 1 + $start_date->diffInMonths( $end_date ) )
    ] )->setPropertyMap( 'Article' );
    
    $articles = array_map( function ( EwcArticle $article ) {
        $teaser = new TeaserArticle( $article );
        
        return View::generate( '@base/page/partials/teaser/teaser.twig', $teaser->getViewData() );
    }, $provider->queryWithRequirements() );
    
    View::render( '@base/pages/archive', [ 'articles' => $articles ] );
}
