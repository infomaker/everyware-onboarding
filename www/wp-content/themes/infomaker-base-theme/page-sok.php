<?php

use Customer\EwcArticle;
use Customer\TeaserArticle;
use Everyware\OpenContentProvider;
use EwTools\Handler\OpenContentQueryBuilder;
use EwTools\Twig\View;

$sortings = [
    'desc' => 'desc',
    'asc' => 'asc'
];
$query = array_get($_GET, 'q', '');
$sort = array_get($_GET, 'sort', $sortings['desc']);
$nextSort = $sort === $sortings['desc'] ? $sortings['asc'] : $sortings['desc'];


$provider = OpenContentProvider::setup( [
    'q'                      => OpenContentQueryBuilder::where('Status', 'usable')->setText(urldecode($query))->buildQueryString(),
    'contenttypes'           => [ 'Article' ],
    'sort.indexfield'        => 'Pubdate',
    'sort.Pubdate.ascending' => $sort !== $sortings['desc'],
    'limit'                  => 30,
    'start'                  => 0
] )->setPropertyMap( 'Article' );

$articlesData = array_map( function ( EwcArticle $article ) {
    $teaser = new TeaserArticle( $article );
    return $teaser->getViewData();
}, $provider->queryWithRequirements() );

$articles = null;

if(count($articlesData) > 0) {
    $articles = View::generate( '@base/page/partials/articles-list/articles-list.twig', [
        'articles' => $articlesData
    ]);
}

View::render( '@base/pages/search', [
    'articles' => $articles,
    'query' => $query,
    'nextSort' => $nextSort,
] );
