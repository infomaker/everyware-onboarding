<?php

use Customer\EwcPage;
use EwTools\Metaboxes\EwPageMeta;
use EwTools\Models\Page;
use EwTools\Twig\View;
use Everyware\OpenContentProvider;

$ewc_page = new EwcPage( Page::current() );


$provider = OpenContentProvider::setup( [
    'contenttypes'           => [ 'Article' ],
    'sort.indexfield'        => 'Pubdate',
    'sort.Pubdate.ascending' => 'false',
    'limit'                  => 30,
    'q' => EwPageMeta::getMetaData(Page::current(), 'query')
] );

$articles = $provider->queryWithRequirements();
if( is_array( $articles ) ) {
    add_filter( 'ew_content_container_fill', function ( $arr ) use ( $articles ) {
        $arr = array_merge( $arr, $articles );

        return $arr;
    } );
}

View::render('@base/main', ['page' => $ewc_page ]);
