<?php

use Everyware\Concepts\ConceptPage;
use Everyware\OpenContentProvider;
use EwConcepts\Models\Page;
use EwConcepts\Router;

Router::register( '{concept}/', function ( $page_slug ) {
    $pages = Page::whereIn( 'slug', $page_slug );
    
    if( ! $pages->isEmpty() ) {
        $concept_page = new ConceptPage( $pages->first(), OpenContentProvider::setup( [] ) );
        $concept_page->renderCollectionPage();
    }
} );

Router::register( '{concept}/{name}', function ( $page_slug, $name ) {
    $pages = Page::whereIn( 'slug', $page_slug );
    
    if( ! $pages->isEmpty() ) {
        $concept_page = new ConceptPage( $pages->first(), OpenContentProvider::setup( [] ) );
        $concept_page->renderConceptPage( $name );
    }
} );

Router::register( '{concept}/{name}/{uuid}', function ( $page_slug, $name, $uuid = null ) {
    $pages = Page::whereIn( 'slug', $page_slug );
    
    if( ! $pages->isEmpty() ) {
        $concept_page = new ConceptPage( $pages->first(), OpenContentProvider::setup( [] ) );
        $concept_page->renderConceptPage( $name, $uuid );
    }
} );

Router::route();