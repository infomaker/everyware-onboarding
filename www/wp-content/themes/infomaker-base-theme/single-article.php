<?php

global $post;

use Customer\SingleArticle;
use EwTools\Twig\View;

$article = SingleArticle::createFromPost($post);

if ( $article instanceof SingleArticle) {
    View::render( '@base/page/single-article', $article->getViewData() );
}
