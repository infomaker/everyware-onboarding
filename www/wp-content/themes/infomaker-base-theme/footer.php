<?php

use Customer\EwcFooter;
use Everyware\handler\ViewHandler;

ViewHandler::renderPagePart('footer', new EwcFooter());