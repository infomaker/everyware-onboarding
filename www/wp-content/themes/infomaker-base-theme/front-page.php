<?php

use EwTools\Models\Page;
use EwTools\Metaboxes\EwPageMeta;
use Everyware\OpenContentProvider;
use EwTools\Twig\View;

$provider = OpenContentProvider::setup( [
    'contenttypes'           => [ 'Article' ],
    'sort.indexfield'        => 'Pubdate',
    'sort.Pubdate.ascending' => 'false',
    'limit'                  => 30,
    'q' => EwPageMeta::getMetaData(Page::current(), 'query')
] );

$articles = $provider->queryWithRequirements();

if( is_array( $articles ) ) {
    add_filter( 'ew_content_container_fill', function ( $arr ) use ( $articles ) {
        $arr = array_merge( $arr, $articles );
        
        return $arr;
    } );
}

View::render('@base/main');
