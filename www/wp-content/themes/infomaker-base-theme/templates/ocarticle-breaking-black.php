<?php
/**
 * Article Name: Breaking-black
 */

use Customer\TeaserArticle;
use EwTools\Twig\View;

$teaser = TeaserArticle::createFromOcArticle( $article );
$teaser->appendClass('teaser--breaking')
       ->appendClass('teaser--breaking-black');

View::render( '@base//teaser/breaking-black.twig', $teaser->getViewData() );