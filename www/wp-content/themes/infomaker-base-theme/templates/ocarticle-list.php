<?php
/**
 * Article Name: Right now
 */

use Customer\TeaserArticle;
use EwTools\Twig\View;

$teaser = TeaserArticle::createFromOcArticle( $article );

View::render( '@base//teaser/list.twig', $teaser->getViewData() );