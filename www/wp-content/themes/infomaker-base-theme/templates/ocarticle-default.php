<?php
/**
 * Article Name: Standard
 */

use Customer\TeaserArticle;
use EwTools\Twig\View;

$teaser = TeaserArticle::createFromOcArticle( $article );

View::render( '@base/teaser/default.twig', $teaser->getViewData() );