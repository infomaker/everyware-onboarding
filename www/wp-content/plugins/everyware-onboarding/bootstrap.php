<?php
/**
 * Plugin Name: Everyware Onboarding
 * Description: Plugin which contains widgets and plugins for the sites
 * Version: 1.0
 * Author: Infomaker Scandinavia AB
 */

use Customer\Config;
use Customer\Startup;

// Check so we don't give direct access to file.
if( ! defined( 'WPINC' ) ) {
    die;
}

Startup::run( __DIR__ );
$plugin = CustomerPlugin::bootstrap( new Config() );