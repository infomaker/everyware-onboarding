<?php namespace EwConcepts;

use EwTools\Admin\Message;
use EwTools\Admin\ProjectPluginPage\TabsPage;
use EwTools\Interfaces\SettingsPageInterface;
use EwTools\Support\Str;

/**
 * AdminPage
 *
 * @link    http://infomaker.se
 * @package AdTech
 * @since   AdTech\AdminPage 0.1
 */
class AdminPage extends TabsPage {
    
    /**
     * @var string
     */
    protected static $title = 'Concepts';
    
    /**
     * @var string
     */
    protected static $text_domain = 'concepts_textdomain';
    
    public function __construct() {
        $this->setDescription( 'Manage Concepts on your site.' );
        add_action( 'admin_enqueue_scripts', [ $this, 'enqueueScripts' ] );
    }
    
    public function onSettingsPageLoad() {
        add_action( 'admin_enqueue_scripts', [ $this, 'enqueuePageScripts' ] );
        parent::onSettingsPageLoad();
    }
    
    public function enqueueScripts() {
        $slug = $this->getSlug();
        wp_enqueue_style( "{$slug}-style", $this->getAsset( 'css/admin.css' ) );
        wp_enqueue_script( "{$slug}-js", $this->getAsset( 'js/admin.js' ), [ 'jquery' ], false, true );
    }
    
    public function enqueuePageScripts() {
    }
    
    /**
     * Retrieve asset
     *
     * @param $path
     *
     * @since 0.1
     * @return string
     */
    private function getAsset( $path ) {
        static $asset_path;
        
        if( $asset_path === null ) {
            $asset_path = Str::append( 'assets', plugin_dir_url( __FILE__ ) );
        }
        
        return Str::finish( $asset_path, "/{$path}" );
    }
    
    /**
     * Retrieve the slug to be use to identify the page
     *
     * @since 0.1
     * @return mixed
     */
    public function getSlug() {
        return Str::slug( static::$title );
    }
    
    /**
     * Retrieve the title of the page to be used in the menu
     *
     * @since 0.1
     * @return string
     */
    public function getTitle() {
        return static::$title;
    }
    
    /**
     * @param array $tabs
     *
     * @since 0.1
     * @return AdminPage
     */
    public static function setup( array $tabs = [] ) {
        $admin_page = new static();
        
        foreach ( $tabs as $tab ) {
            if( $tab instanceof SettingsPageInterface ) {
                $admin_page->addSettingsTab( $tab );
            }
        }
        
        add_action( 'admin_notices', function () {
            if( empty( locate_template( 'concepts.php' ) ) ) {
                Message::create( Message::WARNING, sprintf( '<strong>Notice:</strong> Template <strong>concepts.php</strong> missing in theme: <strong>%s</strong>. Create it to handle concept requests.', wp_get_theme() ), true );
            }
        } );
        
        return $admin_page;
        
    }
}