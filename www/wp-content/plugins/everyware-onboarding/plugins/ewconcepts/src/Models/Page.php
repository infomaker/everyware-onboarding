<?php namespace EwConcepts\Models;

use EwTools\Models\Everyboard;
use EwTools\Storage\CollectionModel;
use EwTools\Support\Str;

/**
 * Page
 *
 * @property int    board
 * @property string slug
 * @property string title
 * @property string main_type
 * @property array  types
 * @property string whitelist
 * @link  http://infomaker.se
 * @since EwConcepts\Models\Page 0.1
 */
class Page extends CollectionModel {
    
    protected $option_name = 'ew_concept_pages';
    
    protected $fields = [
        'slug'      => '',
        'title'     => '',
        'board'     => 0,
        'types'     => [],
        'whitelist' => '',
        'main_type' => ''
    ];
    
    protected function onSet( $fields = [] ) {
        
        $main_type = $fields[ 'main_type' ];
        
        // If a Page is created as a Concept page
        if( Str::notEmpty( $main_type ) ) {
            $defaults            = $this->fields;
            $defaults[ 'title' ] = Str::title( $main_type );
            $defaults[ 'types' ] = [ $main_type ];
            $fields              = array_replace_recursive( $defaults, array_filter( $fields ) );
        }
        
        if( Str::notEmpty( $fields[ 'title' ] ) ) {
            $fields[ 'slug' ] = Str::slug( $fields[ 'title' ] );
        }
        
        return $fields;
    }
    
    /**
     * Helper function to retrieve the selected board
     *
     * @since 0.1
     * @return Everyboard
     */
    public function getBoard() {
        return Everyboard::createFromId( $this->board );
    }
    
    /**
     * Retrieve the list of names that are allowed to be shown on the page
     *
     * @since 1.0.0
     * @return array
     */
    public function getWhitelist() {
        return array_filter( (array)preg_split( '/\s*(,|\|)\s*/', $this->whitelist ) );
    }
    
    /**
     * Determine if the name is whitelisted to the page
     *
     * @param $name
     *
     * @since 1.0.0
     * @return bool
     */
    public function whitelisted( $name ) {
        $whitelist = $this->getWhitelist();
        
        if ( empty($whitelist) ) {
            return true;
        }
        return in_array( strtolower($name), array_map('strtolower', $whitelist), true);
    }
    
    /**
     * @param string $page
     *
     * @since 1.0.0
     * @return bool
     */
    public static function hasPage( $page = '' ) {
        return static::all()->map( function ( self $page ) {
            return $page->slug;
        } )->contains( $page );
    }
    
    /**
     * @param $slug
     *
     * @since 0.1
     * @return array
     */
    public static function getTypesBySlug( $slug ) {
        $page = static::whereIn( 'slug', $slug )->first();
        
        if( $page instanceof self ) {
            return $page->types;
        }
        
        return [];
    }
    
    /**
     * @param string      $type
     * @param string|null $name
     * @param string|null $uuid
     *
     * @return string
     * @since 0.1
     */
    public static function generatePermalink( $type, $name = null, $uuid = null ) {
        $pages = static::whereIn( 'main_type', $type );
        
        $slug = $pages->isNotEmpty() ? $pages->first()->slug : Str::slug( $type );
        
        return implode( '/', array_filter( [
            site_url(),
            $slug,
            strtolower( urlencode( $name ) ),
            $uuid
        ] ) );
    }
    
    /**
     * @param array $concepts
     *
     * @since 1.0.0
     * @return \EwTools\Storage\CollectionDB
     */
    public static function setupOriginals( array $concepts = [] ) {
        $pages   = static::collection();
        $updated = false;
        foreach ( $concepts as $concept ) {
            if( $pages->all()->whereIn( 'main_type', $concept )->isEmpty() ) {
                $updated = true;
                $pages->add( new static( [
                    'main_type' => $concept
                ] ) );
            }
        }
        if( $updated ) {
            $pages->save();
        }
        
        return $pages;
    }
}