<?php namespace EwConcepts;

use EwTools\Support\Str;

/**
 * Router
 *
 * @link    http://infomaker.se
 * @package EwConcepts
 * @since   EwConcepts\Router 0.1
 */
class Router {
    
    /**
     * Contains the registered types of feed templates
     *
     * @var array
     */
    protected static $registered_routes = [];
    
    /**
     * Singleton container
     *
     * @var Router
     */
    private static $instance;
    
    /**
     * Contains the requested route
     *
     * @var string
     */
    protected $route;
    
    /**
     * Constructor
     */
    private function __construct() {
    }
    
    /**
     * Add a route to match with
     *
     * @param $route
     * @param $callback
     *
     * @since 0.1
     * @return Router
     */
    public function addRoute( $route, $callback ) {
        static::$registered_routes[ '/' . trim( $route, '/' ) ] = $callback;
        
        return $this;
    }
    
    /**
     * Match route with registered patterns
     *
     * @param string $route
     *
     * @since 0.1
     * @return mixed
     */
    public function getMatchedPattern( $route ) {
        
        // Check if no exact match exist
        if( ! array_key_exists( $route, static::$registered_routes ) ) {
            
            foreach ( array_keys( static::$registered_routes ) as $registered_route ) {
                if( $this->matchesRoutePattern( $route, $registered_route ) ) {
                    return $registered_route;
                }
                
                if( Str::is( $registered_route, $route ) ) {
                    return $registered_route;
                }
            }
        }
        
        return $route;
    }
    
    /**
     * Check for match among routs with optional parameters or
     * check for match with wildcard route
     *
     * @param string $pattern
     * @param string $route
     *
     * @since 0.1
     * @return bool
     */
    protected function matchesRoutePattern( $route, $pattern ) {
        return (bool)preg_match( $this->createRegExpPattern( $pattern ), $route ) || Str::is( $pattern, $route );
    }
    
    /**
     * Create a regular expression to match routes with optional parameters
     *
     * @param string $route_pattern
     *
     * @since 0.1
     * @return string
     */
    protected function createRegExpPattern( $route_pattern ) {
        return '/^' . str_replace( '/', '\/', preg_replace( '/\{([\w-]+?)\}/', '([\w-]+?)', $route_pattern ) ) . '$/';
    }
    
    /**
     * Get the values of the optional parameters from the route.
     *
     * @param $route
     * @param $pattern
     *
     * @since 0.1
     * @return array
     */
    protected function extractOptionalParameters( $route, $pattern ) {
        if( $pattern === '/*' ) {
            return explode( '/', trim( $route, '/' ) );
        }
        
        preg_match_all( $this->createRegExpPattern( $pattern ), $route, $matches );
        
        return isset( $matches[ 1 ] ) ? array_flatten( array_slice( $matches, 1 ) ) : [];
    }
    
    /**
     * Route the request by finding a match from the list of registered route-patterns
     * and sending the optional parameters
     *
     * @param string $route
     *
     * @since 0.1
     * @return mixed the function result, or false on error.
     */
    public function routeRequest( $route ) {
        $route_parts   = explode( '/', trim( $route, '/' ) );
        
        // Decode and slugify every parameter before proceeding
        $decoded_route = '/' . implode( '/', array_map( function ( $part ) {
                return Str::slug( urldecode( $part ) );
            }, $route_parts ) );
        
        $matched_pattern = $this->getMatchedPattern( $decoded_route );
        $params          = $this->extractOptionalParameters( $decoded_route, $matched_pattern );
        
        // Filter original route_parts to only keep the requested params
        $filtered_params = array_filter($route_parts, function ( $part ) use ($params) {
            return in_array( Str::slug( urldecode( $part )), $params, true );
        });
        
        return $this->callRoute( $matched_pattern, $filtered_params );
    }
    
    /**
     * Call registered function with matching parameters
     *
     * @param string $route
     * @param array  $params
     *
     * @since 0.1
     * @return mixed the function result, or false on error.
     */
    protected function callRoute( $route, array $params = [] ) {
        if( array_key_exists( $route, static::$registered_routes ) ) {
            return call_user_func_array( static::$registered_routes[ $route ], $params );
        }
        
        return false;
    }
    
    /**
     * Retrieve instance of Singleton
     *
     * @since 0.1
     * @return Router
     */
    public static function getInstance() {
        if( static::$instance === null ) {
            static::$instance = new static;
        }
        
        return static::$instance;
    }
    
    /**
     * Helper-function to add a route to match for
     *
     * @param string   $route
     * @param callable $callback
     *
     * @since 0.1
     * @return void
     */
    public static function register( $route, callable $callback ) {
        static::getInstance()->addRoute( $route, $callback );
    }
    
    public static function route() {
        return static::getInstance()->routeRequest( get_query_var( 'concept_route' ) );
    }
}