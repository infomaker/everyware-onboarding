<?php namespace EwConcepts\Tabs;

use EwConcepts\Models\Page;
use EwTools\Admin\Message;
use EwTools\Models\Everyboard;
use EwTools\Storage\Cache;
use EwTools\Support\Str;
use EwTools\Twig\View;
use OcAPI;

/**
 * Pages
 *
 * @link    http://infomaker.se
 * @package ConceptPage\Tabs
 * @since   ConceptPage\Tabs\Pages 0.1
 */
class Pages extends Tab {
    
    protected static $title = 'Pages';
    
    /**
     * Twig-template to use
     *
     * @var string
     */
    protected static $template = '@plugins/ewconcepts/src/templates/admin/pages';
    
    protected $text_domain = 'ew_textdomain';
    
    /**
     * Contains the collection to work with
     *
     * @var \EwTools\Storage\CollectionDB
     */
    protected $pages;
    
    /**
     * @var OcAPI
     */
    private $oc_api;
    
    /**
     * Pages constructor.
     *
     * @param OcAPI $oc_api
     *
     * @since 0.1
     */
    public function __construct( OcAPI $oc_api ) {
        $this->oc_api = $oc_api;
        $this->pages  = $this->getPages();
    }
    
    /**
     * Return current Page-data in the form of a model
     *
     * @since 0.1
     * @return Page
     */
    protected function currentPage() {
        return $this->isEdit() ? $this->pages->all()->get( $this->currentId() ) : new Page( array_get( $_POST, 'page', [] ) );
    }
    
    /**
     * @param Page $page
     *
     * @since    0.1
     * @return bool
     */
    public function addPage( Page $page ) {
        $page_title = $page->title;
        
        // If no Page has been sent
        if( empty( $page_title ) ) {
            Message::create( 'error', "Title can't be empty" );
            
            return false;
        }
        
        // If keyword already exists
        if( $this->pages->all()->whereIn( 'title', $page_title )->isNotEmpty() ) {
            Message::create( 'error', "Couldn't add Page: {$page_title} since it already exists" );
            
            return false;
        }
        
        // If for some reason Keyword couldn't be saved
        if( ! $this->pages->add( $page )->save() ) {
            Message::create( 'Error', "Ops! Something went wrong and Page: {$page_title} could not be saved" );
            
            return false;
        }
        
        Message::create( 'Success', "Page: {$page_title} added!" );
        
        return true;
    }
    
    /**
     * Fires on Page-load if active
     *
     * @since 0.1
     * @return string
     */
    public function currentAction() {
        return array_get( $_POST, 'action_type', '' );
    }
    
    /**
     * Fires on Page-load if active
     *
     * @since 0.1
     * @return void
     */
    public function onPageLoad() {
        if( ! $this->routeAction( $this->currentAction() ) ) {
            $this->has_error = true;
        }
    }
    
    protected function routeAction( $action ) {
        switch ( $action ) {
            case 'create':
                return $this->addPage( $this->currentPage() );
            case 'remove':
                return $this->removePage( $this->currentId() );
            case 'update':
                return $this->updatePage( $this->currentId(), $this->currentPage() );
        }
        
        return false;
    }
    
    /**
     * @param int $id
     *
     * @since 0.1
     * @return bool
     */
    public function removePage( $id ) {
        
        if( ! $this->pages->all()->has( $id ) ) {
            return false;
        }
        
        $page = $this->pages->all()->get($id);
        
        if( ! $this->pages->remove( $id )->save() ) {
            Message::create( 'Error', "Ops! Something went wrong and Page: {$page->title} could not be removed" );
            
            return false;
        }
        
        Message::create( 'Success', "Page: {$page->title} has been removed" );
        
        return false;
    }
    
    /**
     * @param int  $id
     * @param Page $page
     *
     * @return bool
     * @since 0.1
     */
    public function updatePage( $id, Page $page ) {
        if( Str::notEmpty( $page->main_type ) && ! in_array( $page->main_type, $page->types, true ) ) {
            Message::create( 'Error', "Ops! Something when updating Page: {$page->title}. This page is bound to the concept {$page->main_type} and must therefor include this type to be saved" );
            
            return false;
        }
        
        if( ! $this->pages->update( $id, $page )->save() ) {
            Message::create( 'Error', "Ops! Something went wrong and Page: {$page->title} could not be saved" );
            
            return false;
        }
        Message::create( 'Success', "Page: {$page->title} has been updated!" );
        
        return true;
    }
    
    /**
     * @since 0.1
     * @return bool
     */
    protected function isEdit() {
        return array_get( $_POST, 'action_type', '' ) === 'edit';
    }
    
    /**
     * Retrieve the id of the tag to update or remove
     *
     * @since 0.1
     * @return mixed
     */
    protected function currentId() {
        return (int)array_get( $_POST, 'id' );
    }
    
    /**
     * Fires on Page-load if active
     *
     * @since 0.1
     * @return string
     */
    public function pageContent() {
        $page = $this->onError() ? $this->currentPage() : new Page();
        
        return View::generate( static::$template, [
            'form_action'   => $_SERVER[ 'REQUEST_URI' ],
            'text_domain'   => $this->text_domain,
            'submit_action' => 'everyware-concepts-page-submit',
            'pages'         => $this->pages->all()->sort(),
            'edit'          => $this->isEdit(),
            'id'            => $this->currentId(),
            'page'          => $page,
            'boards'        => Everyboard::toSelect( (int)$page->board ),
            'concepts'      => collect( $this->getConceptTypes() )->sort(),
        ] );
    }
    
    /**
     * Fetch available types of Concepts from Open Content
     *
     * @since 0.1
     * @return array
     */
    protected function getConceptTypes() {
        $cache_key = 'ew_concept_types';
        
        if( ( $concept_types = Cache::get( $cache_key ) ) !== false ) {
            return $concept_types;
        }
        
        $this->oc_api->prepare_oc_suggest_query( 'Type', 'contenttype:Concept' );
        
        $concept_types = array_map( function ( $concept ) {
            return $concept->name;
        }, $this->oc_api->get_oc_suggest() );
        
        Cache::set( $cache_key, $concept_types, OcAPI::TRANSIENT_EXPIRE_TIME );
        
        return $concept_types;
    }
    
    /**
     * Setup the pages
     *
     * @since 0.1
     * @return \EwTools\Storage\CollectionDB
     */
    private function getPages() {
        
        $pages = Page::setupOriginals( $this->getConceptTypes() );
        
        // Recreate pages to set default values
        $pages->setCollection($pages->all()->map(function ($page) {
            return new Page($page);
        }));
        
        return $pages;
    }
}