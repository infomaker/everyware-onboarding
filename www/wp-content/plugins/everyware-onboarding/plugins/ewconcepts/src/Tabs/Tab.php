<?php namespace EwConcepts\Tabs;

use EwTools\Interfaces\SettingsPageInterface;
use EwTools\Support\Str;

/**
 * Tab
 *
 * @link    http://infomaker.se
 * @package AdTech\Tabs
 * @since   AdTech\Tabs\Tab 0.1
 */
abstract class Tab implements SettingsPageInterface {
    
    protected static $title = 'Tab';
    
    /**
     * Keep track if error has occurred
     *
     *
     * @var bool
     */
    protected $has_error = false;
    
    protected function onError() {
        return $this->has_error;
    }
    
    /**
     * Retrieve any messages that may be displayed by the settings-page
     *
     * @since 0.1
     * @return string
     */
    public function getMessage() {
        return '';
    }
    
    /**
     * Retrieve the slug to be used to identify and activate tab
     *
     * @since 0.1
     * @return string
     */
    public function getTabSlug() {
        return Str::slug( static::$title );
    }
    
    /**
     * The title/label displayed on the tab
     *
     * @since 0.1
     * @return string
     */
    public function getTabTitle() {
        return static::$title;
    }
}