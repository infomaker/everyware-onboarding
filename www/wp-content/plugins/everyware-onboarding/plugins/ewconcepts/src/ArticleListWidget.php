<?php
/**
 * Concept Article widget.
 * Plugin Name: Concept Article Widget
 * Description: Use this widget to retrieve articles related by a Concept. Create lists or render them by templates.
 * Version: 1.0.0
 * Author: Infomaker Scandinavia AB
 * Author URI: http://infomaker.se
 */

namespace EwConcepts;

use AbstractOcObject;
use EwTools\Admin\OCWidget;
use EwTools\Handler\OpenContentQueryBuilder;
use EwTools\Models\Section;
use EwTools\Support\Str;
use EwTools\Handler\Templates;

/**
 * ArticleListWidget
 *
 * @link    http://infomaker.se
 * @package EwConcepts
 * @since   EwConcepts\ArticleListWidget 1.0.0
 */
class ArticleListWidget extends OCWidget {
    
    /**
     * Contains the relational property between Concepts and Articles
     *
     * @var string
     */
    protected static $article_relation_property;
    
    /**
     * Contains all properties that will be fetched from OC
     *
     * @var array
     */
    protected static $article_properties;
    
    /**
     * Twig-template to use
     *
     * @var string
     */
    protected $template_path = '@plugins/ewconcepts/src/templates/widgets/article-list';
    
    /**
     * Contains the default template to use when rendering articles in the list
     *
     * @var string
     */
    private $default_template = 'ocarticle-default.php';
    
    /**
     * Widget fields
     * Contains fields for managing a headline and page to bind it to, a way to fetch data from Open Content
     * and a teaser template to represent every article rendered in the list.
     *
     * @var array
     */
    protected $fields = [
        'headline'             => '',
        'section'              => '',
        'concept_names'        => '',
        'template'             => '',
        'remove_list_elements' => false,
        'oc_query'             => '',
        'oc_query_start'       => 0,
        'oc_query_limit'       => 10,
        'oc_query_sort'        => '',
    ];
    
    /**
     * ArticleList constructor.
     *
     * @since 0.1
     */
    public function __construct() {
        $this->setWidgetData( __FILE__ );
        parent::__construct( 'ew-concept-article-list' );
        
        if( static::$article_relation_property === null ) {
            static::$article_relation_property = collect( $this->oc_api->get_properties_by_type( 'Concept', 'Article' ) )->first();
        }
        
        if( static::$article_properties === null ) {
            static::$article_properties = $this->oc_api->get_default_properties();
        }
    }
    
    /**
     * Fires when rendering the widget form
     *
     * @param array $view_data
     *
     * @since 0.1
     * @return void
     */
    protected function edit( $view_data ) {
        $this->view( 'form', array_replace_recursive( $view_data, [
            'sections'   => Section::toSelect( $view_data[ 'section' ][ 'value' ] ),
            'templates'  => Templates::getSelectData(),
            'sortings'   => $this->sortingsSelectData(),
            'textdomain' => 'ewtools_textdomain'
        ] ) );
    }
    
    /**
     * Fires when rendering widget
     *
     * @param array $view_data
     *
     * @since 0.1
     * @return void
     */
    protected function show( $view_data ) {
        $view_data = array_replace_recursive( $view_data, [
            'articles'      => $this->fetchArticles( $view_data ),
            'section_class' => Section::getClassBySlug( $view_data[ 'section' ] )
        ] );
        
        $this->view( 'widget', $view_data );
    }
    
    /**
     * Function to fetch the articles from Open Content
     *
     * @param $view_data
     *
     * @since 0.1
     * @return array
     */
    private function fetchArticles( $view_data ) {
        $concepts = $this->oc_api->search( $this->convertWidgetQuery( $view_data ) );
        
        $articles = collect( $concepts )->filter( function ( $item ) {
                return $item instanceof AbstractOcObject;
            } )->map( function ( AbstractOcObject $concept ) {
                return $concept->get( static::$article_relation_property );
            } )->flatten( 1 )->sortByDesc( 'pubdate' )->toArray();
        
        return $this->generateFromTemplate( $articles, $view_data[ 'template' ] );
    }
    
    /**
     * Run the article through the template to generate HTML
     *
     * @param OC_Article[] $oc_articles
     * @param string       $template
     *
     * @since 0.1
     * @return array
     */
    private function generateFromTemplate( $oc_articles, $template = '' ) {
        $template_file = Str::notEmpty( $template ) ? $template : $this->default_template;
        $template      = locate_template( [ $template_file, "/templates/{$template_file}" ] );
        
        $articles = [];
        ob_start();
        foreach ( $oc_articles as $index => $article ) {
            include $template;
            $articles[] = ob_get_contents();
            ob_clean();
        }
        ob_end_clean();
        
        return $articles;
    }
    
    /**
     * Convert the form-data into values readable by OpenContent::search()
     *
     * @param $view_data
     *
     * @since 0.1
     * @return array
     */
    protected function convertWidgetQuery( array $view_data ) {
        $query = OpenContentQueryBuilder::where( 'Name', $this->extractConceptNames( $view_data ) );
        
        return [
            'q'            => $query->buildQueryString(),
            'contenttypes' => [ 'Concept' ],
            'properties'   => $this->getProperties(),
            'filters'      => $this->getFilters( $view_data )
        ];
    }
    
    /**
     * Extract the list of names from the string provided by the form
     *
     * @since 1.0.0
     *
     * @param array $view_data
     *
     * @return array
     */
    protected function extractConceptNames( array $view_data ) {
        return array_filter( (array)preg_split( '/\s*(,|\|)\s*/', $view_data[ 'concept_names' ] ) );
    }
    
    /**
     * @since 1.0.0
     * @return array
     */
    protected function getProperties() {
        return array_map( function ( $property ) {
            return static::$article_relation_property . '.' . $property;
        }, static::$article_properties );
    }
    
    /**
     * @param $view_data
     *
     * @since 1.0.0
     * @return string
     */
    protected function getFilters( $view_data ) {
        $filters = array_filter( [
            'q'         => $view_data[ 'oc_query' ],
            'sort.name' => $view_data[ 'oc_query_sort' ],
            'limit'     => $view_data[ 'oc_query_limit' ],
            'start'     => $view_data[ 'oc_query_start' ],
        ] );
        
        $filter_query = implode( '|', array_map( function ( $v, $k ) {
            return "{$k}={$v}";
        }, $filters, array_keys( $filters ) ) );
        
        return static::$article_relation_property . "({$filter_query})";
    }
}