<?php namespace EwConcepts;

use EwConcepts\Models\Page;
use EwTools\Storage\Cache;
use EwTools\Support\Str;
use OcAPI;

/**
 * ConceptsController
 *
 * @link    http://infomaker.se
 * @package EwConcepts
 * @since   EwConcepts\ConceptsController 0.1
 */
class ConceptsController {
    
    /**
     * @var OcAPI
     */
    protected $oc_api;
    
    /**
     * Contains the current request
     *
     * @var string
     */
    protected $request;
    
    /**
     * ConceptsController constructor.
     *
     * @since 0.1
     * * @param OcAPI $oc_api
     */
    public function __construct( OcAPI $oc_api ) {
        $this->request = $this->extractConceptRoute();
        $this->oc_api  = $oc_api;
    }
    
    /**
     * WP_filter: Fires on the "request" filter
     * Route the request to index.php?concept_route=[route/for/concept]
     *
     * @param array $query_vars
     *
     * @since 0.1
     * @return array
     */
    public function routeRequest( array $query_vars = [] ) {
        if( ! empty( $query_vars ) && $this->shouldRoute() ) {
            return [ 'concept_route' => $this->request ];
        }
        
        return $query_vars;
    }
    
    /**
     * Wp_action: Fires on the "template_redirect" action
     * Redirect to concepts-template if the conditions are met
     *
     * @since 0.1
     * @return void
     */
    public function templateRedirect() {
        
        if( get_query_var( 'concept_route' ) !== '' && $this->hasTemplate() ) {
            add_filter( 'template_include', function () {
                return locate_template( 'concepts.php' );
            }, 10, 1 );
        }
    }
    
    /**
     * WP_filter: Firers on the "query_vars" filter
     * Make sure the necessary query parameter is registered in Wordpress
     *
     * @param $query_vars
     *
     * @since 0.1
     * @return array
     */
    public function setQueryVars( $query_vars ) {
        return array_merge( $query_vars, [ 'concept_route' ] );
    }
    
    /**
     * Extract route after home_url and remove query vars.
     *
     * @since 0.1
     * @return string
     */
    protected function extractConceptRoute() {
        return ltrim( strtok( str_replace( home_url(), '', $_SERVER[ 'REQUEST_URI' ] ), '?' ), '/' );
    }
    
    /**
     * Fetch available types of Concepts from Open Content
     *
     * @since 0.1
     * @return array
     */
    protected function getConceptPages() {
        $cache_key = 'ew_concepts_page_routes';
        
        if( ( $concept_types = Cache::get( $cache_key ) ) !== false ) {
            return $concept_types;
        }
        
        Cache::set( $cache_key, Page::all(), OcAPI::TRANSIENT_EXPIRE_TIME );
        
        return $concept_types;
    }
    
    /**
     * Determine if the necessary template exists
     *
     * @since 0.1
     * @return bool
     */
    protected function hasTemplate() {
        return locate_template( 'concepts.php' ) !== '';
    }
    
    /**
     * Determine if a request should be considered a Concept page and therefore be routed as such.
     *
     * @return bool
     * @since 0.1
     */
    protected function shouldRoute() {
        return $this->hasTemplate() && get_page_by_path( $this->request ) === null && Page::hasPage( Str::before( $this->request, '/' ) );
    }
    
    public static function init() {
        $controller = new static( new OcAPI() );
        add_filter( 'query_vars', [ $controller, 'setQueryVars' ] );
        add_filter( 'request', [ $controller, 'routeRequest' ] );
        add_action( 'template_redirect', [ $controller, 'templateRedirect' ] );
    }
    
    /**
     * Setup routing for automatic concept-pages
     *
     * @since 0.1
     */
    public static function setup() {
        add_action( 'init', [ static::class, 'init' ] );
    }
}