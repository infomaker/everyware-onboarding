<?php

/**
 * Plugin Name: EwConcepts Plugin
 * Description: Plugin created for Everyware. This plugin includes pages, helper-classes and other tools to manage
 * Concepts from Open Content.
 * Version: 0.1.0
 * Author: Infomaker Scandinavia AB
 * Author URI: http://infomaker.se
 */

use EwConcepts\AdminPage;
use EwConcepts\ArticleListWidget;
use EwConcepts\ConceptsController;
use EwConcepts\Tabs\Pages;

if( ! defined( 'WPINC' ) || ! class_exists( 'CustomerPlugin' ) ) {
    exit;
}

ArticleListWidget::register();

if( is_admin() ) {
    CustomerPlugin::getInstance()->addPage( AdminPage::setup( [ new Pages( new OcAPI() ) ] ) );
} else {
    ConceptsController::setup();
}