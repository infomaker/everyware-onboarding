var gulp = require('gulp'); // Gulp of-course

// CSS related plugins.
var sass = require('gulp-sass'), // Gulp pluign for Sass compilation
	autoprefixer = require('gulp-autoprefixer'), // Autoprefixing magic
	minifyCss = require('gulp-clean-css'); // Minifies CSS files

// JS related plugins.
var	browserify = require('browserify'),
	concat = require('gulp-concat'), // Concatenates JS files
	uglify = require('gulp-uglify'); // Minifies JS files

// Utility related plugins.
var notify = require('gulp-notify'), // Sends message notification to you
	source = require('vinyl-source-stream'),
	buffer = require('vinyl-buffer'),
	rename = require('gulp-rename'), // Renames files E.g. style.css -> style.min.css
	sourcemaps = require('gulp-sourcemaps'), // Maps code in a compressed file (E.g. style.css) back to it’s original position in a source file
	util = require('gulp-util');

var EwGulp = (function () {
	
	var jqueryPlugin = function (name) {
			return srcAsset('/js/jquery/jquery.' + name + '.js');
		},
		themeAssetsPath = function (theme) {
			return './www/wp-content/themes/' + theme + '/assets';
		},
		srcAsset = function (path) {
			return './assets' + path;
		},
		pluginAssetsPath = function (plugin) {
			return './www/wp-content/plugins/' + plugin + '/src/admin/assets';
		};
	
	return {
		jqueryPlugin: jqueryPlugin,
		pluginAssetsPath: pluginAssetsPath,
		themeAssetsPath: themeAssetsPath,
		srcAsset: srcAsset
	};
})();

var scripts = {
	'popper': 'node_modules/popper.js/dist/umd/popper.min.js',
	'bootstrap': 'node_modules/bootstrap/dist/js/bootstrap.min.js',
	'jquery': 'node_modules/jquery/dist/jquery.min.js',
	'lazysizes': 'node_modules/lazysizes/lazysizes.min.js',
	'slick': 'node_modules/slick-carousel/slick/slick.min.js'
};

var TryoutEwGulp = (function () {
	var theme = EwGulp.themeAssetsPath('tryout-theme'),
		production = !!util.env.production,
		dev = !production;
	return {
		bodyJS: function () {
			return [
				EwGulp.jqueryPlugin('inview'),
				EwGulp.jqueryPlugin('mediamatcher'),
				EwGulp.srcAsset('/js/body/body.js')
			];
		},
		scssTask: function (assets) {
			var styleDest = theme + '/css';
			
			return gulp.src(assets)
				.pipe(dev ? sourcemaps.init() : util.noop())
				.pipe(sass({
					errLogToConsole: true,
					outputStyle: 'compact',
					// outputStyle: 'compressed',
					// outputStyle: 'nested',
					// outputStyle: 'expanded',
					precision: 10
				}))
				.pipe(autoprefixer(
					'last 2 version',
					'> 1%',
					'safari 5',
					'ie 8',
					'ie 9',
					'opera 12.1',
					'ios 6',
					'android 4'))
				.pipe(dev ? sourcemaps.write() : util.noop())
				.pipe(gulp.dest(styleDest))
				.pipe(rename({suffix: '.min'}))
				.pipe(minifyCss({level: 2}))
				.pipe(gulp.dest(styleDest))
				.pipe(notify({message: 'Build of "styles" Completed!', onLast: true}));
		},
		JsTask: function (assets, concatName) {
			var styleDest = theme + '/js';
			
			return gulp.src(assets)
				.pipe(dev ? sourcemaps.init() : util.noop())
				.pipe(concat(concatName + '.js'))
				.pipe(browserify())
				.pipe(dev ? sourcemaps.write() : util.noop())
				.pipe(gulp.dest(styleDest))
				.pipe(rename({basename: concatName, suffix: '.min'}))
				.pipe(uglify())
				.pipe(gulp.dest(styleDest))
				.pipe(notify({
					message: 'Build of "' + concatName + '.js" and "' + concatName + '.min.js" Completed!',
					onLast: true
				}));
		},
		devScssTask: function (assets) {
			return gulp.src(assets)
				.pipe(sourcemaps.init())
				.pipe(sass().on('error', function (err) {
					util.log(err.message);
					this.emit('end')
				}))
				.pipe(sourcemaps.write())
				.pipe(gulp.dest(theme + '/css'));
		},
		stageScssTask: function (assets) {
			return gulp.src(assets)
				.pipe(sourcemaps.init())
				.pipe(sass().on('error', util.log))
				.pipe(minifyCss({compatibility: 'ie9', level: 2}))
				.pipe(rename({
					suffix: '.min'
				}))
				.pipe(sourcemaps.write())
				.pipe(gulp.dest(theme + '/css'));
		},
		prodScssTask: function (assets) {
			return gulp.src(assets)
				.pipe(sass().on('error', util.log))
				.pipe(minifyCss({compatibility: 'ie9', level: 2}))
				.pipe(rename({
					suffix: '.min'
				}))
				.pipe(gulp.dest(theme + '/css'));
		},
		devJsTask: function (assets, concatName) {
			return browserify({
				entries: assets,
				debug: true
				})
				.bundle()
				.on('error', util.log)
				.pipe(source(concatName))
				.pipe(buffer())
				.pipe(sourcemaps.init({loadMaps: true}))
				.pipe(sourcemaps.write('.'))
				.pipe(gulp.dest(theme + '/js'));
		},
		stageJsTask: function (assets, concatName) {
			return browserify({
				entries: assets,
				debug: true
			})
				.bundle()
				.on('error', util.log)
				.pipe(source(concatName))
				.pipe(buffer())
				.pipe(uglify())
				.pipe(sourcemaps.init({loadMaps: true}))
				.pipe(sourcemaps.write('.'))
				.pipe(gulp.dest(theme + '/js'));
		},
		prodJsTask: function (assets, concatName) {
			console.log( 'Destination: ', theme + '/js' );
			console.log( 'concatName: ', concatName );
			return browserify({
				entries: assets,
				debug: false
			})
				.bundle()
				.on('error', util.log)
				.pipe(source(concatName))
				.pipe(buffer())
				.pipe(uglify())
				.pipe(gulp.dest(theme + '/js'));
		}
	}
})();

/*
 |--------------------------------------------------------------------------
 | Development and Test tasks
 |--------------------------------------------------------------------------
 */

gulp.task('sass:dev', function () {
	return TryoutEwGulp.devScssTask(EwGulp.srcAsset('/scss/style.scss'));
});

gulp.task('sass:admin', function () {
	return TryoutEwGulp.devScssTask(EwGulp.srcAsset('/scss/admin/admin.scss'));
});

gulp.task('js-dev:admin', function () {
	return TryoutEwGulp.devJsTask(EwGulp.srcAsset('/js/admin/admin.js'), 'admin.js');
});

gulp.task('js-dev:body', function () {
	return TryoutEwGulp.devJsTask(TryoutEwGulp.bodyJS(), 'body.js');
});

gulp.task('js-dev:head', function () {
	return TryoutEwGulp.devJsTask(EwGulp.srcAsset('/js/head.js'), 'head.js');
});

gulp.task('js-dev:article', function () {
	return TryoutEwGulp.devJsTask(EwGulp.srcAsset('/js/article/article.js'), 'article.js');
});

/*
 |--------------------------------------------------------------------------
 | Staging tasks
 |--------------------------------------------------------------------------
 */

gulp.task('sass:stage', function () {
	return TryoutEwGulp.stageScssTask(EwGulp.srcAsset('/scss/style.scss'));
});

gulp.task('sass-stage:admin', function () {
	return TryoutEwGulp.stageScssTask(EwGulp.srcAsset('/scss/admin/admin.scss'));
});

gulp.task('js-stage:admin', function () {
	return TryoutEwGulp.stageJsTask(EwGulp.srcAsset('/js/admin/admin.js'), 'admin.min.js');
});

gulp.task('js-stage:body', function () {
	return TryoutEwGulp.stageJsTask(TryoutEwGulp.bodyJS(), 'body.min.js');
});

gulp.task('js-stage:head', function () {
	return TryoutEwGulp.stageJsTask(EwGulp.srcAsset('/js/head.js'), 'head.min.js');
});

gulp.task('js-stage:article', function () {
	return TryoutEwGulp.stageJsTask(EwGulp.srcAsset('/js/article/article.js'), 'article.min.js');
});

/*
 |--------------------------------------------------------------------------
 | Production tasks
 |--------------------------------------------------------------------------
 */

gulp.task('sass:prod', function () {
	return TryoutEwGulp.prodScssTask(EwGulp.srcAsset('/scss/style.scss'));
});

gulp.task('sass-prod:admin', function () {
	return TryoutEwGulp.prodScssTask(EwGulp.srcAsset('/scss/admin/admin.scss'));
});

gulp.task('js-prod:admin', function () {
	return TryoutEwGulp.prodJsTask(EwGulp.srcAsset('/js/admin/admin.js'), 'admin.min.js');
});

gulp.task('js-prod:body', function () {
	return TryoutEwGulp.prodJsTask(TryoutEwGulp.bodyJS(), 'body.min.js');
});

gulp.task('js-prod:head', function () {
	return TryoutEwGulp.prodJsTask(EwGulp.srcAsset('/js/head.js'), 'head.min.js');
});

gulp.task('js-prod:article', function () {
	return TryoutEwGulp.prodJsTask(EwGulp.srcAsset('/js/article/article.js'), 'article.min.js');
});

/*
 |--------------------------------------------------------------------------
 | Tasks
 |--------------------------------------------------------------------------
 */

// Add jquery to the theme
gulp.task('js:jquery', function () {
	return gulp.src(scripts['jquery']).pipe(gulp.dest(EwGulp.themeAssetsPath('tryout-theme') + '/js'));
});

gulp.task('fonts', function () {
	return gulp.src(EwGulp.srcAsset('/fonts/**/**.*'))
		.pipe(gulp.dest(EwGulp.themeAssetsPath('tryout-theme') + '/fonts'));
});

gulp.task('images', function () {
	return gulp.src(EwGulp.srcAsset('/images/**/*.*'))
		.pipe(gulp.dest(EwGulp.themeAssetsPath('tryout-theme') + '/images'));
});

gulp.task('svgs', function () {
	return gulp.src(EwGulp.srcAsset('/svg/**.svg'))
		.pipe(gulp.dest(EwGulp.themeAssetsPath('tryout-theme') + '/svg'));
});

gulp.task('copy', ['fonts', 'images', 'svgs']);

gulp.task('js:dev', ['js-dev:article', 'js-dev:body', 'js-dev:head'], function () {
	return gulp.src([
		scripts.jquery,
		scripts.popper,
		scripts.bootstrap,
		scripts.lazysizes,
		scripts.slick,
		EwGulp.themeAssetsPath('tryout-theme') + '/js/body.js'
	])
		.pipe(concat('body.js'))
		.pipe(gulp.dest(EwGulp.themeAssetsPath('tryout-theme') + '/js'));
});

gulp.task('js:stage', ['js-stage:article', 'js-stage:body', 'js-stage:head'], function () {
	return gulp.src([
		scripts.jquery,
		scripts.popper,
		scripts.bootstrap,
		scripts.lazysizes,
		scripts.slick,
		EwGulp.themeAssetsPath('tryout-theme') + '/js/body.min.js'
	])
		.pipe(concat('body.min.js'))
		.pipe(gulp.dest(EwGulp.themeAssetsPath('tryout-theme') + '/js'));
});

gulp.task('js:prod', ['js-prod:article', 'js-prod:body', 'js-prod:head'], function () {
	return gulp.src([
		scripts.jquery,
		scripts.popper,
		scripts.bootstrap,
		scripts.lazysizes,
		scripts.slick,
		EwGulp.themeAssetsPath('tryout-theme') + '/js/body.min.js'
	])
		.pipe(concat('body.min.js'))
		.pipe(gulp.dest(EwGulp.themeAssetsPath('tryout-theme') + '/js'));
});

gulp.task('admin:dev', ['js-dev:admin', 'sass:admin']);
gulp.task('admin:stage', ['js-stage:admin', 'sass-stage:admin']);
gulp.task('admin:prod', ['js-prod:admin', 'sass-prod:admin']);

gulp.task('default', ['sass:dev', 'js:dev', 'admin:dev', 'copy', 'js:jquery']);
gulp.task('stage', ['sass:stage', 'js:stage', 'admin:stage', 'copy', 'js:jquery']);
gulp.task('prod', ['sass:prod', 'js:prod', 'admin:prod', 'copy', 'js:jquery']);

/*
 |--------------------------------------------------------------------------
 | Watcher for development
 |--------------------------------------------------------------------------
 */
gulp.task('watch', ['default'], function () {
	var watch = {
		scss: [
			EwGulp.srcAsset('/lib/bootstrap-scss/*.scss'),
			EwGulp.srcAsset('/scss/**/*.scss')
		],
		js: [
			EwGulp.srcAsset('/js/**/*.js')
		],
		onChange: function (event) {
			console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
		}
	};
	
	gulp.watch(watch.scss, ['sass:dev']).on('change', watch.onChange);
	gulp.watch(watch.js, ['js:dev']).on('change', watch.onChange);
});

gulp.task('watch:admin', ['admin:dev'], function () {
	var watch = {
		scss: [
			EwGulp.srcAsset('/scss/admin/**/*.scss')
		],
		js: [
			EwGulp.srcAsset('/js/admin/**/*.js')
		],
		onChange: function (event) {
			console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
		}
	};
	
	gulp.watch(watch.scss, ['sass:admin']).on('change', watch.onChange());
	gulp.watch(watch.js, ['js-dev:admin']).on('change', watch.onChange());
});