ngx.header["Content-Type"] = "text/plain"

local json = require "cjson"
local http = require "resty.http"

local m_redis = nil
local redis_config = nil
local purge_url = nil
local user_agent_keys = {
    ["mobile"] = "Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X)",
    ["tablet"] = "Mozilla/5.0 (iPad; CPU OS 7_0 like Mac OS X)",
    ["desktop"] = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4)",
}

local Purge = {
    _VERSION = '0.01',
}

function Purge.run()
    -- need to read the body in order to avoid errors.
    ngx.req.read_body()
    local post_args, err = ngx.req.get_post_args()

    if not err then
        if post_args["purge_url"] ~= nil then
            purge_url = post_args["purge_url"]
            local red = Purge.connect_to_redis()

            if red ~= nil and purge_url ~= nil then
                local wildcard_key = Purge.generate_cache_key( purge_url )
                red:select( ledge:config_get("redis_database") )

                Purge.expire_pattern(0, wildcard_key, 1000)
            end
        end
    end

    ngx.exit(200)
end

function Purge.expire_pattern(cursor, pattern, count)
    local res, err = m_redis:scan(
        cursor,
        "MATCH", pattern,
        "COUNT", count
    )

    if not res or res == ngx_null then
        ngx_log(ngx_ERR, err)
    else

        for _,key in ipairs(res[2]) do
            local entity = m_redis:get(key)
            if entity then
                ledge:ctx().redis = m_redis

                -- Remove the ::key part to give the cache_key without a suffix
                local cache_key = string.sub(key, 1, -(string.len("::key") + 1))
                local res = ledge:expire_keys(
                    ledge:key_chain(cache_key), -- a keychain for this key
                    ledge:entity_keys(cache_key .. "::" .. entity) -- the entity keys for the live entity
                )

                -- If cache is stale, trigger update
                if res == true then
                    local device, status = string.match(key, "ledge:cache:(%w*):(%w*).*")

                    local data = {
                        uri = purge_url,
                        headers = {
                            ["user-agent"] = user_agent_keys[device]
                        }
                    }

                    -- if the cache is based on auth, set revalidation header
                    if status == 'open' then
                        data.headers["X-Revalidation-Process"] = true
                    end

                    Purge.trigger_background_update( data );
                end
            end
        end
    end
end

function Purge.trigger_background_update( data )
    local httpc = http.new()

    local headers = {
        ["Cache-Control"] = "max-stale=0, stale-if-error=0",
    }

    for k, v in pairs(headers) do
        data.headers[k] = v
    end

    local res, err = httpc:request_uri(data.uri, {
        method = "GET",
        headers = data.headers
    })
end

function Purge.connect_to_redis()
    redis_config = ledge:config_get("redis_host")
    
    if redis_config ~= nil then
        local redis_m = require "resty.redis"
        local red = redis_m:new()

        red:set_timeout(3000)

        -- Connect to redis.
        local ok,err = red:connect(redis_config.host, redis_config.port)
        if not ok then
            ngx.say("Could not connect to Redis.")
            return nil;
        else
            m_redis = red
        end

        return red
    end

    return nil
end

function Purge.generate_cache_key( url )
    local prefix = 'ledge:cache:'
    local domain, args = string.match(url, "https?://([%w%.]*)/?(.*)" )

    return prefix .. "*:*:" .. domain .. ":/" .. args .."::key"
end


Purge.run()