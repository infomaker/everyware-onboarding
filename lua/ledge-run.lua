-- Set headers which will be passed on to upstream PHP
ngx.req.set_header("X-Skip-Cache", ngx.var.skip_cache)
ngx.req.set_header("X-Force-Cache", ngx.var.force_cache)

-- If the Cache-Control header is set to "max-age=0", which is the default
-- for most new browser requests, we will remove it, to "force" cache hits.
-- Same happens if it is set to "no-cache".
-- Unless skip_cache is set to 1.
-- Note that this slightly breaks the contract for Cache-Control.
local cacheHeader = ngx.req.get_headers()["Cache-Control"]
if (cacheHeader == "max-age=0" or cacheHeader == "no-cache") and ngx.var.skip_cache == "0" then
    ngx.req.clear_header("Cache-Control")
    ngx.req.clear_header("Pragma")
end

-- Seems we have to set the read timeout larger than the default 5 seconds.
ledge:config_set("upstream_read_timeout", 30000)

-- We want to cache on the device spec too.
-- Note that we have to set the cache_key_spec here instead of in
-- init_by_lua, since otherwise Ledge will complain about the
-- "ngx.var.every_device" variable not existing.
--
-- Check for AWS LoadBalancer forwarded_proto header.
local scheme = 'http'
if ngx.var.http_x_forwarded_proto ~= nil then
    scheme = ngx.var.http_x_forwarded_proto
end

-- URI's starting with /wp-content/ or /wp-includes/
-- exclude the scheme and host vars in cache_key_spec
local uri = ngx.var.uri
if string.find(uri, "^/wp%-content/") or string.find(uri, "^/wp%-includes/") then
  ledge:config_set("cache_key_spec", { ngx.var.uri, ngx.var.args })
else
  ledge:config_set("cache_key_spec", { ngx.var.every_device, ngx.var.customerprefix_auth_var, scheme, ngx.var.host, ngx.var.uri, ngx.var.args })
end

-- This bind will modify headers and such before it's saved to Redis.
-- i.e. this will control Ledge cache expires
ledge:bind("before_save", function(res)
    -- Check if the request should not be cached, then set the length higher than max memory
    -- so we don't create loads of unnecessary queues
    if ngx.var.skip_cache == "1" then
        res.length = ledge:config_get("cache_max_memory") * 1024 + 1
    end

    -- Set the cache time to be calculated in Expires header.
    local cache_time = 300 -- Defaults to 300 seconds

    -- Set correct Cache-Control header for stale functionality
    -- The Expires header tell Ledge when to expire the cache.
    res.header["Cache-Control"] = "stale-while-revalidate=3600, stale-if-error=43200"
    res.header["Expires"] = os.date("%a, %d %b %Y %H:%M:%S GMT", (ngx.time() + cache_time))
end)

ledge:bind("origin_fetched", function(res)
    -- Redirects coming from upstream will redirect to port 8081. Let us fix that.
    if res.status == 301 then
        local location = res.header["Location"]
        -- Remove first occurance of the port reference
        res.header["Location"] = string.gsub(location, ":8081", "", 1)
    end
end)

-- Set specific Cache-Control headers for different filetypes.
-- These headers will be visible to the users.
ledge:bind("response_ready", function(res)
  local cache_control = "max-age=3600"
  if res.header["Content-Type"] ~= nil then
    local content_type = res.header["Content-Type"]
    if content_type == "text/css" or string.find(content_type, "application/") then
      cache_control = "max-age=31536000"
    elseif string.find(content_type, "text%/html") then
      cache_control = "no-cache, no-store, must-revalidate"
      res.header["Pragma"] = "no-cache"
      res.header["Expires"] = "0"
    elseif string.find(content_type, "image/") then
      cache_control = "max-age=86400"
    end
  end

  res.header["Cache-Control"] = cache_control -- Default set 300 seconds max age
end)

-- Modify the domain and url that revalidation background process should request.
ledge:bind("before_save_revalidation_data", function(params, headers)
  local server_host = ngx.var.host
  params['server_addr'] = server_host
  params['scheme'] = scheme
  params['server_port'] = 80

  if scheme == "https" then
    params['server_port'] = 443
    params['ssl_server_name'] = server_host
    params['ssl_verify'] = "false"
  end

  for k, v in pairs(ngx.req.get_headers()) do
      headers[k] = v
  end
end)

ledge:run()
