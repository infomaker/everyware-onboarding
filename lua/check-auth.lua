local uri = ngx.var.uri
local method = ngx.var.request_method

-- Return "false" when uri contains /wp-admin/, or not request_method GET or HEAD
if (string.find(uri, "/wp%-admin/") and uri ~= '/wp-admin/admin-ajax.php') or method ~= "GET" then
  return "false"
end

-- Default to always "true"
local check_auth = "true"

-- Match all uri's longer than and 5 characters (/x.js)
if string.len(uri) >= 5 then
  -- An array with "allowed" extensions, that should skip check-auth
  local extensions = { "js",
                       "css",
                       "eot",
                       "ttf",
                       "woff",
                       "woff2",
                       "otf",
                       "xml",
                       "html",
                       "htm",
                       "svg",
                       "svgz",
                       "rss",
                       "atom",
                       "jpg",
                       "jpeg",
                       "gif",
                       "png",
                       "ico",
                       "bmp",
                       "rtf",
                       "map" }

  -- Parse the file extension from request_uri
  local extension = uri:match("[^.]+$")

  -- Loop through extensions array and match with this extension
  -- If there are a match, set check_auth "false"
  for _, v in ipairs(extensions) do
    if v == extension then
      check_auth = "false"
    end
  end
  return check_auth -- Return the check_auth var, set to false if had match on array
else
  return "true"
end
