#!/usr/bin/env bash
#

# Make the user enter which domain we should create certificate for
echo "Enter the primary domain i.e. ewtest.dev" ;\
read DOMAIN

# Make the user enter all alt domain names
echo "Enter all alternative domains i.e. infomaker.ewtest.dev,demo.ewtest.dev"
read ALT_DOMAINS

# Set the wildcarded domain
WILDCARDED_DOMAIN="*.${DOMAIN}"

COUNTER=2
FIRST_LOOP=1
for ALT_DOMAIN in $(echo ${ALT_DOMAINS} | sed "s/,/ /g")
do
  (( ${FIRST_LOOP} )) && ALT_DOMAINS="DNS.1 = ${DOMAIN}\nDNS.${COUNTER} = ${ALT_DOMAIN}" || ALT_DOMAINS="${ALT_DOMAINS}\nDNS.${COUNTER} = ${ALT_DOMAIN}"

  unset FIRST_LOOP
  let COUNTER=COUNTER+1
done

# Specify where we will install
# the xip.io certificate
SSL_DIR="$(pwd)"

# A blank passphrase
PASSPHRASE=""

# Set our CSR variables
SUBJ="
C=SE
ST=Smaland
O=Infomaker Scandinavia AB
localityName=Dev environment
commonName=${WILDCARDED_DOMAIN}
organizationalUnitName=Infomaker IT
emailAddress=im-it@infomaker.se
"

cat > v3.txt << EOF
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
$(printf "%b" "${ALT_DOMAINS}")
EOF

# Generate our Private Key, CSR and Certificate
openssl genrsa -out "${SSL_DIR}/${DOMAIN}.key" 4096 > /dev/null 2>&1
openssl req -new -subj "$(echo -n "${SUBJ}" | tr "\n" "/")" -key "${SSL_DIR}/${DOMAIN}.key" -out "${SSL_DIR}/${DOMAIN}.csr" -sha512 -passin pass:${PASSPHRASE} > /dev/null 2>&1
openssl x509 -req -days 24855 -in "${SSL_DIR}/${DOMAIN}.csr" -signkey "${SSL_DIR}/${DOMAIN}.key" -out "${SSL_DIR}/${DOMAIN}.crt" -sha512 -extfile v3.txt > /dev/null 2>&1

if [[ $? -eq "0" ]]; then
  echo "Successfully created certificate for ${WILDCARDED_DOMAIN}"
else
  echo "Could not create certificate for ${WILDCARDED_DOMAIN}!"
fi

# Clean up
rm -rf ${SSL_DIR}/v3.txt ${SSL_DIR}/*.csr
