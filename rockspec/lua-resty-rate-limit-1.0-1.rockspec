package = "lua-resty-rate-limit"
version = "1.0-1"
source = {
   url = "git://github.com/kimbrandt/lua-resty-rate-limit",
   tag = "v1.0"
}
description = {
   summary = "Request rate-limit with Redis support.",
   homepage = "https://github.com/kimbrandt/lua-resty-rate-limit",
   license = "MIT License",
   maintainer = "Kim Brandt <kim@infomaker.se>"
}
dependencies = {
   "lua ~> 5.1",
   "lua-cjson ~> 2.1.0"
}
build = {
   type = "builtin",
   modules = {
      ["resty.rate.limit"] = "lib/resty/rate/limit.lua"
   }
}
